package com.airpnp.ads.activities;

import android.app.Activity;
import android.os.Build;
import android.os.Bundle;
import android.view.Window;

public class CleanActivity extends Activity {
	@Override
	protected void onCreate( Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
			finishAndRemoveTask();
		} else {
			finish();
		}
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		overridePendingTransition(0, 0);
	}

}
