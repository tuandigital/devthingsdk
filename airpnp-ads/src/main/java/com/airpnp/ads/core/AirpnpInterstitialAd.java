package com.airpnp.ads.core;

import android.app.Activity;
import android.os.Build;
import android.text.TextUtils;

import com.airpnp.ads.storages.RemoteConfig;
import com.airpnp.ads.utils.SystemUtil;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;

public class AirpnpInterstitialAd {
	private Activity mActivity;
	private AirpnpInterstitialAdListener mListener;
	private InterstitialAd mInterstitialAd;

	public AirpnpInterstitialAd(Activity activity, AirpnpInterstitialAdListener listener) {
		mActivity = activity;
		mListener = listener;
	}

	public void loadAd(String admobId) {
		if (!TextUtils.isEmpty(admobId)) {
			mInterstitialAd = new InterstitialAd(mActivity);
			mInterstitialAd.setAdUnitId(admobId);
			mInterstitialAd.setAdListener(new AdListener() {
				@Override
				public void onAdLoaded() {
					super.onAdLoaded();
					AirpnpInterstitialAd.this.onAdLoaded();
				}

				@Override
				public void onAdFailedToLoad(int i) {
					super.onAdFailedToLoad(i);
					AirpnpInterstitialAd.this.onAdLoadFailed();
				}

				@Override
				public void onAdOpened() {
					super.onAdOpened();
					AirpnpInterstitialAd.this.onAdDisplayed();
				}

				@Override
				public void onAdClosed() {
					super.onAdClosed();
					AirpnpInterstitialAd.this.onAdClosed();
				}
			});
			mInterstitialAd.loadAd(new AdRequest.Builder().build());
		} else {
			onAdLoadFailed();
		}
	}

	public void show() {
		if (mInterstitialAd != null) {
			mInterstitialAd.show();
		}
	}

	public void destroy() {
		mListener = null;
		mActivity = null;
	}

	private void onAdLoadFailed() {
		if (mListener != null) {
			mListener.onAdLoadFailed();
		}
	}

	private void onAdLoaded() {
		if (mListener != null) {
			mListener.onAdLoaded();
		}
	}

	private void onAdDisplayed() {
		if (mListener != null) {
			mListener.onAdDisplayed();
		}
	}

	private void onAdClosed() {
		if (mListener != null) {
			mListener.onAdClosed();
		}
	}
}
