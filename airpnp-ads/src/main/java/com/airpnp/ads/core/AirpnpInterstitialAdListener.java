package com.airpnp.ads.core;

public interface AirpnpInterstitialAdListener {
	void onAdLoaded();
	void onAdLoadFailed();
	void onAdDisplayed();
	void onAdClosed();
}
