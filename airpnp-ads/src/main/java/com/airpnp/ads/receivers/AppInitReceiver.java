package com.airpnp.ads.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.airpnp.ads.storages.PrefManager;
import com.airpnp.core.AirpnpApp;
import com.airpnp.core.utils.LogUtil;

public class AppInitReceiver extends BroadcastReceiver {
	@Override
	public void onReceive(Context context, Intent intent) {
		AirpnpApp.initialize(context);
		String action = intent.getAction();
//		LogUtil.w("Action init: " + action);
		if (Intent.ACTION_BOOT_COMPLETED.equals(action)) {
			UpdateConfigAlarmReceiver.setAlarm(context, false);
		} else {
			UpdateConfigAlarmReceiver.schedule(context);
		}
		FreqAlarmReceiver.schedule(context);
		long firstOpen = PrefManager.getInstance().getLong(PrefManager.Keys.FIRST_OPEN);
		if (firstOpen == 0) {
			PrefManager.getInstance().put(PrefManager.Keys.FIRST_OPEN, System.currentTimeMillis());
		}
	}
}
