package com.airpnp.ads.receivers;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.job.JobInfo;
import android.app.job.JobScheduler;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.SystemClock;

import com.airpnp.ads.services.UpdateConfigJobService;
import com.airpnp.ads.services.UpdateConfigService;
import com.airpnp.ads.storages.PrefManager;

public class UpdateConfigAlarmReceiver extends BroadcastReceiver {

	private static final long FETCH_INTERVAL = 1800000;
	private static final long FETCH_DELTA = 30000;
	private static final int REQUEST_CODE = 231;
	private static final int JOB_ID = 1313;
	private static final int BOOT_DELAY = 1800000;

	@Override
	public void onReceive(Context context, Intent intent) {
		//        Log.w("Adss", "Receive in UpgradeReceiver");
		int sdkInt = Build.VERSION.SDK_INT;
		if (sdkInt < Build.VERSION_CODES.O) {
			Intent serviceIntent = new Intent(context, UpdateConfigService.class);
			context.startService(serviceIntent);
		} else {
			ComponentName serviceComponent = new ComponentName(context, UpdateConfigJobService.class);
			JobInfo.Builder builder = new JobInfo.Builder(JOB_ID, serviceComponent);
			builder.setMinimumLatency(10000);
			builder.setOverrideDeadline(20000);
			builder.setRequiresCharging(false);
			JobScheduler jobScheduler = context.getSystemService(JobScheduler.class);
			if (jobScheduler != null) {
//                Log.w("Adss", "Start schedule job");
				jobScheduler.cancel(JOB_ID);
				jobScheduler.schedule(builder.build());
			}
		}
	}

	public static void schedule(Context context) {
		long lastFetched = PrefManager.getInstance().getLong(PrefManager.Keys.LAST_CONFIG_FETCHED, 0);
		long currentElapsed = SystemClock.elapsedRealtime();
//		LogUtil.w("In UpdateConfigAlarmReceiver");
		if (currentElapsed - lastFetched > FETCH_INTERVAL + FETCH_DELTA || lastFetched > currentElapsed) {
//            Log.w("Adss", "Schedule Upgrade Receiver");
			setAlarm(context, true);
		}
	}

	public static void setAlarm(Context context, boolean force) {
		AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
		if (alarmManager != null) {
			cancelAlarm(context);
			int sdkInt = Build.VERSION.SDK_INT;
			long when = SystemClock.elapsedRealtime();
			if (!force) {
				when += FETCH_INTERVAL;
			}
			PendingIntent pendingIntent = getPendingIntent(context);
			if (sdkInt < Build.VERSION_CODES.KITKAT) {
				alarmManager.set(AlarmManager.ELAPSED_REALTIME_WAKEUP, when, pendingIntent);
			} else if (sdkInt < Build.VERSION_CODES.M) {
				alarmManager.setExact(AlarmManager.ELAPSED_REALTIME_WAKEUP, when, pendingIntent);
			} else {
				alarmManager.setExactAndAllowWhileIdle(AlarmManager.ELAPSED_REALTIME_WAKEUP, when, pendingIntent);
			}
		}
	}

	private static void cancelAlarm(Context context) {
		AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
		if (alarmManager != null) {
			alarmManager.cancel(getPendingIntent(context));
		}
	}

	private static PendingIntent getPendingIntent(Context context) {
		Intent alarmIntent = new Intent(context, UpdateConfigAlarmReceiver.class);
		return PendingIntent.getBroadcast(context, REQUEST_CODE, alarmIntent, PendingIntent.FLAG_CANCEL_CURRENT);
	}
}

