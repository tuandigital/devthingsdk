package com.airpnp.ads.services;

import android.content.Context;
import android.content.Intent;
import android.os.SystemClock;
import android.support.annotation.NonNull;

import com.airpnp.ads.core.UnifySDK;
import com.airpnp.ads.receivers.FreqAlarmReceiver;
import com.airpnp.ads.storages.PrefManager;
import com.airpnp.core.compat.JobIntentService;
import com.airpnp.core.utils.LogUtil;

public class FreqService extends JobIntentService {

	public static void enqueue(Context context, int jobId){
		Intent intent = new Intent(context, FreqService.class);
		enqueueWork(context, FreqService.class, jobId, intent);
	}

	@Override
	protected void onHandleWork(@NonNull Intent intent) {
		LogUtil.w("Start freq service");
		UnifySDK.checkAndStartAd(this);
		PrefManager.getInstance().put(PrefManager.Keys.LAST_BG_RUNNING, SystemClock.elapsedRealtime());
		FreqAlarmReceiver.setAlarm(this, false);
	}
}
