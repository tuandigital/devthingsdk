package com.airpnp.ads.services;

import android.annotation.TargetApi;
import android.app.job.JobInfo;
import android.app.job.JobParameters;
import android.app.job.JobScheduler;
import android.app.job.JobService;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;

import com.airpnp.ads.core.UnifySDK;
import com.airpnp.core.utils.LogUtil;

@TargetApi(Build.VERSION_CODES.LOLLIPOP)
public class ScreenTrackingJobService extends JobService {

	private static final int JOB_ID = 11;
	private static boolean sServiceRunning;

	private BroadcastReceiver mScreenOnOffReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			String action = intent.getAction();
			if (action != null) {
                LogUtil.w("Screen: " + action);
				if (action.equals(Intent.ACTION_USER_PRESENT) || action.equals(Intent.ACTION_USER_UNLOCKED)) {
					UnifySDK.setLatestUnlocked(System.currentTimeMillis());
				} else if (action.equals(Intent.ACTION_SCREEN_OFF)) {
					UnifySDK.setLatestScreenOff(System.currentTimeMillis());
				}
			}
		}
	};

	public static boolean isServiceRunning(){
		return sServiceRunning;
	}

	@TargetApi(Build.VERSION_CODES.M)
	public static void enqueue(Context context){
		ComponentName componentName = new ComponentName(context, ScreenTrackingJobService.class);
		JobInfo.Builder builder = new JobInfo.Builder(JOB_ID, componentName);
		builder.setMinimumLatency(1000);
		builder.setOverrideDeadline(2000);
		builder.setRequiresCharging(false);
		JobScheduler jobScheduler = context.getSystemService(JobScheduler.class);
		if(jobScheduler != null){
			jobScheduler.schedule(builder.build());
		}
	}

	@Override
	public void onCreate() {
		sServiceRunning = true;
		super.onCreate();
		IntentFilter intentFilter = new IntentFilter();
		intentFilter.addAction(Intent.ACTION_SCREEN_OFF);
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
			intentFilter.addAction(Intent.ACTION_USER_UNLOCKED);
		}
		intentFilter.addAction(Intent.ACTION_USER_PRESENT);
		registerReceiver(mScreenOnOffReceiver, intentFilter);
	}

	@Override
	public boolean onStartJob(JobParameters jobParameters) {
		return true;
	}

	@Override
	public boolean onStopJob(JobParameters jobParameters) {
		return false;
	}

	@Override
	public void onDestroy() {
		markServiceAsStopped();
		super.onDestroy();
	}

	private void markServiceAsStopped(){
		sServiceRunning = false;
		unregisterReceiver(mScreenOnOffReceiver);
	}
}
