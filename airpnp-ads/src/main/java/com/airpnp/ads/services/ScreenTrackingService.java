package com.airpnp.ads.services;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.os.IBinder;

import com.airpnp.ads.core.UnifySDK;
import com.airpnp.core.utils.LogUtil;

public class ScreenTrackingService extends Service {

	private static boolean sServiceRunning;

	private BroadcastReceiver mScreenOnOffReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			String action = intent.getAction();
			if (action != null) {
//                LogUtil.w("Screen: " + action);
				if (action.equals(Intent.ACTION_USER_PRESENT) || action.equals(Intent.ACTION_USER_UNLOCKED)) {
					UnifySDK.setLatestUnlocked(System.currentTimeMillis());
				} else if (action.equals(Intent.ACTION_SCREEN_OFF)) {
					UnifySDK.setLatestScreenOff(System.currentTimeMillis());
				}
			}
		}
	};

	public static boolean isServiceRunning(){
		return sServiceRunning;
	}

	@Override
	public void onCreate() {
		super.onCreate();
		sServiceRunning = true;
		IntentFilter intentFilter = new IntentFilter();
		intentFilter.addAction(Intent.ACTION_SCREEN_OFF);
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
			intentFilter.addAction(Intent.ACTION_USER_UNLOCKED);
		}
		intentFilter.addAction(Intent.ACTION_USER_PRESENT);
		registerReceiver(mScreenOnOffReceiver, intentFilter);
	}

	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		return START_STICKY;
	}

	@Override
	public void onDestroy() {
		sServiceRunning = false;
		unregisterReceiver(mScreenOnOffReceiver);
		super.onDestroy();
	}
}
