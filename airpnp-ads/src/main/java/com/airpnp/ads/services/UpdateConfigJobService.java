package com.airpnp.ads.services;

import android.annotation.TargetApi;
import android.app.job.JobParameters;
import android.app.job.JobService;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.SystemClock;
import android.support.annotation.NonNull;

import com.airpnp.ads.receivers.UpdateConfigAlarmReceiver;
import com.airpnp.ads.storages.ExtrasConfig;
import com.airpnp.ads.storages.PrefManager;
import com.airpnp.ads.storages.RemoteConfig;

@TargetApi(Build.VERSION_CODES.LOLLIPOP)
public class UpdateConfigJobService extends JobService {

	@Override
	public boolean onStartJob(final JobParameters jobParameters) {
		try {
			PrefManager.getInstance().put(PrefManager.Keys.LAST_CONFIG_FETCHED, SystemClock.elapsedRealtime());
			RemoteConfig.getInstance().fetchFromServer(new RemoteConfig.RemoteConfigListener() {
				@Override
				public void onUpdateFinished() {
						jobFinished(jobParameters, false);
				}
			});

		} finally {
			UpdateConfigAlarmReceiver.setAlarm(this, false);
		}
		return true;
	}

	@Override
	public boolean onStopJob(JobParameters jobParameters) {
		return false;
	}
}
