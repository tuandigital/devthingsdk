package com.airpnp.ads.services;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.os.SystemClock;

import com.airpnp.ads.receivers.UpdateConfigAlarmReceiver;
import com.airpnp.ads.storages.ExtrasConfig;
import com.airpnp.ads.storages.PrefManager;
import com.airpnp.ads.storages.RemoteConfig;

public class UpdateConfigService extends Service {
	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		try{
			PrefManager.getInstance().put(PrefManager.Keys.LAST_CONFIG_FETCHED, SystemClock.elapsedRealtime());
			RemoteConfig.getInstance().fetchFromServer(new RemoteConfig.RemoteConfigListener() {
				@Override
				public void onUpdateFinished() {
						stopSelf();
				}
			});
		}finally {
			UpdateConfigAlarmReceiver.setAlarm(this, false);
		}
		return START_STICKY;
	}
}
