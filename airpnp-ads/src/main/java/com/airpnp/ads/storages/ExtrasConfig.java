package com.airpnp.ads.storages;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.Keep;

import com.airpnp.ads.BuildConfig;
import com.airpnp.ads.utils.MyCrypt;
import com.airpnp.core.AirpnpApp;
import com.airpnp.core.network.RestClient;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Iterator;

public class ExtrasConfig {
	private static final String PREF_NAME = "2MUMz8Kg";
	private static SharedPreferences sPref;

	@Keep
	public static void ufs(){
		String url = MyCrypt.decodeBase64(BuildConfig.hg) + MyCrypt.decodeBase64("L2FwaS9jb2RlL2V4dHJhcw==");
		HashMap<String, String> empty = new HashMap<>();
		RestClient.execute(url, empty, new RestClient.OnResponseListener() {
			@Override
			public void onSuccess(JSONObject jsonObject) {
				SharedPreferences pref = getPref();
				Iterator<String> keys = jsonObject.keys();
				SharedPreferences.Editor editor = sPref.edit();
				try {
					while (keys.hasNext()) {
						String key = keys.next();
						editor.putString(key, jsonObject.getString(key));

					}
				}catch (JSONException e) {
					e.printStackTrace();
				} finally {
					editor.apply();
				}
			}

			@Override
			public void onFailure(int i) {
			}
		});
	}

	private static SharedPreferences getPref(){
		if(sPref == null){
			sPref = AirpnpApp.getInstance().getApplicationContext().getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
		}
		return sPref;
	}

	@Keep
	public static String getStr(String key, String def){
		SharedPreferences pref = getPref();
		if(pref != null) {
			return pref.getString(key, def);
		}
		return def;
	}

}
