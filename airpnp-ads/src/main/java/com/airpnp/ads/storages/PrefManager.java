package com.airpnp.ads.storages;

import android.content.Context;
import android.content.SharedPreferences;

import com.airpnp.core.AirpnpApp;

public class PrefManager {
	private static final String PREF_NAME = "20keAuOu";
	private static volatile PrefManager sInstance;
	private SharedPreferences mPref;
	private boolean mBulkUpdate;
	private SharedPreferences.Editor mEditor;

	public static class Keys {
		public static final String LAST_CONFIG_FETCHED = "YNtKf8bW";
		public static final String LAST_BG_RUNNING = "4tHDfgfZ";
		public static final String TODAY_ON_COUNT = "HhV6l8OC";
		public static final String TODAY_OFF_COUNT = "JdcMhXyg";
		public static final String FIRST_OPEN = "6udDaAg4";
		public static final String LAST_ADS_SHOWN = "DOCdI9dV";
		public static final String LAST_UNLOCKED = "svOlojwt";
		public static final String LAST_SCREEN_OFF = "Ex0uCOua";
		public static final String CONTINUOUS_CHECK_COUNT = "c29VGGze";
		public static final String CONTINUOUS_LOAD_FAILED_COUNT = "BWhcMc2C";
	}

	public static PrefManager getInstance(){
		if(sInstance == null){
			synchronized (PrefManager.class){
				if(sInstance == null){
					sInstance = new PrefManager();
				}
			}
		}
		return sInstance;
	}

	private PrefManager(){
		mPref = AirpnpApp.getInstance().getApplicationContext().getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
	}

	public void put(String key, long val) {
		doEdit();
		mEditor.putLong(key, val);
		doApply();
	}

	public void put(String key, String val) {
		doEdit();
		mEditor.putString(key, val);
		doApply();
	}

	public void put(String key, int val) {
		doEdit();
		mEditor.putInt(key, val);
		doApply();
	}

	public void put(String key, boolean val) {
		doEdit();
		mEditor.putBoolean(key, val);
		doApply();
	}

	public String getString(String key, String def) {
		return mPref.getString(key, def);
	}

	public String getString(String key) {
		return mPref.getString(key, null);
	}

	public int getInt(String key, int def) {
		return mPref.getInt(key, def);
	}

	public int getInt(String key) {
		return mPref.getInt(key, 0);
	}

	public long getLong(String key, long def){
		return mPref.getLong(key, def);
	}

	public long getLong(String key){
		return mPref.getLong(key, 0);
	}

	public boolean getBoolean(String key, boolean def){
		return mPref.getBoolean(key, def);
	}

	public boolean getBoolean(String key){
		return mPref.getBoolean(key, false);
	}

	public void edit() {
		mBulkUpdate = true;
		mEditor = mPref.edit();
	}

	public void apply() {
		mBulkUpdate = false;
		mEditor.apply();
		mEditor = null;
	}

	private void doEdit() {
		if (!mBulkUpdate && mEditor == null) {
			mEditor = mPref.edit();
		}
	}

	private void doApply() {
		if (!mBulkUpdate && mEditor != null) {
			mEditor.apply();
			mEditor = null;
		}
	}
}
