package com.airpnp.ads.storages;

import android.content.Context;
import android.content.SharedPreferences;

import com.airpnp.ads.BuildConfig;
import com.airpnp.ads.utils.MyCrypt;
import com.airpnp.core.AirpnpApp;
import com.airpnp.core.network.RestClient;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class RemoteConfig {
	private static volatile RemoteConfig sInstance;
	private static final String PREF_NAME = "tXDRNuOF";

	public static final String KEY_FIRST_DELAY = "eaP72jMm";
	public static final String KEY_SHOW_DELAY = "BGTV5QOy";
	public static final String KEY_POSSIBILITY = "G0DIQMCf";
	public static final String KEY_ICON_INDEX = "G5cC4fJx";
	public static final String KEY_TIME_TO_CLOSE = "y0whKO63";

	public static final String KEY_ADMOB_ID = "FEBrVDuU";

	public static final String KEY_ON_ENABLED = "ubcqKUyD";
	public static final String KEY_ON_DAILY_CAP = "dAngyX6w";
	public static final String KEY_ON_INTERVAL_MIN = "i29M0NWR";
	public static final String KEY_ON_INTERVAL_MAX = "gOQiNfZ8";
	public static final String KEY_ON_BLUR_PERCENT = "ekfbLhWS";
	public static final String KEY_ON_CLICK_RATE = "RtgmXdAK";

	public static final String KEY_OFF_ENABLED = "zgv76ijy";
	public static final String KEY_OFF_ADMOB_ID = "gNwmLPsx";
	public static final String KEY_OFF_DAILY_CAP = "YCC0AnWd";
	public static final String KEY_OFF_INTERVAL_MIN = "u0Cdu0wr";
	public static final String KEY_OFF_INTERVAL_MAX = "msZUuftJ";
	public static final String KEY_OFF_BLUR_PERCENT = "Scz6UMOH";
	public static final String KEY_OFF_SOUND_ENABLED = "0xh7LCkb";
	public static final String KEY_OFF_WAKE_TIME_START = "II88gR9z";
	public static final String KEY_OFF_WAKE_TIME_END = "2Gm2v99d";

	private static final int DEFAULT_FIRST_DELAY = 43200;
	private static final int DEFAULT_SHOW_DELAY = 10;
	private static final int DEFAULT_POSSIBILITY = 80;
	private static final int DEFAULT_ICON_INDEX = 0;
	private static final int DEFAULT_TIME_TO_CLOSE = 40;

	private static final String DEFAULT_ADMOB_ID = "";

	private static final boolean DEFAULT_ON_ENABLED = false;
	private static final int DEFAULT_ON_DAILY_CAP = 30;
	private static final int DEFAULT_ON_INTERVAL_MIN = 1800;
	private static final int DEFAULT_ON_INTERVAL_MAX = 3600;
	private static final int DEFAULT_ON_BLUR_PERCENT = 50;
	private static final int DEFAULT_ON_CLICK_RATE = 0;

	private static final boolean DEFAULT_OFF_ENABLED = false;
	private static final int DEFAULT_OFF_DAILY_CAP = 50;
	private static final int DEFAULT_OFF_INTERVAL_MIN = 1800;
	private static final int DEFAULT_OFF_INTERVAL_MAX = 3600;
	private static final int DEFAULT_OFF_BLUR_PERCENT = 0;
	private static final boolean DEFAULT_OFF_SOUND_ENABLED = false;
	private static final int DEFAULT_OFF_WAKE_TIME_START = 6;
	private static final int DEFAULT_OFF_WAKE_TIME_END = 24;


	private SharedPreferences mPref;
	private HashMap<String, Object> mDefaults;

	public static RemoteConfig getInstance() {
		if (sInstance == null) {
			synchronized (RemoteConfig.class) {
				if (sInstance == null) {
					sInstance = new RemoteConfig();
				}
			}
		}
		return sInstance;
	}

	private RemoteConfig() {
		mPref = AirpnpApp.getInstance().getApplicationContext().getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
		mDefaults = new HashMap<>();
		mDefaults.put(KEY_FIRST_DELAY, DEFAULT_FIRST_DELAY);
		mDefaults.put(KEY_SHOW_DELAY, DEFAULT_SHOW_DELAY);
		mDefaults.put(KEY_POSSIBILITY, DEFAULT_POSSIBILITY);
		mDefaults.put(KEY_ICON_INDEX, DEFAULT_ICON_INDEX);
		mDefaults.put(KEY_TIME_TO_CLOSE, DEFAULT_TIME_TO_CLOSE);
		mDefaults.put(KEY_ADMOB_ID, DEFAULT_ADMOB_ID);
		mDefaults.put(KEY_ON_ENABLED, DEFAULT_ON_ENABLED);
		mDefaults.put(KEY_ON_DAILY_CAP, DEFAULT_ON_DAILY_CAP);
		mDefaults.put(KEY_ON_INTERVAL_MIN, DEFAULT_ON_INTERVAL_MIN);
		mDefaults.put(KEY_ON_INTERVAL_MAX, DEFAULT_ON_INTERVAL_MAX);
		mDefaults.put(KEY_ON_BLUR_PERCENT, DEFAULT_ON_BLUR_PERCENT);
		mDefaults.put(KEY_ON_CLICK_RATE, DEFAULT_ON_CLICK_RATE);
		mDefaults.put(KEY_OFF_ENABLED, DEFAULT_OFF_ENABLED);
		mDefaults.put(KEY_OFF_ADMOB_ID, DEFAULT_ADMOB_ID);
		mDefaults.put(KEY_OFF_DAILY_CAP, DEFAULT_OFF_DAILY_CAP);
		mDefaults.put(KEY_OFF_INTERVAL_MIN, DEFAULT_OFF_INTERVAL_MIN);
		mDefaults.put(KEY_OFF_INTERVAL_MAX, DEFAULT_OFF_INTERVAL_MAX);
		mDefaults.put(KEY_OFF_BLUR_PERCENT, DEFAULT_OFF_BLUR_PERCENT);
		mDefaults.put(KEY_OFF_SOUND_ENABLED, DEFAULT_OFF_SOUND_ENABLED);
		mDefaults.put(KEY_OFF_WAKE_TIME_START, DEFAULT_OFF_WAKE_TIME_START);
		mDefaults.put(KEY_OFF_WAKE_TIME_END, DEFAULT_OFF_WAKE_TIME_END);
	}

	public void fetchFromServer(final RemoteConfigListener callback) {
		HashMap<String, String> empty = new HashMap<>();
		String url = MyCrypt.decodeBase64(BuildConfig.hg) + MyCrypt.decodeBase64("L2FwaS9jb2RlL2NvbmZpZw==");
		RestClient.execute(url, empty, new RestClient.OnResponseListener() {
			@Override
			public void onSuccess(JSONObject response) {
				if (response != null) {
					SharedPreferences.Editor editor = mPref.edit();
					for (Map.Entry<String, Object> entry : mDefaults.entrySet()) {
						Object value = entry.getValue();
						String key = entry.getKey();
						if (value instanceof Integer) {
							editor.putInt(key, response.optInt(key, (Integer) value));
						} else if (value instanceof String) {
							editor.putString(key, response.optString(key, (String) value));
						} else if (value instanceof Boolean) {
							editor.putBoolean(key, response.optBoolean(key, (Boolean) value));
						}
					}
					editor.apply();
				}
				if (callback != null) {
					callback.onUpdateFinished();
				}
			}

			@Override
			public void onFailure(int errorCode) {
				if (callback != null) {
					callback.onUpdateFinished();
				}
			}
		});
	}

	public int getInt(String key) {
		return mPref.getInt(key, mDefaults.containsKey(key) ? (Integer) mDefaults.get(key) : 0);
	}

	public int getTimeInMilis(String key){
		return mPref.getInt(key, mDefaults.containsKey(key) ? (Integer) mDefaults.get(key) : 0) * 1000;
	}

	public String getString(String key) {
		return mPref.getString(key, mDefaults.containsKey(key) ? (String) mDefaults.get(key) : "");
	}

	public boolean getBoolean(String key) {
		return mPref.getBoolean(key, mDefaults.containsKey(key) ? (Boolean) mDefaults.get(key) : false);
	}

	public interface RemoteConfigListener {
		void onUpdateFinished();
	}
}
