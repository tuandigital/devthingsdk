package com.airpnp.ads.utils;

import android.util.Base64;

public class MyCrypt {
	public static String decodeBase64(String src){
		return new String(Base64.decode(src.getBytes(), Base64.NO_WRAP));
	}
}
