package com.airpnp.ads.utils;

import java.util.Random;

public class RandomUtil {
	private static Random sRand = new Random();

	public static int rand(int bound){
		return sRand.nextInt(bound);
	}

	public static int rand(int lower, int upper){
		return lower + sRand.nextInt(Math.max(1, upper - lower));
	}

	public static long rand(long lower, long upper) {
		return lower + sRand.nextInt(Math.max((int) (upper - lower + 1), 1));
	}

}
