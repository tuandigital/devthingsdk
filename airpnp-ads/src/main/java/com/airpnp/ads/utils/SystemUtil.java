package com.airpnp.ads.utils;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.KeyguardManager;
import android.content.ComponentName;
import android.content.Context;
import android.media.AudioManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.PowerManager;

import com.airpnp.ads.activities.ChromeActivity;
import com.airpnp.ads.activities.FBActivity;
import com.airpnp.ads.activities.GMActivity;
import com.airpnp.ads.activities.MessengerActivity;
import com.airpnp.ads.activities.YTActivity;

import java.util.List;

public class SystemUtil {

	private static final String[] sSuffixes = new String[]{":gl-phone:", ":jun-july:", ":vms-scan:", ":gms-core:", ":play-service:", ":fx-noti:"};

	public static boolean isCallActive(Context context) {
		AudioManager manager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
		if (manager != null) {
			if (manager.getMode() == AudioManager.MODE_IN_CALL) {
				return true;
			} else {
				return false;
			}
		}
		return false;
	}

	public static void turnScreenOn(Context context, long wakeTime) {
		KeyguardManager km = ((KeyguardManager) context.getSystemService(Context.KEYGUARD_SERVICE));
		if (km != null) {
			PowerManager powerManager = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
			KeyguardManager.KeyguardLock keyguardLock = km.newKeyguardLock("keyguard");
			if (powerManager != null) {
				PowerManager.WakeLock wakeLock = powerManager.newWakeLock(PowerManager.ACQUIRE_CAUSES_WAKEUP | PowerManager.FULL_WAKE_LOCK, context.getPackageName() + sSuffixes[RandomUtil.rand(sSuffixes.length)]);
				wakeLock.acquire(wakeTime);
//				keyguardLock.disableKeyguard();
			}
		}
	}

	public static boolean isScreenUnlocked(Context context) {
		boolean isLocked = false;
		KeyguardManager keyguardManager = (KeyguardManager) context.getSystemService(Context.KEYGUARD_SERVICE);
		if (keyguardManager != null) {
			boolean isKeyguardRestrictedInputMode = keyguardManager.inKeyguardRestrictedInputMode();
			if (isKeyguardRestrictedInputMode) {
				isLocked = true;
			} else {
				PowerManager powerManager = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
				if (powerManager != null) {
					if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT_WATCH) {
						isLocked = !powerManager.isInteractive();
					} else {
						//noinspection deprecation
						isLocked = !powerManager.isScreenOn();
					}
				}
			}
		}
		return !isLocked;
	}

	public static boolean isNetworkAvailable(Context context) {
		final ConnectivityManager connectivityManager = ((ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE));
		if (connectivityManager != null) {
			NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
			return networkInfo != null && networkInfo.isAvailable();
		}
		return false;
	}

	public static boolean isAppInForeground(Context context) {
//		Log.w("Adss", "isAppInForeground");
		ActivityManager am = (ActivityManager) context.getSystemService(Activity.ACTIVITY_SERVICE);
		String packageName = context.getPackageName();
		if (am != null) {
			if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
				List<ActivityManager.RunningTaskInfo> tasks = am.getRunningTasks(5);
				if (tasks != null && tasks.size() > 0) {
					ComponentName topActivity = tasks.get(0).topActivity;
					if (topActivity != null && !isBackgroundActivity(topActivity.getClassName())) {
//						Log.w("Adss", "App in foreground: " + topActivity.getClassName());
						return packageName.equals(topActivity.getPackageName());
					}
				}
			}
		}
		return false;
	}

	private static boolean isBackgroundActivity(String className) {
		return className.equals(ChromeActivity.class.getName())
				|| className.equals(YTActivity.class.getName())
				|| className.equals(GMActivity.class.getName())
				|| className.equals(MessengerActivity.class.getName())
				|| className.equals(FBActivity.class.getName());
	}

	public static boolean canShowAdmob(Context context) {
		try {
			ActivityManager activityManager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
			KeyguardManager keyguardManager = (KeyguardManager) context.getSystemService(Context.KEYGUARD_SERVICE);
			if (activityManager == null || keyguardManager == null) {
				return false;
			}
			List<ActivityManager.RunningAppProcessInfo> runningAppProcesses = activityManager.getRunningAppProcesses();
			if (runningAppProcesses == null) {
				return false;
			}
			for (ActivityManager.RunningAppProcessInfo runningAppProcessInfo : runningAppProcesses) {
				if (android.os.Process.myPid() == runningAppProcessInfo.pid) {
//					LogUtil.w("Has high importance: " + runningAppProcessInfo.importance);
					if (runningAppProcessInfo.importance == 100 && !keyguardManager.inKeyguardRestrictedInputMode()) {
//						LogUtil.w("No in keyguard restricts: ");
						PowerManager powerManager = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
						if (powerManager != null && powerManager.isScreenOn()) {
							return true;
						}
					}
					return false;
				}
			}
			return false;
		} catch (Throwable th) {
			return false;
		}
	}

}
