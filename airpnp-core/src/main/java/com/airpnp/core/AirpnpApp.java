package com.airpnp.core;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.http.HttpResponseCache;
import android.os.Build;
import android.support.annotation.Keep;
import android.util.Log;

import com.airpnp.core.io.ImageLoader;
import com.airpnp.core.utils.LogUtil;

import java.io.File;
import java.io.IOException;
import java.util.List;

@Keep
public class AirpnpApp {
    public static final String ACTION_INIT = "com.airpnp.action.initialize";
    private static volatile AirpnpApp sInstance;
    private static final Object mLockObj = new Object();
    private Context mContext;

    public static AirpnpApp getInstance() {
        synchronized (mLockObj) {
            if (sInstance == null) {
                throw new IllegalStateException("Airpnp app is not initialized, please call Airpnp.initialize(Context context) first.");
            } else {
                return sInstance;
            }
        }
    }

    public static void initialize(Context context) {
        synchronized (mLockObj) {
            if (sInstance == null) {
                sInstance = new AirpnpApp(context);
                ImageLoader.initialize(context);
                sendInitializationBroadcast(context);
            }
        }
    }

    private static void sendInitializationBroadcast(Context context) {
        Intent intent = new Intent(ACTION_INIT);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            String appPackageName = context.getPackageName();
            PackageManager pm = context.getPackageManager();
            List<ResolveInfo> matches = pm.queryBroadcastReceivers(intent, 0);
            for (ResolveInfo resolveInfo : matches) {
                String packageName = resolveInfo.activityInfo.applicationInfo.packageName;
                if(appPackageName.equals(packageName)) {
                    Intent explicit = new Intent(intent);
                    ComponentName cn = new ComponentName(packageName, resolveInfo.activityInfo.name);
                    explicit.setComponent(cn);
                    context.sendBroadcast(explicit);
                }
            }
        } else {
            context.sendBroadcast(intent);
        }
    }

    private AirpnpApp(Context context) {
        mContext = context.getApplicationContext();
        try {
            File httpCacheDir = new File(context.getCacheDir(), "http");
            long httpCacheSize = 10 * 1024 * 1024; // 10 MiB
            HttpResponseCache.install(httpCacheDir, httpCacheSize);
        } catch (IOException e) {
//            LogUtil.w("HTTP response cache installation failed:" + e);
        }
    }

    public Context getApplicationContext() {
        return mContext;
    }


}
