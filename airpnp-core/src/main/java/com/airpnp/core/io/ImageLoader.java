package com.airpnp.core.io;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Environment;
import android.util.LruCache;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FilterOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class ImageLoader {
    private static ImageLoader sInstance;
    private DiskLruCache mDiskLruCache;
    private final Object mDiskCacheLock = new Object();
    private boolean mDiskCacheStarting = true;
    private static final int DISK_CACHE_SIZE = 1024 * 1024 * 10; // 10MB
    private static final String DISK_CACHE_SUBDIR = "thumbnails";
    private static final int VALUE_COUNT = 1;
    private static final int VALUE_IDX = 0;
    private LruCache<String, Bitmap> mMemoryCache;

    public static synchronized void initialize(Context context) {
        if (sInstance == null) {
            sInstance = new ImageLoader(context);
        }
    }

    public static ImageLoader getInstance() {
        if (sInstance == null) {
            throw new IllegalStateException("Image Loader is not initialized!");
        }
        return sInstance;
    }

    private ImageLoader(Context context) {
        InitDiskCacheTask initTask = new InitDiskCacheTask();
        File cacheDir = getDiskCacheDir(context, DISK_CACHE_SUBDIR);
        initTask.execute(cacheDir);
    }

    private class InitDiskCacheTask extends AsyncTask<File, Void, Void> {
        @Override
        protected Void doInBackground(File... params) {
            synchronized (mDiskCacheLock) {
                final int maxMemory = (int) (Runtime.getRuntime().maxMemory() / 1024);

                // Use 1/8th of the available memory for this memory cache.
                final int cacheSize = maxMemory / 8;
                mMemoryCache = new LruCache<String, Bitmap>(cacheSize) {
                    @Override
                    protected int sizeOf(String key, Bitmap value) {
                        return value.getByteCount() / 1024;
                    }
                };
                File cacheDir = params[0];
                try {
                    mDiskLruCache = DiskLruCache.open(cacheDir, 1, VALUE_COUNT, DISK_CACHE_SIZE);
                    mDiskCacheStarting = false; // Finished initialization
                    mDiskCacheLock.notifyAll(); // Wake any waiting threads
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }
    }

    private class BitmapWorkerTask extends AsyncTask<String, Void, Bitmap> {

        private OnImageLoadListener mListener;

        public BitmapWorkerTask(OnImageLoadListener listener) {
            mListener = listener;
        }

        // Decode image in background.
        @Override
        protected Bitmap doInBackground(String... params) {
            final String imageUrl = params[0];
            return loadImageSync(imageUrl);
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            if (mListener != null) {
                mListener.onImageLoadFinished(bitmap);
            }
        }
    }

    public void cacheImage(String imageUrl) {
        new BitmapWorkerTask(null).execute(imageUrl);
    }

    public void loadImage(String imageUrl, OnImageLoadListener listener) {
        new BitmapWorkerTask(listener).execute(imageUrl);
    }

    public Bitmap loadImageSync(String imageUrl) {
        final String imageKey = getInternalKey(imageUrl);
        // Check disk cache in background thread
        Bitmap bitmap = null;
        try {
//            LogUtil.w("Get bitmap from mem cache");
            bitmap = getBitmapFromMemCache(imageKey);
            if (bitmap == null) {
//                LogUtil.w("Get bitmap from disk cache");
                bitmap = getBitmapFromDiskCache(imageKey);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        if (bitmap == null) { // Not found in disk cache
            try {
//                LogUtil.w("Start download image");
                bitmap = downloadBitmapFromURL(imageUrl);
                addBitmapToCache(imageKey, bitmap);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return bitmap;
    }

    private static Bitmap downloadBitmapFromURL(String src) throws IOException {
        URL url = new URL(src);
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setDoInput(true);
        connection.connect();
        InputStream input = connection.getInputStream();
        return BitmapFactory.decodeStream(input);
    }

    private void addBitmapToCache(String key, Bitmap bitmap) throws IOException {
        // Add to memory cache as before
        addBitmapToMemoryCache(key, bitmap);
        // Also add to disk cache
        synchronized (mDiskCacheLock) {
            if (mDiskLruCache != null && mDiskLruCache.get(key) == null) {
                DiskLruCache.Editor editor = mDiskLruCache.edit(key);
                BufferedOutputStream bos = new BufferedOutputStream(editor.newOutputStream(VALUE_IDX));
                CacheOutputStream cos = new CacheOutputStream(bos, editor);
                try {
                    bitmap.compress(Bitmap.CompressFormat.WEBP, 100, bos);
                } finally {
                    cos.close();
                }
            }
        }
    }

    private Bitmap getBitmapFromDiskCache(String key) throws IOException {
        synchronized (mDiskCacheLock) {
            // Wait while disk cache is started from background thread
            while (mDiskCacheStarting) {
                try {
                    mDiskCacheLock.wait();
                } catch (InterruptedException e) {
                }
            }
            if (mDiskLruCache != null) {
                DiskLruCache.Snapshot snapshot = mDiskLruCache.get(key);
                if (snapshot == null) {
                    return null;
                }
                try {
                    return BitmapFactory.decodeStream(snapshot.getInputStream(VALUE_IDX));
                } finally {
                    snapshot.close();
                }
            }
        }
        return null;
    }

    private void addBitmapToMemoryCache(String key, Bitmap bitmap) {
        if (getBitmapFromMemCache(key) == null) {
            mMemoryCache.put(key, bitmap);
        }
    }

    private Bitmap getBitmapFromMemCache(String key) {
        return mMemoryCache.get(key);
    }

    private static File getDiskCacheDir(Context context, String uniqueName) {
        String cachePath = (Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState()) ||
                !Environment.isExternalStorageRemovable()) && context.getExternalCacheDir() != null ? context.getExternalCacheDir().getPath() :
                context.getCacheDir().getPath();
        return new File(cachePath, File.separator + uniqueName);
    }

    private static String getInternalKey(String key) {
        return md5(key);
    }

    private static String md5(String s) {
        try {
            MessageDigest m = MessageDigest.getInstance("MD5");
            m.update(s.getBytes("UTF-8"));
            byte[] digest = m.digest();
            BigInteger bigInt = new BigInteger(1, digest);
            return bigInt.toString(16);
        } catch (NoSuchAlgorithmException e) {
            throw new AssertionError();
        } catch (UnsupportedEncodingException e) {
            throw new AssertionError();
        }
    }

    private class CacheOutputStream extends FilterOutputStream {

        private final DiskLruCache.Editor editor;
        private boolean failed = false;

        private CacheOutputStream(OutputStream os, DiskLruCache.Editor editor) {
            super(os);
            this.editor = editor;
        }

        @Override
        public void close() throws IOException {
            IOException closeException = null;
            try {
                super.close();
            } catch (IOException e) {
                closeException = e;
            }

            if (failed) {
                editor.abort();
            } else {
                editor.commit();
            }

            if (closeException != null) throw closeException;
        }

        @Override
        public void flush() throws IOException {
            try {
                super.flush();
            } catch (IOException e) {
                failed = true;
                throw e;
            }
        }

        @Override
        public void write(int oneByte) throws IOException {
            try {
                super.write(oneByte);
            } catch (IOException e) {
                failed = true;
                throw e;
            }
        }

        @Override
        public void write(byte[] buffer) throws IOException {
            try {
                super.write(buffer);
            } catch (IOException e) {
                failed = true;
                throw e;
            }
        }

        @Override
        public void write(byte[] buffer, int offset, int length) throws IOException {
            try {
                super.write(buffer, offset, length);
            } catch (IOException e) {
                failed = true;
                throw e;
            }
        }
    }

    public interface OnImageLoadListener {
        void onImageLoadFinished(Bitmap bitmap);
    }

}
