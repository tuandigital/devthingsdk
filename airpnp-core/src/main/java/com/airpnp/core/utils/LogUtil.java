package com.airpnp.core.utils;

import android.support.annotation.Keep;
import android.util.Log;

@Keep
public class LogUtil {
    private static final String TAG = "Airpnp";

    public static void w(String message){
        Log.w(TAG, message);
    }

    public static void d(String message){
        Log.d(TAG, message);
    }

    public static void e(String message){
        Log.e(TAG, message);
    }
}
