package com.pisen.android.ads.admediationsdksample;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.devthing.sdk.ads.InterstitialAd;
import com.devthing.sdk.ads.RewardedVideoAd;
import com.devthing.sdk.ads.RewardedVideoAdListener;
import com.devthing.sdk.ads.view.BannerAdView;
import com.devthing.sdk.base.ads.adapters.BaseBannerAdapter;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity implements RewardedVideoAdListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        BannerAdView bannerAdView = findViewById(R.id.ad_view);
        bannerAdView.setAdSize(BaseBannerAdapter.ADSIZE_BANNER);
        bannerAdView.loadAd();
        final InterstitialAd interstitialAd = new InterstitialAd(this);
        Button btnLoadAds =  findViewById(R.id.load_ad);
        btnLoadAds.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                interstitialAd.loadAd();
            }
        });
        Button btnShowAds = findViewById(R.id.show_ads);
        btnShowAds.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                interstitialAd.show();
            }
        });

        final RewardedVideoAd rewardedVideoAd = new RewardedVideoAd(this);
        rewardedVideoAd.setRewardedVideoAdListener(this);

        Button btnLoadRewardedAd = findViewById(R.id.load_rewarded_ad);
        btnLoadRewardedAd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rewardedVideoAd.loadAd();
            }
        });

        Button btnShowRewardedAd = findViewById(R.id.show_rewarded_ads);
        btnShowRewardedAd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rewardedVideoAd.show();
            }
        });
//
//        NativeAd nativeAd = new NativeAd(this);
//        nativeAd.setNativeAdListener(this);
//        nativeAd.loadAd();
    }

    @Override
    public void onRewardedAdLoaded() {

    }

    @Override
    public void onRewardedAdLoadFailed() {

    }

    @Override
    public void onRewardedAdClosed() {

    }

    @Override
    public void onRewardedAdDisplayed() {

    }

    @Override
    public void onRewardedAdDisplayFailed() {

    }

    @Override
    public void onRewardSuccess() {

    }

//    @Override
//    public void onRewardSuccess(int rewardItem) {
////        SdkLogger.debug("Rewarded Success : " + rewardItem);
//    }
//
//    @Override
//    public void onRewardedAdLoaded() {
//
//    }
//
//    @Override
//    public void onRewardedAdLoadFailed() {
//
//    }
//
//    @Override
//    public void onRewardedAdClosed() {
//
//    }
//
//    @Override
//    public void onRewardedAdDisplayed() {
//
//    }
//
//    @Override
//    public void onAdLoaded(NativeAd ad) {
//        FrameLayout adContainer = findViewById(R.id.ads_container);
//        adContainer.removeAllViews();
//        View adView = ad.render(new NativeViewBinder.Builder(R.layout.native_ads)
//        .titleId(R.id.ads_title)
//        .iconImageId(R.id.ads_icon)
//        .callToActionId(R.id.ads_install)
//        .adChoiceImageId(R.id.ad_choice)
//        .build());
//        if(adView != null) {
//            adContainer.addView(adView);
//        }
//    }
//
//    @Override
//    public void onAdLoadFailed() {
//
//    }
//
//    @Override
//    public void onAdClicked() {
//
//    }
//
//    public void setupTaskDescription() {
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//            Bitmap bmp = Bitmap.createBitmap(48, 48, Bitmap.Config.ARGB_4444);
//            ActivityManager.TaskDescription description = new ActivityManager.TaskDescription("   ", bmp);
//            setTaskDescription(description);
//        }
//        //will care for all posts
//        Handler mHandler = new Handler();
//        mHandler.postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
//                    if (!isDestroyed() && isFinishing()) {
//                        finish();
//                    }
//                }
//            }
//        }, 60000);
//    }

}
