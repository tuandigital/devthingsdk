package com.metric.milano.veget;

import android.app.ActivityManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;

/**
 * Created by tuandigital on 2/23/18.
 */

public class AkmaiReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        if(!isMyServiceRunning(context, UniversalService.class)) {
            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){
                uk.scheduleJob(context);
            } else{
                Intent trackingIntent = new Intent(context, UniversalService.class);
                context.startService(trackingIntent);
            }
        }
        if("com.highnu.kowkgas.coment".equals(intent.getAction())){
            uk.getInstance(context).updateOnlyLastShown();
        }
    }

    private boolean isMyServiceRunning(Context context, Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        if (manager != null) {
            for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
                if (serviceClass.getName().equals(service.service.getClassName())) {
                    return true;
                }
            }
        }
        return false;
    }
}
