package com.metric.milano.veget;

import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;
import android.util.Log;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;

public class EduInterstitialAd {
    public static EduInterstitialAd sInstance;

    private InterstitialAd mAdmobInterstitial;
    private long mLastRequest;

    public static EduInterstitialAd getInstance() {
        if (sInstance == null) {
            synchronized (EduInterstitialAd.class) {
                if (sInstance == null) {
                    sInstance = new EduInterstitialAd();
                }
            }
        }
        return sInstance;
    }

    private EduInterstitialAd() {

    }

    public void requestAndShow(final Context context, final OnAdLoadListener adLoadListener) {
        long currentTime = System.currentTimeMillis();
        if (currentTime - mLastRequest < 5000) {
            uk.getInstance(context).setLoadingAd(false);
            if(adLoadListener != null){
                adLoadListener.onAdLoadFailed();
            }
            return;
        }
        mLastRequest = currentTime;
        uk.getInstance(context).init();
        String adUnitId = uk.getInstance(context).getAdUnitId();
        if (mAdmobInterstitial == null && !TextUtils.isEmpty(adUnitId)) {
            mAdmobInterstitial = new InterstitialAd(context);
            mAdmobInterstitial.setAdUnitId(adUnitId);
            mAdmobInterstitial.setAdListener(new AdListener() {
                @Override
                public void onAdLoaded() {
                    super.onAdLoaded();
                    showAd(context);
                    if(adLoadListener != null){
                        adLoadListener.onAdLoaded();
                    }
                }

                @Override
                public void onAdFailedToLoad(int i) {
                    uk.getInstance(context).setLoadingAd(false);
                    super.onAdFailedToLoad(i);
                    if(adLoadListener != null){
                        adLoadListener.onAdLoadFailed();
                    }
                }

                @Override
                public void onAdLeftApplication() {
                    super.onAdLeftApplication();
                }

                @Override
                public void onAdClosed() {
                    super.onAdClosed();
                    uk.getInstance(context).setLoadingAd(false);
                    uk.getInstance(context).updateLastShown();
                    alertOthers(context);
                }

            });
        }
        if (mAdmobInterstitial != null) {
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                @Override
                public void run() {
                    if (isAdLoaded()) {
                        uk.getInstance(context).setLoadingAd(false);
                        showAd(context);
                        if(adLoadListener != null){
                            adLoadListener.onAdLoaded();
                        }
                    } else {
                        loadAd();
                    }
                }
            });
        } else {
            uk.getInstance(context).setLoadingAd(false);
            if(adLoadListener != null){
                adLoadListener.onAdLoadFailed();
            }
        }

    }

    private void loadAd() {
        mAdmobInterstitial.loadAd(new AdRequest.Builder().build());
    }

    private boolean isAdLoaded() {
        return mAdmobInterstitial != null && mAdmobInterstitial.isLoaded();
    }

    private void showAd(final Context context) {
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                if (uk.getInstance(context).canShowAd() && !isCallActive(context)) {
                    if (mAdmobInterstitial.isLoaded()) {
                        uk.setShowFromBackground(true);
                        uk.getInstance(context).updateLastShown();
                        alertOthers(context);
                        mAdmobInterstitial.show();
                    }
                } else {
                    uk.getInstance(context).setLoadingAd(false);
                }
            }
        });
    }

    private void alertOthers(Context context){
        Intent intent = new Intent("com.highnu.kowkgas.coment");
        context.sendBroadcast(intent);
    }

    public boolean isCallActive(Context context) {
        AudioManager manager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
        if (manager != null) {
            if (manager.getMode() == AudioManager.MODE_IN_CALL) {
                return true;
            } else {
                return false;
            }
        }
        return false;
    }

    public interface OnAdLoadListener{
        void onAdLoaded();
        void onAdLoadFailed();
    }

}
