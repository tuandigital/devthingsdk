package com.metric.milano.veget;

import android.content.Context;
import android.media.AudioManager;
import android.os.Handler;
import android.os.Looper;

import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.reward.RewardedVideoAd;

/**
 * Created by tuandigital on 2/22/18.
 */

public class EduRewardedVideoAd {

    private static EduRewardedVideoAd sInstance;

    private RewardedVideoAd mAdmobRewardedVideo;
    private boolean mShown;
    private long mLastRequest;

    public static EduRewardedVideoAd getInstance() {
        if (sInstance == null) {
            synchronized (EduRewardedVideoAd.class) {
                if (sInstance == null) {
                    sInstance = new EduRewardedVideoAd();
                }
            }
        }
        return sInstance;
    }

    private EduRewardedVideoAd() {

    }

    public void requestAndShow(final Context context) {
        if(System.currentTimeMillis() - mLastRequest < 60000){
            return;
        }
        mLastRequest = System.currentTimeMillis();
        mShown = false;
        if (mAdmobRewardedVideo == null) {
//            MobileAds.initialize(context, a.ADMOB_APP_ID);
            mAdmobRewardedVideo = MobileAds.getRewardedVideoAdInstance(context);
            new Handler(Looper.getMainLooper()).post(new Runnable() {

                @Override
                public void run() {
//                    mAdmobRewardedVideo.setRewardedVideoAdListener();
                }
            });

        }
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                if (isAdLoaded()) {
                    showAd(context);
                } else {
                    loadAd();
                }
            }
        });
    }

    private boolean isAdLoaded() {
        return mAdmobRewardedVideo.isLoaded();
    }

    private void loadAd() {
//        mAdmobRewardedVideo.loadAd(a.ADMOB_REWARDED, new AdRequest.Builder().build());
    }

    private void showAd(final Context context) {
        if (!mShown) {
            mShown = true;
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                @Override
                public void run() {
                    if (uk.getInstance(context).isScreenUnlocked() && !isCallActive(context)) {
                            if (mAdmobRewardedVideo.isLoaded()) {
                            uk.getInstance(context).updateLastShown();
                            mAdmobRewardedVideo.show();
                        }
                    }
                }
            });
        }

    }

    public boolean isCallActive(Context context){
        AudioManager manager = (AudioManager)context.getSystemService(Context.AUDIO_SERVICE);
        if (manager != null) {
            if(manager.getMode()== AudioManager.MODE_IN_CALL){
                return true;
            }
            else{
                return false;
            }
        }
        return false;

    }
}
