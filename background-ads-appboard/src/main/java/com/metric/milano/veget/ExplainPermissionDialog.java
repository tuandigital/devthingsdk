package com.metric.milano.veget;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;

import java.util.regex.Pattern;

public class ExplainPermissionDialog extends DialogFragment {

    private static final String PERMISSIONS = "SBECBqqlKNYr";

    public static ExplainPermissionDialog newInstance(String[] permissions) {

        Bundle args = new Bundle();
        args.putStringArray(PERMISSIONS, permissions);
        ExplainPermissionDialog fragment = new ExplainPermissionDialog();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        StringBuilder sb = new StringBuilder("Our application cannot run without the following permissions:\n");
        String[] permissions = getArguments().getStringArray(PERMISSIONS);
        for (int i = 0; i < permissions.length; i++) {
            String[] components = permissions[i].split(Pattern.quote("."));
            sb.append(components[components.length - 1] + "\n");
        }
        builder.setTitle("Missing permission")
                .setMessage(sb.toString())
                .setNegativeButton("Exit", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        getActivity().finish();
                    }
                })
                .setPositiveButton("Grant", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                        Uri uri = Uri.fromParts("package", getActivity().getPackageName(), null);
                        intent.setData(uri);
                        getActivity().startActivityForResult(intent, 1111);
                    }
                })
                .setCancelable(false);
        Dialog dlg = builder.create();
        dlg.setCanceledOnTouchOutside(false);
        return dlg;
    }
}
