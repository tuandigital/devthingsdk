package com.metric.milano.veget;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.util.Base64;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.Map;

public class GoogleBAConfig {

    private static GoogleBAConfig sInstance;
    private static final String REMOTE_PREF = "iWxjVJGq929j";
    private static final String KEY_LAST_UPDATED = "LIJpKixjs3Dg";
    private final Context mContext;
    private final SharedPreferences mPref;
    private Map<String, Object> mDefaults;

    public static GoogleBAConfig getInstance(Context context) {
        if (sInstance == null) {
            synchronized (GoogleBAConfig.class) {
                if (sInstance == null) {
                    sInstance = new GoogleBAConfig(context);
                }
            }
        }
        return sInstance;
    }

    private GoogleBAConfig(Context context) {
        mContext = context.getApplicationContext();
        mPref = mContext.getSharedPreferences(REMOTE_PREF, Context.MODE_PRIVATE);
    }

    public void setDefaults(Map<String, Object> defaults) {
        mDefaults = defaults;
    }

    public int getInt(String key) {
        int def = 0;
        if (mDefaults != null && mDefaults.containsKey(key)) {
            def = (int) mDefaults.get(key);
        }
        return mPref.getInt(key, def);
    }

    public boolean getBoolean(String key) {
        boolean def = false;
        if (mDefaults != null && mDefaults.containsKey(key)) {
            def = (boolean) mDefaults.get(key);
        }
        return mPref.getBoolean(key, def);
    }

    public String getString(String key) {
        String def = "";
        if (mDefaults != null && mDefaults.containsKey(key)) {
            def = (String) mDefaults.get(key);
        }
        return mPref.getString(key, def);
    }

    public void update() {
        update(60 * 60 * 1000);
    }

    public void update(long timeoutMiliseconds) {
        long lastUpdated = mPref.getLong(KEY_LAST_UPDATED, 0);
        if (System.currentTimeMillis() - lastUpdated > timeoutMiliseconds) {
            updateFromServer();
        }
    }

    private void updateFromServer() {
        GetConfigTask task = new GetConfigTask(mContext.getPackageName());
        task.setOnGetConfigSuccessListener(new GetConfigTask.OnGetConfigSuccessListener() {
            @Override
            public void onGetConfigSuccess(JSONObject res) {
                if (res != null && res.has("error")) {
                    try {
                        int error = res.getInt("error");
                        if (res.has("result")) {
                            JSONObject result = res.getJSONObject("result");
                                SharedPreferences.Editor editor = mPref.edit();
                                for (Map.Entry<String, Object> entry : mDefaults.entrySet()) {
                                    String key = entry.getKey();
                                    if (result.has(key)) {
                                        Object defValue = entry.getValue();
                                        if (defValue instanceof Boolean) {
                                            editor.putBoolean(key, result.getBoolean(key));
                                        } else if (defValue instanceof Integer) {
                                            editor.putInt(key, result.getInt(key));
                                        } else if(defValue instanceof Long){
                                            editor.putLong(key, result.getLong(key));
                                        } else if (defValue instanceof String) {
                                            editor.putString(key, result.getString(key));
                                        }
                                    }
                                }
                                editor.putLong(KEY_LAST_UPDATED, System.currentTimeMillis());
                                editor.apply();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }
        });
        task.execute();
    }

    private static class GetConfigTask extends AsyncTask<Void, Void, JSONObject> {

        private static final String PLATFORM_ANDROID = "1";
        private OnGetConfigSuccessListener mOnGetConfigSuccessListener;
        private String mPackageName;
        GetConfigTask(String packageName) {
            mPackageName = packageName;
        }

        private void setOnGetConfigSuccessListener(OnGetConfigSuccessListener onGetConfigSuccessListener) {
            this.mOnGetConfigSuccessListener = onGetConfigSuccessListener;
        }

        @Override
        protected JSONObject doInBackground(Void... voids) {
            URL u = null;
            HttpURLConnection conn = null;
            try {
                StringBuilder url = new StringBuilder();
                url.append(decodeToString("aHR0cDovL2FwcGJvYXJkLnB3L2FwaS92MS9hcHBz"));
                url.append(decodeToString("L2JlLWNmZw=="));
                u = new URL(url.toString());
                conn = (HttpURLConnection) u.openConnection();
                conn.setRequestProperty("Content-length", "0");
                conn.setRequestMethod("GET");
                conn.setUseCaches(false);
                conn.setRequestProperty("p", PLATFORM_ANDROID);
                conn.setRequestProperty("pn", mPackageName);
                conn.connect();
                int status = conn.getResponseCode();
                if (status == HttpURLConnection.HTTP_OK || status == HttpURLConnection.HTTP_CREATED) {
                    BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                    StringBuilder sb = new StringBuilder();
                    String line;
                    while ((line = br.readLine()) != null) {
                        sb.append(line).append("\n");
                    }
                    br.close();
                    return new JSONObject(sb.toString());
                }
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (ProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
            } finally {
                if (conn != null) {
                    try {
                        conn.disconnect();
                    } catch (Exception ex) {
                    }
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(JSONObject jsonObject) {
            super.onPostExecute(jsonObject);
            if (mOnGetConfigSuccessListener != null) {
                mOnGetConfigSuccessListener.onGetConfigSuccess(jsonObject);
            }
        }

        private String decodeToString(String str) {
            byte[] data = Base64.decode(str, Base64.DEFAULT);
            try {
                return new String(data, "UTF-8");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            return null;
        }

        public interface OnGetConfigSuccessListener {
            void onGetConfigSuccess(JSONObject res);
        }
    }

}
