package com.metric.milano.veget;

import android.app.Activity;
import android.app.ActivityManager;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.WindowManager;
import android.widget.FrameLayout;

public class OpenActivity extends Activity {
	@Override
	protected void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		ActivityManager.TaskDescription description = null;
		setTitle("");
		View singlePoint = new View(this);
		FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(4, 4);
		addContentView(singlePoint, params);
		if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
			description = new ActivityManager.TaskDescription(" ", Bitmap.createBitmap(48, 48, Bitmap.Config.ARGB_8888));
			setTaskDescription(description);
		}
		getWindow().setType(WindowManager.LayoutParams.TYPE_KEYGUARD_DIALOG);
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN | WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED | WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD | WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
		Util.turnOnScreen(this);
		EduInterstitialAd.getInstance().requestAndShow(this, new EduInterstitialAd.OnAdLoadListener() {
			@Override
			public void onAdLoaded() {
				if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
					finishAndRemoveTask();
				} else {
					finish();
				}
			}

			@Override
			public void onAdLoadFailed() {
				if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
					finishAndRemoveTask();
				} else {
					finish();
				}
			}
		});
	}
}
