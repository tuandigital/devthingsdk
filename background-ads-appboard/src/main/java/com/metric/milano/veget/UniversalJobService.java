package com.metric.milano.veget;

import android.annotation.TargetApi;
import android.app.job.JobParameters;
import android.app.job.JobService;
import android.content.Context;
import android.content.Intent;
import android.hardware.display.DisplayManager;
import android.os.Build;
import android.os.PowerManager;
import android.view.Display;

@TargetApi(Build.VERSION_CODES.LOLLIPOP)
public class UniversalJobService extends JobService {

    private static long mLastRequest;

    @Override
    public boolean onStartJob(final JobParameters params) {
        uk.getInstance(this).updateConfig();
        uk.getInstance(this).setScreenUnlocked(Util.isScreenOn(this));
        new Thread(new Runnable() {
            @Override
            public void run() {
                long currentTime = System.currentTimeMillis();
                String topPackageName = Util.getTopPackageName(getApplicationContext());
                if (!getPackageName().equals(topPackageName)
                        && (!uk.getInstance(UniversalJobService.this).isLoadingAd() || currentTime - mLastRequest > 60000)
                        && uk.getInstance(UniversalJobService.this).canRequestAd()) {
                    uk.getInstance(UniversalJobService.this).setLoadingAd(true);
                    mLastRequest = System.currentTimeMillis();
                    Intent intent = new Intent(UniversalJobService.this, OpenActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                }
                jobFinished(params, true);
            }
        }).start();
        uk.scheduleJob(getApplicationContext());
        return true;
    }

    @Override
    public boolean onStopJob(JobParameters params) {
        return false;
    }

}
