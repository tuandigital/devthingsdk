package com.metric.milano.veget;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.hardware.display.DisplayManager;
import android.os.Build;
import android.os.IBinder;
import android.os.PowerManager;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.Display;

import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by tuandigital on 2/23/18.
 */

public class UniversalService extends Service {

    private static final int REQUEST_CODE = 5416354;
    private BroadcastReceiver mScreenOnOffReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action != null) {
                if (action.equals(Intent.ACTION_USER_PRESENT)) {
                    uk.getInstance(context).setScreenUnlocked(true);
                    mLastestScreenUnlocked = System.currentTimeMillis();
                } else if (action.equals(Intent.ACTION_SCREEN_OFF)) {
                    uk.getInstance(context).setScreenUnlocked(false);
                }
                uk.getInstance(context).updateConfig();
            }
        }
    };
    private Timer mTimer;
    private long mLastRequest;
    private long mLastestScreenUnlocked;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        uk.getInstance(this).updateConfig();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(Intent.ACTION_SCREEN_OFF);
        intentFilter.addAction(Intent.ACTION_SCREEN_ON);
        intentFilter.addAction(Intent.ACTION_USER_PRESENT);
        registerReceiver(mScreenOnOffReceiver, intentFilter);
        setAlarm();
        uk.getInstance(this).setScreenUnlocked(Util.isScreenOn(this));
        mTimer = new Timer();
        mTimer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
//                Log.w("Alog", "Start timer");
                long currentTime = System.currentTimeMillis();
                String topPackageName = Util.getTopPackageName(getApplicationContext());
                if (!getPackageName().equals(topPackageName)
                        && (!uk.getInstance(UniversalService.this).isLoadingAd() || currentTime - mLastRequest > 60000)
                        && (uk.getInstance(UniversalService.this).isLaunchBackground() || currentTime - mLastestScreenUnlocked > uk.getInstance(UniversalService.this).getShowDelay())
                        && uk.getInstance(UniversalService.this).canRequestAd()) {
//					Log.w("Alog", "Start requets");
                    uk.getInstance(UniversalService.this).setLoadingAd(true);
                    mLastRequest = System.currentTimeMillis();
                    Intent intent = new Intent(UniversalService.this, OpenActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                }
            }
        }, 0, 10000);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        unregisterReceiver(mScreenOnOffReceiver);
        mTimer.cancel();
        super.onDestroy();
    }

    private void setAlarm() {
        AlarmManager alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
        Intent intent = new Intent(getApplicationContext(), AkmaiReceiver.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(getApplicationContext(), REQUEST_CODE, intent, PendingIntent.FLAG_CANCEL_CURRENT);
        if (alarmManager != null) {
            alarmManager.setRepeating(AlarmManager.RTC, System.currentTimeMillis(), 10000, pendingIntent);
        }
    }

}
