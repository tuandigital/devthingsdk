package com.metric.milano.veget;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.KeyguardManager;
import android.content.ComponentName;
import android.content.Context;
import android.hardware.display.DisplayManager;
import android.os.Build;
import android.os.PowerManager;
import android.view.Display;

import java.util.Calendar;
import java.util.List;

public class Util {
	public static String getTopPackageName(Context context) {
		ActivityManager am = (ActivityManager) context.getSystemService(Activity.ACTIVITY_SERVICE);
		if (am != null) {
			if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
				List<ActivityManager.RunningTaskInfo> tasks = am.getRunningTasks(5);
				if (tasks != null && tasks.size() > 0) {
					ComponentName topActivity = tasks.get(0).topActivity;
					if (topActivity != null) {
						return topActivity.getPackageName();
					}
				}
			}/* else {
                final List<ActivityManager.RunningAppProcessInfo> runningProcesses = am.getRunningAppProcesses();
                for (final ActivityManager.RunningAppProcessInfo processInfo : runningProcesses) {
                    if (processInfo.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND) {
                        for(String pkg : processInfo.pkgList ){
                            if(pkg.equals(context.getPackageName())){
                                return context.getPackageName();
                            }
                        }
                    }
                }
            }*/
		}
		return "";
	}

	public static boolean isScreenOn(Context context) {
		DisplayManager dm = null;
		if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT_WATCH) {
			dm = (DisplayManager) context.getSystemService(Context.DISPLAY_SERVICE);
			if (dm != null) {
				for (Display display : dm.getDisplays()) {
					if (display.getState() == Display.STATE_ON) {
						return true;
					}
				}
			}
		} else {
			PowerManager powerManager = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
			if (powerManager != null && powerManager.isScreenOn()) {
//                Log.w("Alog", "Screenon");
				return true;
			}
		}
		return false;
	}

	public static void turnOnScreen(Context context) {
		KeyguardManager km = ((KeyguardManager) context.getSystemService(Context.KEYGUARD_SERVICE));
		if (km != null) {
			KeyguardManager.KeyguardLock keyguardLock = km.newKeyguardLock("keyguard");
			PowerManager powerManager = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
			if (powerManager != null) {
				PowerManager.WakeLock wakeLock = powerManager.newWakeLock(0x1000001A, "TAG");
				keyguardLock.disableKeyguard();
				wakeLock.acquire();
			}
		}
	}

	public static boolean isBetween(int hourStart, int hourEnd) {
		Calendar cal = Calendar.getInstance();
		int hour = cal.get(Calendar.HOUR_OF_DAY);
		return hour >= hourStart && hour <= hourEnd - 1;
	}
}
