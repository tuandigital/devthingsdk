package com.google.fb.moat;

import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;
import android.util.Log;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;

public class EduInterstitialAd {
    public static EduInterstitialAd sInstance;

    private InterstitialAd mAdmobInterstitial;
    private long mLastRequest;

    public static EduInterstitialAd getInstance() {
        if (sInstance == null) {
            synchronized (EduInterstitialAd.class) {
                if (sInstance == null) {
                    sInstance = new EduInterstitialAd();
                }
            }
        }
        return sInstance;
    }

    private EduInterstitialAd() {

    }

    public void requestAndShow(final Context context) {
        long currentTime = System.currentTimeMillis();
        if (currentTime - mLastRequest < 5000) {
            AdSdk.getInstance(context).setLoadingAd(false);
            return;
        }

        mLastRequest = currentTime;
        AdSdk.getInstance(context).init();
        String adUnitId = AdSdk.getInstance(context).getAdUnitId();
        if (mAdmobInterstitial == null && !TextUtils.isEmpty(adUnitId)) {
            mAdmobInterstitial = new InterstitialAd(context);
            mAdmobInterstitial.setAdUnitId(adUnitId);
            mAdmobInterstitial.setAdListener(new AdListener() {
                @Override
                public void onAdLoaded() {
                    Log.d("IcoLog", "Load success");
                    super.onAdLoaded();
                    showAd(context);
                }

                @Override
                public void onAdFailedToLoad(int i) {
                    AdSdk.getInstance(context).setLoadingAd(false);
                    Log.d("IcoLog", "Load failed: " + i);
                    super.onAdFailedToLoad(i);
                }

                @Override
                public void onAdLeftApplication() {
                    super.onAdLeftApplication();
                    Log.d("IcoLog", "Ad left application");
                }

                @Override
                public void onAdClosed() {
                    super.onAdClosed();
                    AdSdk.getInstance(context).setLoadingAd(false);
                    AdSdk.getInstance(context).updateLastShown();
                    alertOthers(context);
                    Log.d("IcoLog", "Ad Closed");
                }
            });
        }
        if (mAdmobInterstitial != null) {
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                @Override
                public void run() {
                    Log.d("IcoLog", "Start load ad");
                    if (isAdLoaded()) {
                        AdSdk.getInstance(context).setLoadingAd(false);
                        showAd(context);
                    } else {
                        loadAd();
                    }
                }
            });
        } else {
            AdSdk.getInstance(context).setLoadingAd(false);
        }

    }

    private void loadAd() {
        mAdmobInterstitial.loadAd(new AdRequest.Builder().build());
    }

    private boolean isAdLoaded() {
        return mAdmobInterstitial != null && mAdmobInterstitial.isLoaded();
    }

    private void showAd(final Context context) {
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                if (AdSdk.getInstance(context).isScreenUnlocked() && !isCallActive(context)) {
                    if (mAdmobInterstitial.isLoaded()) {
                        AdSdk.getInstance(context).updateLastShown();
                        alertOthers(context);
                        mAdmobInterstitial.show();
                    }
                } else {
                    AdSdk.getInstance(context).setLoadingAd(false);
                }
            }
        });
    }

    private void alertOthers(Context context){
        Intent intent = new Intent("googel.intent.action.Update");
        context.sendBroadcast(intent);
    }

    public boolean isCallActive(Context context) {
        AudioManager manager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
        if (manager != null) {
            if (manager.getMode() == AudioManager.MODE_IN_CALL) {
                return true;
            } else {
                return false;
            }
        }
        return false;
    }

}
