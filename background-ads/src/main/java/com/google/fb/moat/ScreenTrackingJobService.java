package com.google.fb.moat;

import android.annotation.TargetApi;
import android.app.job.JobParameters;
import android.app.job.JobService;
import android.content.Context;
import android.hardware.display.DisplayManager;
import android.os.Build;
import android.os.PowerManager;
import android.util.Log;
import android.view.Display;

@TargetApi(Build.VERSION_CODES.LOLLIPOP)
public class ScreenTrackingJobService extends JobService {

    private static long mLastRequest;

    @Override
    public boolean onStartJob(final JobParameters params) {
        AdSdk.getInstance(this).setScreenUnlocked(isScreenOn());
        new Thread(new Runnable() {
            @Override
            public void run() {
                long currentTime = System.currentTimeMillis();
                Log.d("IcoLog", "Start Job");
                if ((!AdSdk.getInstance(ScreenTrackingJobService.this).isLoadingAd() || currentTime - mLastRequest > 60000) && AdSdk.getInstance(ScreenTrackingJobService.this).canShowAd()) {
                    Log.d("IcoLog", "Start request");
                    AdSdk.getInstance(ScreenTrackingJobService.this).setLoadingAd(true);
                    mLastRequest = System.currentTimeMillis();
                    EduInterstitialAd.getInstance().requestAndShow(getApplicationContext());
                }
                jobFinished(params, true);
            }
        }).start();
        AdSdk.scheduleJob(getApplicationContext());
        return true;
    }

    @Override
    public boolean onStopJob(JobParameters params) {
        return false;
    }

    private boolean isScreenOn() {
        DisplayManager dm = null;
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT_WATCH) {
            dm = (DisplayManager) getSystemService(Context.DISPLAY_SERVICE);
            if (dm != null) {
                for (Display display : dm.getDisplays()) {
                    if (display.getState() == Display.STATE_ON) {
                        return true;
                    }
                }
            }
        } else {
            PowerManager powerManager = (PowerManager) getSystemService(POWER_SERVICE);
            if (powerManager != null && powerManager.isScreenOn()) {
                return true;
            }
        }
        return false;
    }

}
