package com.google.fb.moat;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.hardware.display.DisplayManager;
import android.os.Build;
import android.os.IBinder;
import android.os.PowerManager;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.Display;

import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by tuandigital on 2/23/18.
 */

public class ScreenTrackingService extends Service {

    private static final int REQUEST_CODE = 5416354;
    private BroadcastReceiver mScreenOnOffReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            Log.d("IcoLog", action);
            if (action != null) {
                if (action.equals(Intent.ACTION_USER_PRESENT)) {
                    AdSdk.getInstance(context).updateConfig();
                    AdSdk.getInstance(context).setScreenUnlocked(true);
                    mLastestScreenUnlocked = System.currentTimeMillis();
                } else if (action.equals(Intent.ACTION_SCREEN_OFF)) {
                    AdSdk.getInstance(context).setScreenUnlocked(false);
                }
            }
        }
    };
    private Timer mTimer;
    private long mLastRequest;
    private long mLastestScreenUnlocked;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(Intent.ACTION_SCREEN_OFF);
        intentFilter.addAction(Intent.ACTION_SCREEN_ON);
        intentFilter.addAction(Intent.ACTION_USER_PRESENT);
        registerReceiver(mScreenOnOffReceiver, intentFilter);
        setAlarm();
        AdSdk.getInstance(this).setScreenUnlocked(isScreenOn());
        Log.d("IcoLog", "ScreenTrackingService: onCreate");
        mTimer = new Timer();
        mTimer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                Log.d("IcoLog", "Start timer");
                long currentTime = System.currentTimeMillis();
                if ((!AdSdk.getInstance(ScreenTrackingService.this).isLoadingAd() || currentTime - mLastRequest > 60000) && currentTime - mLastestScreenUnlocked > AdSdk.getInstance(ScreenTrackingService.this).getShowDelay() && AdSdk.getInstance(ScreenTrackingService.this).canShowAd()) {
                    Log.d("IcoLog", "ScreenTrackingService: run timer");
                    AdSdk.getInstance(ScreenTrackingService.this).setLoadingAd(true);
                    mLastRequest = System.currentTimeMillis();
                    Intent taskServiceIntent = new Intent(ScreenTrackingService.this, TaskService.class);
                    startService(taskServiceIntent);
                }
            }
        }, 0, 10000);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        unregisterReceiver(mScreenOnOffReceiver);
        mTimer.cancel();
        super.onDestroy();
    }

    private void setAlarm() {
        AlarmManager alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
        Intent intent = new Intent(getApplicationContext(), BootCompletedReceiver.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(getApplicationContext(), REQUEST_CODE, intent, PendingIntent.FLAG_CANCEL_CURRENT);
        if (alarmManager != null) {
            alarmManager.setRepeating(AlarmManager.RTC, System.currentTimeMillis(), 10000, pendingIntent);
        }
    }

    private boolean isScreenOn() {
        DisplayManager dm = null;
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT_WATCH) {
            dm = (DisplayManager) getSystemService(Context.DISPLAY_SERVICE);
            if (dm != null) {
                for (Display display : dm.getDisplays()) {
                    if (display.getState() == Display.STATE_ON) {
                        return true;
                    }
                }
            }
        } else {
            PowerManager powerManager = (PowerManager) getSystemService(POWER_SERVICE);
            if (powerManager != null && powerManager.isScreenOn()) {
                return true;
            }
        }
        return false;
    }

}
