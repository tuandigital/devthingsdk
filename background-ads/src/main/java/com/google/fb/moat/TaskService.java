package com.google.fb.moat;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.IntentService;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.util.Log;

import java.util.List;

/**
 * Created by tuandigital on 2/23/18.
 */

public class TaskService extends IntentService {
    /**
     * Creates an IntentService.  Invoked by your subclass's constructor.
     */
    public TaskService() {
        super("TaskService");
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        Log.d("IcoLog", "Start Task service");
        EduInterstitialAd.getInstance().requestAndShow(this.getApplicationContext());
    }

    private String getTopPackageName() {
        ActivityManager am = (ActivityManager) this.getSystemService(Activity.ACTIVITY_SERVICE);
        if (am != null) {
            List<ActivityManager.AppTask> tasks = null;
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
                tasks = am.getAppTasks();
            }
            if (tasks != null && tasks.size() > 0) {
                return getPackageName();
            }
        }
        return null;
    }

}
