package com.devthing.sdk;

import android.content.Context;

import com.devthing.sdk.main.BuildConfig;
import com.devthing.sdk.network.RestClient;
import com.devthing.sdk.utils.MediationPrefUtil;
import com.devthing.sdk.utils.SdkLogger;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class MediationSdk {
	private static boolean sIsInitialized;
	private static Context sAppContext;
	private static JSONObject sMediationConfig;
	private static List<OnAppConfigChangeListener> sAppConfigChangeListeners;

	public static synchronized void initialize(Context context) {
		if (!sIsInitialized) {
			sAppContext = context.getApplicationContext();
			sAppConfigChangeListeners = new ArrayList<>();
			sIsInitialized = true;
		}
	}

	public static synchronized boolean isInitialized() {
		return sIsInitialized;
	}

	public static Context getAppContext() {
		return sAppContext;
	}

	public static JSONObject getMediationConfig() {
		if (sMediationConfig == null) {
			String savedMediationConfig = MediationPrefUtil.getInstance(sAppContext).getString(MediationPrefUtil.KEY_MEDIATION_CONFIG, null);
			if (savedMediationConfig != null) {
				try {
					sMediationConfig = new JSONObject(savedMediationConfig);
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
		}
		return sMediationConfig;
	}

	public static synchronized void addAppConfigChangeListener(OnAppConfigChangeListener listener) {
		if (sAppConfigChangeListeners == null) {
			sAppConfigChangeListeners = new ArrayList<>();
		}
		if (!sAppConfigChangeListeners.contains(listener)) {
			sAppConfigChangeListeners.add(listener);
		}
	}

	public static synchronized void removeAppConfigListener(OnAppConfigChangeListener listener) {
		if (sAppConfigChangeListeners != null) {
			if (sAppConfigChangeListeners.contains(listener)) {
				sAppConfigChangeListeners.remove(listener);
			}
		}
	}

	public static void requestMediationConfig() {
		RestClient.execute(BuildConfig.MEDIATION_API, new RestClient.OnResponseListener() {
			@Override
			public void onSuccess(JSONObject response) {
				if (response != null) {
//					SdkLogger.debug(response.toString());
					updateMediationConfig(response);
				}
			}

			@Override
			public void onFailure(int errorCode) {
				SdkLogger.debug("error : " + errorCode);
			}
		});
	}

	private static void updateMediationConfig(JSONObject mediationConfig) {
		MediationPrefUtil.getInstance(sAppContext).putString(MediationPrefUtil.KEY_MEDIATION_CONFIG, mediationConfig.toString());
		for (int i = 0; i < sAppConfigChangeListeners.size(); i++) {
			sAppConfigChangeListeners.get(i).onAppConfigChanged();
		}
	}

	public interface OnAppConfigChangeListener {
		void onAppConfigChanged();
	}
}
