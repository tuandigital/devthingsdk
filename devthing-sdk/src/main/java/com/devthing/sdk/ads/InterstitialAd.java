package com.devthing.sdk.ads;

import android.content.Context;
import android.text.TextUtils;

import com.devthing.sdk.MediationSdk;
import com.devthing.sdk.base.ads.adapters.BaseInterstitialAdapter;
import com.devthing.sdk.base.ads.listeners.InternalInterstitialAdListener;
import com.devthing.sdk.utils.CommonConstants;
import com.devthing.sdk.utils.Constants;
import com.devthing.sdk.utils.SdkLogger;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map;

import androidx.annotation.Keep;

public class InterstitialAd implements InternalInterstitialAdListener, MediationSdk.OnAppConfigChangeListener {

	private static final HashMap<String, String> INTERSTITIAL_ADAPTER_MAP;

	static {
		INTERSTITIAL_ADAPTER_MAP = new HashMap<>();
		INTERSTITIAL_ADAPTER_MAP.put(Constants.NETWORK_ADMOB, "AdmobInterstitialAdapter");
		INTERSTITIAL_ADAPTER_MAP.put(Constants.NETWORK_FAN, "FanInterstitialAdapter");
		INTERSTITIAL_ADAPTER_MAP.put(Constants.NETWORK_UNITY, "UnityInterstitialAdapter");
	}

	private Context mContext;
	private InterstitialAdListener mInterstitialAdListener;
	private int mCurrentAdNetworkIndex;
	private BaseInterstitialAdapter mCurrentInterstitialAdapter;
	private boolean mAdLoaded;
	private boolean mAdLoading;

	@Keep
	public InterstitialAd(Context context) {
		mContext = context;
	}

	@Keep
	public void setInterstitialAdListener(InterstitialAdListener listener) {
		mInterstitialAdListener = listener;
	}

	@Keep
	public boolean isLoaded() {
		return mAdLoaded;
	}

	@Keep
	public synchronized void loadAd() {
		MediationSdk.addAppConfigChangeListener(this);
		mCurrentAdNetworkIndex = 0;
		loadNextAdNetwork();
	}

	private void loadNextAdNetwork() {
		if (mCurrentInterstitialAdapter != null) {
			mCurrentInterstitialAdapter.destroy();
			mCurrentInterstitialAdapter = null;
		}
		JSONObject mediationConfig = MediationSdk.getMediationConfig();
		if (mediationConfig != null) {
			try {
				JSONArray interstitials = mediationConfig.getJSONArray(Constants.CONFIG_KEY_INTERSTITIALS);
				if (interstitials != null && mCurrentAdNetworkIndex < interstitials.length()) {
					JSONObject interstitial = interstitials.getJSONObject(mCurrentAdNetworkIndex);
					String network = interstitial.getString(Constants.CONFIG_KEY_NETWORK);
					JSONObject general = mediationConfig.getJSONObject(Constants.CONFIG_KEY_GENERAL);
					if (!TextUtils.isEmpty(network) && INTERSTITIAL_ADAPTER_MAP.containsKey(network)) {
						String adapterClassName = Constants.ADAPTER_PACKAGE + INTERSTITIAL_ADAPTER_MAP.get(network);
						try {
							Class adapterClass = Class.forName(adapterClassName);
							Constructor ctor = adapterClass.getConstructor(Context.class);
							mCurrentInterstitialAdapter = (BaseInterstitialAdapter) ctor.newInstance(mContext);
							Map<String, Object> options = new HashMap<>();
							options.put(CommonConstants.OPT_ADMOB_APP_ID, general.optString(Constants.CONFIG_KEY_ADMOB_APP_ID));
							options.put(CommonConstants.OPT_UNITY_GAME_ID, general.optString(Constants.CONFIG_KEY_UNITY_GAME_ID));
							mCurrentInterstitialAdapter.setInternalInterstitialAdListener(this);
							mCurrentInterstitialAdapter.loadAd(interstitial.optString(Constants.CONFIG_KEY_PLACEMENT), options);
							mAdLoading = true;
						} catch (ClassNotFoundException e) {
							mCurrentAdNetworkIndex++;
							loadNextAdNetwork();
						} catch (NoSuchMethodException e) {
							mCurrentAdNetworkIndex++;
							loadNextAdNetwork();
							e.printStackTrace();
						} catch (IllegalAccessException e) {
							mCurrentAdNetworkIndex++;
							loadNextAdNetwork();
							e.printStackTrace();
						} catch (InstantiationException e) {
							mCurrentAdNetworkIndex++;
							loadNextAdNetwork();
							e.printStackTrace();
						} catch (InvocationTargetException e) {
							mCurrentAdNetworkIndex++;
							loadNextAdNetwork();
							e.printStackTrace();
						}
					} else {
						mCurrentAdNetworkIndex++;
						loadNextAdNetwork();
					}
				} else {
					onAdLoadFailedCallback();
				}
			} catch (JSONException e) {
				e.printStackTrace();
				onAdLoadFailedCallback();
			}
		} else {
			onAdLoadFailedCallback();
		}
	}

	@Keep
	public void show() {
		if (mAdLoaded && mCurrentInterstitialAdapter != null) {
			mAdLoaded = false;
			mCurrentInterstitialAdapter.show();
		} else {
			mAdLoaded = false;
			onAdDisplayFailedCallback();
		}
	}

	@Override
	public void onAdLoaded(BaseInterstitialAdapter adapter) {
		SdkLogger.debug("Load success: " + adapter.getClass().getSimpleName() + " placement: " + adapter.getPlacementId());
		mAdLoaded = true;
		mAdLoading = false;
	}

	@Override
	public void onAdDisplayed(BaseInterstitialAdapter adapter) {
		onAdDisplayedCallback();
	}

	@Override
	public void onAdClosed(BaseInterstitialAdapter adapter) {
		onAdClosedCallback();
	}

	@Override
	public void onAdLoadFailed(BaseInterstitialAdapter adapter, int errorCode) {
		SdkLogger.debug("Load failed: " + adapter.getClass().getSimpleName() + "error Code:" + errorCode);
		mAdLoading = false;
		mAdLoaded = false;
		mCurrentAdNetworkIndex++;
		loadNextAdNetwork();
	}

	@Override
	public void onAdClicked(BaseInterstitialAdapter baseInterstitialAdapter) {
	}

	@Override
	public void onAppConfigChanged() {
		if (!mAdLoaded && !mAdLoading) {
			loadAd();
		}
	}

	@Keep
	public void destroy() {
		mInterstitialAdListener = null;
		if(mCurrentInterstitialAdapter != null){
			mCurrentInterstitialAdapter.destroy();
			mCurrentInterstitialAdapter = null;
		}
	}

	private void onAdLoadedCallback() {
		if (mInterstitialAdListener != null) {
			mInterstitialAdListener.onInterstitialAdLoaded();
		}
	}

	private void onAdLoadFailedCallback() {
		if (mInterstitialAdListener != null) {
			mInterstitialAdListener.onInterstitialAdLoadFailed();
		}
	}

	private void onAdClosedCallback() {
		if (mInterstitialAdListener != null) {
			mInterstitialAdListener.onInterstitialAdClosed();
		}
	}

	private void onAdDisplayedCallback() {
		if (mInterstitialAdListener != null) {
			mInterstitialAdListener.onInterstitialAdDisplayed();
		}
	}

	private void onAdDisplayFailedCallback() {
		if (mInterstitialAdListener != null) {
			mInterstitialAdListener.onInterstitialAdDisplayFailed();
		}
	}

}
