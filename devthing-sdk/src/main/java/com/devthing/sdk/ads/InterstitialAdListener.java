package com.devthing.sdk.ads;


import androidx.annotation.Keep;

@Keep
public interface InterstitialAdListener {
    void onInterstitialAdLoaded();
    void onInterstitialAdLoadFailed();
    void onInterstitialAdClosed();
    void onInterstitialAdDisplayed();
    void onInterstitialAdDisplayFailed();
}
