package com.devthing.sdk.ads;

import android.content.Context;
import android.text.TextUtils;

import com.devthing.sdk.MediationSdk;
import com.devthing.sdk.base.ads.adapters.BaseRewardedAdapter;
import com.devthing.sdk.base.ads.listeners.InternalRewardedAdListener;
import com.devthing.sdk.utils.CommonConstants;
import com.devthing.sdk.utils.Constants;
import com.devthing.sdk.utils.SdkLogger;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map;

import androidx.annotation.Keep;

public class RewardedVideoAd implements InternalRewardedAdListener, MediationSdk.OnAppConfigChangeListener {

    private static final HashMap<String, String> REWARDED_ADAPTER_MAP;

    static {
        REWARDED_ADAPTER_MAP = new HashMap<>();
        REWARDED_ADAPTER_MAP.put(Constants.NETWORK_ADMOB, "AdmobRewardedAdapter");
        REWARDED_ADAPTER_MAP.put(Constants.NETWORK_FAN, "FanRewardedAdapter");
        REWARDED_ADAPTER_MAP.put(Constants.NETWORK_UNITY, "UnityRewardedAdapter");
    }

    private Context mContext;
    private RewardedVideoAdListener mRewardedAdListener;
    private int mCurrentAdNetworkIndex;
    private BaseRewardedAdapter mCurrentRewardedAdapter;
    private boolean mAdLoaded;
    private boolean mAdLoading;

    @Keep
    public RewardedVideoAd(Context context) {
        mContext = context;
    }

    @Keep
    public void setRewardedVideoAdListener(RewardedVideoAdListener listener) {
        mRewardedAdListener = listener;
    }

    @Keep
    public boolean isLoaded() {
        return mAdLoaded;
    }

    @Keep
    public synchronized void loadAd() {
        MediationSdk.addAppConfigChangeListener(this);
        mCurrentAdNetworkIndex = 0;
        loadAdInternal();
    }

    private void loadAdInternal() {
        if(mCurrentRewardedAdapter != null){
            mCurrentRewardedAdapter.destroy();
            mCurrentRewardedAdapter = null;
        }
        JSONObject mediationConfig = MediationSdk.getMediationConfig();
        if (mediationConfig != null) {
            try {
                JSONArray rewardeds = mediationConfig.getJSONArray(Constants.CONFIG_KEY_REWARDEDS);
                if(rewardeds != null && mCurrentAdNetworkIndex < rewardeds.length()){
                    JSONObject rewarded = rewardeds.getJSONObject(mCurrentAdNetworkIndex);
                    String network = rewarded.getString(Constants.CONFIG_KEY_NETWORK);
                    JSONObject general = mediationConfig.getJSONObject(Constants.CONFIG_KEY_GENERAL);
                    if(!TextUtils.isEmpty(network) && REWARDED_ADAPTER_MAP.containsKey(network)){
                        String adapterClassName = Constants.ADAPTER_PACKAGE + REWARDED_ADAPTER_MAP.get(network);
                        try {
                            Class adapterClass = Class.forName(adapterClassName);
                            Constructor ctor = adapterClass.getConstructor(Context.class);
                            mCurrentRewardedAdapter = (BaseRewardedAdapter) ctor.newInstance(mContext);
                            mCurrentRewardedAdapter.setInternalRewardedAdListener(this);
                            Map<String, Object> options = new HashMap<>();
                            options.put(CommonConstants.OPT_ADMOB_APP_ID, general.optString(Constants.CONFIG_KEY_ADMOB_APP_ID));
                            options.put(CommonConstants.OPT_UNITY_GAME_ID, general.optString(Constants.CONFIG_KEY_UNITY_GAME_ID));
                            mCurrentRewardedAdapter.loadAd(rewarded.optString(Constants.CONFIG_KEY_PLACEMENT), options);
                            mAdLoading = true;
                        } catch (ClassNotFoundException e) {
                            mCurrentAdNetworkIndex++;
                            loadAdInternal();
                            e.printStackTrace();
                        } catch (InstantiationException e) {
                            mCurrentAdNetworkIndex++;
                            loadAdInternal();
                            e.printStackTrace();
                        } catch (InvocationTargetException e) {
                            mCurrentAdNetworkIndex++;
                            loadAdInternal();
                            e.printStackTrace();
                        } catch (NoSuchMethodException e) {
                            mCurrentAdNetworkIndex++;
                            loadAdInternal();
                            e.printStackTrace();
                        } catch (IllegalAccessException e) {
                            mCurrentAdNetworkIndex++;
                            loadAdInternal();
                            e.printStackTrace();
                        }
                    } else {
                        mCurrentAdNetworkIndex++;
                        loadAdInternal();
                    }
                } else {
                    onAdLoadFailedCallback();
                }
            } catch (JSONException e) {
                onAdLoadFailedCallback();
                e.printStackTrace();
            }
        } else {
            onAdLoadFailedCallback();
        }
    }

    @Keep
    public void show() {
        if(mAdLoaded && mCurrentRewardedAdapter != null){
            mAdLoaded = false;
            mCurrentRewardedAdapter.show();
        } else {
            onAdDisplayFailedCallback();
        }
    }

    private void internalShow() {
    }

    public void resume(Context context) {
    }

    public void pause(Context context) {
    }

    @Keep
    public void destroy() {
        MediationSdk.removeAppConfigListener(this);
    }

    @Override
    public void onAppConfigChanged() {
        if(!mAdLoaded && !mAdLoading) {
            loadAd();
        }
    }

    @Override
    public void onRewardSuccess(BaseRewardedAdapter adapter) {
        onAdRewardSuccessCallback();
    }

    @Override
    public void onAdLoaded(BaseRewardedAdapter adapter) {
        SdkLogger.debug("Load rewarded success: " + adapter.getClass().getSimpleName());
        mAdLoaded = true;
        mAdLoading = false;
        onAdLoadedCallback();
    }

    @Override
    public void onAdDisplayed(BaseRewardedAdapter adapter) {
        onAdDisplayedCallback();
    }

    @Override
    public void onAdClosed(BaseRewardedAdapter adapter) {
        onAdClosedCallback();
    }

    @Override
    public void onAdLoadFailed(BaseRewardedAdapter adapter, int errorCode) {
        SdkLogger.debug("Load rewarded failed: " + adapter.getClass().getSimpleName() + "errorCode: " + errorCode);
        mAdLoading = false;
        mAdLoaded = false;
        onAdLoadFailedCallback();
        mCurrentAdNetworkIndex++;
        loadAdInternal();
    }

    @Override
    public void onAdClicked(BaseRewardedAdapter baseRewardedAdapter) {
    }

    @Override
    public void onAdDisplayFailed(BaseRewardedAdapter adapter) {
        onAdDisplayFailedCallback();
    }

    private void onAdLoadFailedCallback(){
        if(mRewardedAdListener != null){
            mRewardedAdListener.onRewardedAdLoadFailed();
        }
    }

    private void onAdLoadedCallback(){
        if(mRewardedAdListener != null){
            mRewardedAdListener.onRewardedAdLoaded();
        }
    }

    private void onAdRewardSuccessCallback(){
        if(mRewardedAdListener != null){
            mRewardedAdListener.onRewardSuccess();
        }
    }

    private void onAdClosedCallback(){
        if(mRewardedAdListener != null){
            mRewardedAdListener.onRewardedAdClosed();
        }
    }

    private void onAdDisplayedCallback(){
        if(mRewardedAdListener != null){
            mRewardedAdListener.onRewardedAdDisplayed();
        }
    }

    private void onAdDisplayFailedCallback(){
        if(mRewardedAdListener != null){
            mRewardedAdListener.onRewardedAdDisplayFailed();
        }
    }
}
