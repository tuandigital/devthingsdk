package com.devthing.sdk.ads;

import androidx.annotation.Keep;

@Keep
public interface RewardedVideoAdListener {
    void onRewardedAdLoaded();

    void onRewardedAdLoadFailed();

    void onRewardedAdClosed();

    void onRewardedAdDisplayed();

    void onRewardedAdDisplayFailed();

    void onRewardSuccess();
}
