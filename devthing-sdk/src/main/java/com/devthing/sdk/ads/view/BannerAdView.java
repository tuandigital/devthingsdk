package com.devthing.sdk.ads.view;

import android.content.Context;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.widget.FrameLayout;

import com.devthing.sdk.MediationSdk;
import com.devthing.sdk.base.ads.adapters.BaseBannerAdapter;
import com.devthing.sdk.base.ads.listeners.InternalBannerAdListener;
import com.devthing.sdk.utils.CommonConstants;
import com.devthing.sdk.utils.Constants;
import com.devthing.sdk.utils.SdkLogger;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map;

import androidx.annotation.Keep;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;

public class BannerAdView extends FrameLayout implements InternalBannerAdListener, MediationSdk.OnAppConfigChangeListener {

	private static final long RELOAD_PERIOD = 30000;

	private String mAdSize;
	private BaseBannerAdapter mCurrentBannerAdapter;
	private int mCurrentAdNetworkIndex;
	private boolean mNeedToShowAd;
	private boolean mAdLoaded;

	private static final HashMap<String, String> BANNER_ADAPTER_MAP;

	static {
		BANNER_ADAPTER_MAP = new HashMap<>();
		BANNER_ADAPTER_MAP.put(Constants.NETWORK_ADMOB, "AdmobBannerAdapter");
		BANNER_ADAPTER_MAP.put(Constants.NETWORK_FAN, "FanBannerAdapter");
	}

	private Runnable mScheduleReload = new Runnable() {
		@Override
		public void run() {
			loadAd();
		}
	};

	private Handler mMainHandler;

	@Keep
	public BannerAdView(@NonNull Context context) {
		super(context);
		setup();
	}

	@Keep
	public BannerAdView(@NonNull Context context, @Nullable AttributeSet attrs) {
		super(context, attrs);
		setup();
	}

	@Keep
	public BannerAdView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);
		setup();
	}

	@RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
	public BannerAdView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
		super(context, attrs, defStyleAttr, defStyleRes);
		setup();
	}

	private void setup() {
		mMainHandler = new Handler(Looper.getMainLooper());
		mAdSize = BaseBannerAdapter.ADSIZE_BANNER;
	}

	@Keep
	public void setAdSize(@BaseBannerAdapter.DTSizeDef String adSize) {
		this.mAdSize = adSize;
	}

	@Keep
	public void loadAd() {
		MediationSdk.addAppConfigChangeListener(this);
		mCurrentAdNetworkIndex = 0;
		loadNextAdNetwork();
	}

	@Keep
	public void show() {
		mNeedToShowAd = true;
		new Handler(Looper.getMainLooper()).post(new Runnable() {
			@Override
			public void run() {
				setVisibility(VISIBLE);
			}
		});
	}

	@Keep
	public void hide() {
		mNeedToShowAd = false;
		new Handler(Looper.getMainLooper()).post(new Runnable() {
			@Override
			public void run() {
				setVisibility(GONE);
			}
		});
	}

	private void loadNextAdNetwork() {
		if (mCurrentBannerAdapter != null) {
			mCurrentBannerAdapter.destroy();
			mCurrentBannerAdapter = null;
		}
		JSONObject mediationConfig = MediationSdk.getMediationConfig();
		if (mediationConfig != null) {
			try {
				JSONArray banners = mediationConfig.getJSONArray(Constants.CONFIG_KEY_BANNERS);
				if (mCurrentAdNetworkIndex < banners.length()) {
					JSONObject banner = banners.getJSONObject(mCurrentAdNetworkIndex);
					String network = banner.getString(Constants.CONFIG_KEY_NETWORK);
					JSONObject general = mediationConfig.getJSONObject(Constants.CONFIG_KEY_GENERAL);
					if (!TextUtils.isEmpty(network) && BANNER_ADAPTER_MAP.containsKey(network)) {
						String adapterClassName = Constants.ADAPTER_PACKAGE + BANNER_ADAPTER_MAP.get(network);
						try {
							Class adapterClass = Class.forName(adapterClassName);
							Constructor ctor = adapterClass.getConstructor(Context.class);
							mCurrentBannerAdapter = (BaseBannerAdapter) ctor.newInstance(getContext());
							Map<String, Object> options = new HashMap<>();
							options.put(BaseBannerAdapter.OPT_ADSIZE, mAdSize);
							options.put(CommonConstants.OPT_ADMOB_APP_ID, general.optString(Constants.CONFIG_KEY_ADMOB_APP_ID));
							options.put(CommonConstants.OPT_UNITY_GAME_ID, general.optString(Constants.CONFIG_KEY_UNITY_GAME_ID));
							mCurrentBannerAdapter.setInternalAdListener(this);
							View adView = mCurrentBannerAdapter.loadBanner(banner.optString(Constants.CONFIG_KEY_PLACEMENT), options);
							if(adView != null) {
								removeAllViews();
								addView(adView);
							}
						} catch (ClassNotFoundException e) {
							mCurrentAdNetworkIndex++;
							loadNextAdNetwork();
						} catch (NoSuchMethodException e) {
							e.printStackTrace();
							mCurrentAdNetworkIndex++;
							loadNextAdNetwork();
						} catch (IllegalAccessException e) {
							mCurrentAdNetworkIndex++;
							loadNextAdNetwork();
							e.printStackTrace();
						} catch (InstantiationException e) {
							mCurrentAdNetworkIndex++;
							loadNextAdNetwork();
							e.printStackTrace();
						} catch (InvocationTargetException e) {
							mCurrentAdNetworkIndex++;
							loadNextAdNetwork();
							e.printStackTrace();
						}
					} else {
						mCurrentAdNetworkIndex++;
						loadNextAdNetwork();
					}
				} else {
					scheduleReload();
				}
			} catch (JSONException e) {
				scheduleReload();
				e.printStackTrace();
			}
		} else {
			scheduleReload();
		}
	}

	@Keep
	public void destroy() {
		MediationSdk.removeAppConfigListener(this);
		if (mCurrentBannerAdapter != null) {
			mCurrentBannerAdapter.destroy();
			mCurrentBannerAdapter = null;
		}
	}

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		if (mAdSize != null) {
			float density = getResources().getDisplayMetrics().density;
			int width = getWidthInPixels(density);
			int height = getHeightInPixels(density);
			super.onMeasure(MeasureSpec.makeMeasureSpec(width, MeasureSpec.EXACTLY), MeasureSpec.makeMeasureSpec(height, MeasureSpec.EXACTLY));
		} else {
			super.onMeasure(widthMeasureSpec, heightMeasureSpec);
		}
	}

	@Override
	public void onAdLoaded(BaseBannerAdapter adapter) {
		SdkLogger.info("mAdLoaded: " + adapter.getClass().getSimpleName());
		mAdLoaded = true;
		if(mNeedToShowAd && getVisibility() == GONE){
			setVisibility(VISIBLE);
		}
	}

	@Override
	public void onAdLoadFailed(BaseBannerAdapter adapter, int errorCode) {
		SdkLogger.info("adLoadFailed " + errorCode);
		mAdLoaded = false;
		mCurrentAdNetworkIndex++;
		loadNextAdNetwork();
	}

	@Override
	public void onAdClicked(BaseBannerAdapter baseBannerAdapter) {
	}

	private int getWidthInPixels(float density) {
		switch (mAdSize) {
			case BaseBannerAdapter.ADSIZE_RECTANGLE_250:
				return (int) (300 * density);
			default:
				return (int) (320 * density);
		}
	}

	private int getHeightInPixels(float density) {
		switch (mAdSize) {
			case BaseBannerAdapter.ADSIZE_BANNER_90:
				return (int) (90 * density);
			case BaseBannerAdapter.ADSIZE_RECTANGLE_250:
				return (int) (250 * density);
			default:
				return (int) (50 * density);
		}
	}

	private void scheduleReload(){
		mAdLoaded = false;
		mMainHandler.postDelayed(mScheduleReload, RELOAD_PERIOD);
	}

	@Override
	public void onAppConfigChanged() {
		if(!mAdLoaded) {
			mMainHandler.removeCallbacks(mScheduleReload);
			loadAd();
		}
	}

}
