package com.devthing.sdk.network;

import android.net.http.HttpResponseCache;
import android.os.AsyncTask;

import com.devthing.sdk.MediationSdk;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Map;

import androidx.annotation.Keep;

@Keep
public class RestClient {

	public static final String METHOD_GET = "GET";
	public static final String METHOD_POST = "POST";
	public static final String METHOD_DELETE = "DELETE";

	public static final int ERROR_SUCCESS = 0;

	private static final String PLATFORM_ANDROID = "1";

	public static void execute(String urlStr, OnResponseListener listener){
		new ExecuteHttpTask(urlStr, null, listener).execute();
	}

	public static void execute(String urlStr, Map<String, String> queryParams, OnResponseListener listener) {
		new ExecuteHttpTask(urlStr, queryParams, listener).execute();
	}

	public static void execute(String method, String urlStr, JSONObject body, OnResponseListener listener) {
		new ExecuteHttpTask(method, urlStr, null, body != null ? body.toString() : null, listener).execute();
	}

	public static void execute(String urlStr, JSONObject body, OnResponseListener listener) {
		new ExecuteHttpTask(METHOD_POST, urlStr, null, body != null ? body.toString() : null, listener).execute();
	}

	public static void execute(String urlStr, JSONArray body, OnResponseListener listener) {
		new ExecuteHttpTask(METHOD_POST, urlStr, null, body != null ? body.toString() : null, listener).execute();
	}

	private static class ExecuteHttpTask extends AsyncTask<Void, Void, JSONObject> {

		private static int sRequestedCount = 0;
		private String mUrlStr;
		private String mMethod;
		private Map<String, String> mQueryParams;
		private String mJsonBody;
		private OnResponseListener mListener;

		private ExecuteHttpTask(String urlStr, Map<String, String> queryParams, OnResponseListener listener) {
			this(METHOD_GET, urlStr, queryParams, null, listener);
		}

		private ExecuteHttpTask(String method, String urlStr, Map<String, String> queryParams, String jsonBody, OnResponseListener listener) {
			mMethod = method;
			mUrlStr = urlStr;
			mQueryParams = queryParams;
			mJsonBody = jsonBody;
			mListener = listener;
		}

		@Override
		protected JSONObject doInBackground(Void... voids) {
			JSONObject response = null;
			try {
				String realUrl = null;
				if (mQueryParams != null && mQueryParams.size() > 0) {
					realUrl = applyQueryParams(mUrlStr, mQueryParams);
				} else {
					realUrl = mUrlStr;
				}
				URL url = new URL(realUrl);
				HttpURLConnection connection = (HttpURLConnection) url.openConnection();
				connection.setRequestMethod(mMethod);
				connection.setRequestProperty("p", PLATFORM_ANDROID);
				connection.setRequestProperty("pn", MediationSdk.getAppContext().getPackageName());
				if (mJsonBody != null) {
					connection.setDoOutput(true);
//					LogUtil.w("Json Body:" + mJsonBody);
					connection.setRequestProperty("Content-Type", "application/json");
					OutputStream os = connection.getOutputStream();
					os.write(mJsonBody.getBytes());
					os.close();
				}
				connection.connect();
				if (connection.getResponseCode() == HttpURLConnection.HTTP_OK) {
					InputStream is = connection.getInputStream();
					BufferedReader br = new BufferedReader(new InputStreamReader(is));
					StringBuilder jsonBuilder = new StringBuilder();
					String line;
					while ((line = br.readLine()) != null) {
						jsonBuilder.append(line).append("\n");
					}
					br.close();
					try {
						String json = jsonBuilder.toString();
//						LogUtil.w("Received Json: " + json);
						response = new JSONObject(json);
					} catch (JSONException e) {
						e.printStackTrace();
					}
				}
				connection.disconnect();
				sRequestedCount++;
			} catch (MalformedURLException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				HttpResponseCache cache = HttpResponseCache.getInstalled();
				if (cache != null) {
					cache.flush();
				}
			}
			return response;
		}

		@Override
		protected void onPostExecute(JSONObject jsonObject) {
			if (mListener != null) {
				if (jsonObject != null) {
					mListener.onSuccess(jsonObject);
				} else {
					mListener.onFailure(400);
				}
			}
		}
	}

	private static String applyQueryParams(String baseUrl, Map<String, String> queryParams) {
		StringBuilder builder = new StringBuilder(baseUrl);
		boolean first = true;
		for (String key : queryParams.keySet()) {
			if (first) {
				builder.append("?");
				first = false;
			} else {
				builder.append("&");
			}
			builder.append(key);
			builder.append("=");
			try {
				builder.append(URLEncoder.encode(queryParams.get(key), "UTF-8"));
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			}
		}
		return builder.toString();
	}

	@Keep
	public interface OnResponseListener {
		void onSuccess(JSONObject response);

		void onFailure(int errorCode);
	}
}
