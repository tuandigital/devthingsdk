package com.devthing.sdk.utils;

public class Constants {
	public static final String NETWORK_ADMOB = "admob";
	public static final String NETWORK_FAN = "fan";
	public static final String NETWORK_UNITY = "unity";
	public static final String ADAPTER_PACKAGE = "com.devthing.sdk.ads.adapters.";
	public static final String CONFIG_KEY_NETWORK = "network";
	public static final String CONFIG_KEY_ADMOB_APP_ID = "admobAppId";
	public static final String CONFIG_KEY_UNITY_GAME_ID = "unityGameId";
	public static final String CONFIG_KEY_BANNERS = "banners";
	public static final String CONFIG_KEY_INTERSTITIALS = "interstitials";
	public static final String CONFIG_KEY_REWARDEDS = "rewardeds";
	public static final String CONFIG_KEY_PLACEMENT = "placement";
	public static final String CONFIG_KEY_GENERAL = "general";
}
