package com.devthing.sdk.utils;

import android.content.Context;
import android.content.SharedPreferences;

public class MediationPrefUtil {
    private static final String MEDIATION_PREF = "med_pref";
    public static final String KEY_MEDIATION_CONFIG = "mediation_config";
    public static final String KEY_LAST_INTERSTITIAL_SHOWN = "last_interstitial_shown";
    private static volatile MediationPrefUtil sInstance;

    private SharedPreferences mPref;

    public static MediationPrefUtil getInstance(Context context){
        if(sInstance == null){
            synchronized (MediationPrefUtil.class){
                if(sInstance == null){
                    sInstance = new MediationPrefUtil(context);
                }
            }
        }
        return sInstance;
    }

    private MediationPrefUtil(Context context){
        mPref = context.getSharedPreferences(MEDIATION_PREF, Context.MODE_PRIVATE);
    }

    public String getString(String key, String def){
        return mPref.getString(key, def);
    }

    public void putString(String key, String value){
        mPref.edit().putString(key, value).apply();
    }

    public long getLong(String key, long def){
        return mPref.getLong(key, def);
    }

    public void putLong(String key, long value){
        mPref.edit().putLong(key, value).apply();
    }
}
