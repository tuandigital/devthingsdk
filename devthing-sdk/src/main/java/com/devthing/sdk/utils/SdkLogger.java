package com.devthing.sdk.utils;

import android.util.Log;

public class SdkLogger {

    private static final String TAG = "Adss";

    public static void debug(String message){
        Log.d(TAG, message);
    }

    public static void info(String message){
        Log.i(TAG, message);
    }

    public static void error(String message){
        Log.e(TAG, message);
    }

}
