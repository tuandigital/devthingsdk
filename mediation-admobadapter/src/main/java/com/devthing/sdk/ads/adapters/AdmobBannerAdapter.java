package com.devthing.sdk.ads.adapters;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;
import android.view.View;

import com.devthing.sdk.ads.utils.AdmobSingleton;
import com.devthing.sdk.ads.utils.AdmobUtil;
import com.devthing.sdk.base.AdErrorCode;
import com.devthing.sdk.base.ads.adapters.BaseBannerAdapter;
import com.devthing.sdk.utils.CommonConstants;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;

import java.util.Map;

import androidx.annotation.Keep;

@Keep
public class AdmobBannerAdapter extends BaseBannerAdapter {


    private AdView mAdView;

    public AdmobBannerAdapter(Context context) {
        super(context);
    }

    @Override
    public View loadBannerInternal(String placementId, Map<String, Object> options) {
        String appId = (String) options.get(CommonConstants.OPT_ADMOB_APP_ID);
        AdmobSingleton.getInstance().initializeAdmob(getContext(), appId);
        if (mAdView != null) {
            mAdView.destroy();
            mAdView = null;
        }
        if (!TextUtils.isEmpty(placementId)) {
            String adSizeStr = options.containsKey(OPT_ADSIZE) ? (String) options.get(OPT_ADSIZE) : ADSIZE_BANNER;
            AdSize adSize = getAdSize(adSizeStr);
            mAdView = new AdView(getContext());
            mAdView.setAdSize(adSize);
            mAdView.setAdUnitId(placementId);
            mAdView.setAdListener(new AdListener() {
                @Override
                public void onAdFailedToLoad(int i) {
                    super.onAdFailedToLoad(i);
                    AdmobBannerAdapter.this.onAdLoadFailed(AdmobUtil.getInternalErrorCode(i));
                }

                @Override
                public void onAdLoaded() {
                    super.onAdLoaded();
                    AdmobBannerAdapter.this.onAdLoaded();
                }

                @Override
                public void onAdClicked() {
                    super.onAdClicked();
                    AdmobBannerAdapter.this.onAdClicked();
                }
            });
            if (mAdView != null) {
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        mAdView.loadAd(new AdRequest.Builder().build());
                    }
                });
            }
        } else {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    onAdLoadFailed(AdErrorCode.INTERNAL_ERROR);
                }
            }, 500);
        }
        return mAdView;
    }

    @Override
    public void internalDestroy() {
        if (mAdView != null) {
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                @Override
                public void run() {
                    mAdView.destroy();
                    mAdView = null;
                }
            });
        }
    }

    private AdSize getAdSize(String adSizeStr) {
        if (!TextUtils.isEmpty(adSizeStr)) {
            switch (adSizeStr) {
                case ADSIZE_BANNER:
                    return AdSize.BANNER;
                case ADSIZE_BANNER_90:
                    return AdSize.LEADERBOARD;
                case ADSIZE_RECTANGLE_250:
                    return AdSize.MEDIUM_RECTANGLE;
            }
        }
        return AdSize.BANNER;
    }

}
