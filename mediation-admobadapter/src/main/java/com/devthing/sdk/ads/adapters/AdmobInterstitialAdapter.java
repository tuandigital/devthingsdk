package com.devthing.sdk.ads.adapters;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;

import com.devthing.sdk.ads.utils.AdmobSingleton;
import com.devthing.sdk.ads.utils.AdmobUtil;
import com.devthing.sdk.base.AdErrorCode;
import com.devthing.sdk.base.ads.adapters.BaseInterstitialAdapter;
import com.devthing.sdk.utils.CommonConstants;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;

import java.util.Map;

import androidx.annotation.Keep;

@Keep
public class AdmobInterstitialAdapter extends BaseInterstitialAdapter {

	private InterstitialAd mInterstitialAd;

	public AdmobInterstitialAdapter(Context context) {
		super(context);
	}

	@Override
	public void internalLoadAd(String placementId, Map<String, Object> options) {
		String admobAppId = (String) options.get(CommonConstants.OPT_ADMOB_APP_ID);
		AdmobSingleton.getInstance().initializeAdmob(getContext(), admobAppId);
		if (mInterstitialAd != null) {
			mInterstitialAd = null;
		}
		if (!TextUtils.isEmpty(placementId)) {
			mInterstitialAd = new InterstitialAd(getContext());
			mInterstitialAd.setAdUnitId(placementId);
			mInterstitialAd.setAdListener(new AdListener() {

				@Override
				public void onAdOpened() {
					super.onAdOpened();
					AdmobInterstitialAdapter.this.onAdDisplayed();
				}

				@Override
				public void onAdLoaded() {
					super.onAdLoaded();
					AdmobInterstitialAdapter.this.onAdLoaded();
				}

				@Override
				public void onAdFailedToLoad(int i) {
					super.onAdFailedToLoad(i);
					AdmobInterstitialAdapter.this.onAdLoadFailed(AdmobUtil.getInternalErrorCode(i));
				}

				@Override
				public void onAdClosed() {
					super.onAdClosed();
					AdmobInterstitialAdapter.this.onAdClosed();
				}

				@Override
				public void onAdClicked() {
					super.onAdClicked();
					AdmobInterstitialAdapter.this.onAdClicked();
				}

				@Override
				public void onAdLeftApplication() {
					super.onAdLeftApplication();
				}
			});
		}
		if (mInterstitialAd != null) {
			new Handler(Looper.getMainLooper()).post(new Runnable() {
				@Override
				public void run() {
					mInterstitialAd.loadAd(new AdRequest.Builder().build());
				}
			});
		} else {
			onAdLoadFailed(AdErrorCode.INTERNAL_ERROR);
		}
	}

	@Override
	protected void internalDestroy() {
	}

	@Override
	public void show() {
		new Handler(Looper.getMainLooper()).post(new Runnable() {
			@Override
			public void run() {
				mInterstitialAd.show();
			}
		});
	}


}
