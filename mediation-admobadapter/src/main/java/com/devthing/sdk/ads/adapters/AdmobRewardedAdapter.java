package com.devthing.sdk.ads.adapters;

import android.app.Activity;
import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;

import com.devthing.sdk.ads.utils.AdmobSingleton;
import com.devthing.sdk.ads.utils.AdmobUtil;
import com.devthing.sdk.base.AdErrorCode;
import com.devthing.sdk.base.ads.adapters.BaseRewardedAdapter;
import com.devthing.sdk.utils.CommonConstants;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.rewarded.RewardedAd;
import com.google.android.gms.ads.rewarded.RewardedAdCallback;
import com.google.android.gms.ads.rewarded.RewardedAdLoadCallback;

import java.util.Map;

import androidx.annotation.Keep;
import androidx.annotation.NonNull;

@Keep
public class AdmobRewardedAdapter extends BaseRewardedAdapter {

	private RewardedAd mRewardedAd;

	public AdmobRewardedAdapter(Context context) {
		super(context);
	}

	@Override
	protected void internalLoadAd(String placementId, Map<String, Object> options) {
		String admobAppId = (String) options.get(CommonConstants.OPT_ADMOB_APP_ID);
		AdmobSingleton.getInstance().initializeAdmob(getContext(), admobAppId);
		if (!TextUtils.isEmpty(placementId)) {
			mRewardedAd = new RewardedAd(getContext(), placementId);
		}
		if (mRewardedAd != null) {
			new Handler(Looper.getMainLooper()).post(new Runnable() {
				@Override
				public void run() {
					mRewardedAd.loadAd(new AdRequest.Builder().build(), new RewardedAdLoadCallback(){
						@Override
						public void onRewardedAdLoaded() {
							super.onRewardedAdLoaded();
							AdmobRewardedAdapter.this.onAdLoaded();
						}

						@Override
						public void onRewardedAdFailedToLoad(int i) {
							super.onRewardedAdFailedToLoad(i);
							AdmobRewardedAdapter.this.onAdLoadFailed(AdmobUtil.getInternalErrorCode(i));
						}
					});
				}
			});
		} else {
			onAdLoadFailed(AdErrorCode.INTERNAL_ERROR);
		}
	}

	@Override
	protected void internalDestroy() {

	}

	@Override
	public void show() {
		new Handler(Looper.getMainLooper()).post(new Runnable() {
			@Override
			public void run() {
				mRewardedAd.show((Activity) getContext(), new RewardedAdCallback() {
					@Override
					public void onUserEarnedReward(@NonNull com.google.android.gms.ads.rewarded.RewardItem rewardItem) {
						AdmobRewardedAdapter.this.onAdRewardedSuccess();
					}

					@Override
					public void onRewardedAdClosed() {
						super.onRewardedAdClosed();
						AdmobRewardedAdapter.this.onAdClosed();
					}

					@Override
					public void onRewardedAdFailedToShow(int i) {
						super.onRewardedAdFailedToShow(i);
						AdmobRewardedAdapter.this.onAdDisplayFailed();
					}

					@Override
					public void onRewardedAdOpened() {
						super.onRewardedAdOpened();
						AdmobRewardedAdapter.this.onAdDisplayed();
					}
				});
			}
		});
	}

}
