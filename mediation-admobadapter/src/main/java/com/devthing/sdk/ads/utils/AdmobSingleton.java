package com.devthing.sdk.ads.utils;

import android.content.Context;

import com.google.android.gms.ads.MobileAds;

import androidx.annotation.Keep;

@Keep
public class AdmobSingleton {
	private static volatile AdmobSingleton sInstance;

	private boolean mAdmobInitialized;

	public static AdmobSingleton getInstance(){
		if(sInstance == null){
			synchronized (AdmobSingleton.class) {
				if(sInstance == null) {
					sInstance = new AdmobSingleton();
				}
			}
		}
		return sInstance;
	}

	private AdmobSingleton(){
	}

	public void initializeAdmob(Context context, String appId){
		if(!mAdmobInitialized) {
			MobileAds.initialize(context, appId);
			mAdmobInitialized = true;
		}
	}
}
