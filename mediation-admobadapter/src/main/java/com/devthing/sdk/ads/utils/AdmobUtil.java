package com.devthing.sdk.ads.utils;

import com.devthing.sdk.base.AdErrorCode;
import com.google.android.gms.ads.AdRequest;

import androidx.annotation.Keep;

@Keep
public class AdmobUtil {
    public static int getInternalErrorCode(int srcErrorCode) {
        switch (srcErrorCode) {
            case AdRequest.ERROR_CODE_NETWORK_ERROR:
                return AdErrorCode.NETWORK_ERROR;
            case AdRequest.ERROR_CODE_NO_FILL:
                return AdErrorCode.NO_FILL;
            default:
                return AdErrorCode.INTERNAL_ERROR;
        }
    }

}
