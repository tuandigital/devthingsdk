package com.devthing.sdk.base;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import androidx.annotation.IntDef;
import androidx.annotation.Keep;

@Keep
public class AdErrorCode {
    public static final int NETWORK_ERROR = 1;
    public static final int NO_FILL = 2;
    public static final int INTERNAL_ERROR = 3;

    @Retention(RetentionPolicy.SOURCE)
    @IntDef({
            NETWORK_ERROR,
            NO_FILL,
            INTERNAL_ERROR
    })
    public @interface ImoErrorCodeDef {

    }
}
