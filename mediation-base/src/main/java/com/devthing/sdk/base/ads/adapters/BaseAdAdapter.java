package com.devthing.sdk.base.ads.adapters;

import android.content.Context;

import androidx.annotation.Keep;

@Keep
public abstract class BaseAdAdapter {
    private Context mContext;
    private boolean mLoading;
//    private Timer mCheckLoadingTimer;
    protected String mPlacementId;

    public BaseAdAdapter(Context context) {
        mContext = context;
//        mCheckLoadingTimer = new Timer();
    }

    public String getPlacementId(){
        return mPlacementId;
    }

    protected Context getContext() {
        return mContext;
    }

    protected boolean isLoading() {
        return mLoading;
    }

    protected void setLoading(boolean loading) {
        mLoading = loading;
    }

//    protected void scheduleCheckLoadingTimer() {
//        mLoading = true;
//        if (mCheckLoadingTimer != null) {
//            mCheckLoadingTimer.cancel();
//        }
//        mCheckLoadingTimer = new Timer();
//        mCheckLoadingTimer.schedule(new TimerTask() {
//            @Override
//            public void run() {
//                finishWaitingLoading();
//            }
//        }, 15000);
//    }

//    protected void cancelCheckLoadingTimer() {
//        mLoading = false;
//        if (mCheckLoadingTimer != null) {
//            mCheckLoadingTimer.cancel();
//        }
//        mCheckLoadingTimer = null;
//    }

//    protected void finishWaitingLoading() {
//        if (isLoading()) {
//            notifyFailedLoaded();
//            mLoading = false;
//        }
//    }

//    protected abstract void notifyFailedLoaded();

}
