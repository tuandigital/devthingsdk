package com.devthing.sdk.base.ads.adapters;

import android.content.Context;
import android.view.View;

import com.devthing.sdk.base.ads.listeners.InternalBannerAdListener;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.Map;

import androidx.annotation.Keep;
import androidx.annotation.StringDef;

@Keep
public abstract class BaseBannerAdapter extends BaseAdAdapter {
    public static final String OPT_ADSIZE = "adsize";
    public static final String ADSIZE_BANNER = "BANNER";
    public static final String ADSIZE_BANNER_90 = "BANNER_90";
    public static final String ADSIZE_RECTANGLE_250 = "RECTANGLE_250";
    protected InternalBannerAdListener mInternalBannerAdListener;

    public BaseBannerAdapter(Context context) {
        super(context);
    }

    public View loadBanner(String placementId, Map<String, Object> options){
        mPlacementId = placementId;
        return loadBannerInternal(placementId, options);
    }

    public abstract View loadBannerInternal(String placementId, Map<String, Object> options);

    public void destroy(){
        mInternalBannerAdListener = null;
        internalDestroy();
    }

    protected abstract void internalDestroy();

    public void setInternalAdListener(InternalBannerAdListener adListener){
        mInternalBannerAdListener = adListener;
    }

    @Retention(RetentionPolicy.SOURCE)
    @StringDef({
            ADSIZE_BANNER,
            ADSIZE_BANNER_90,
            ADSIZE_RECTANGLE_250
    })
    @Keep
    public @interface DTSizeDef {}

    protected void onAdClicked(){
        if(mInternalBannerAdListener != null){
            mInternalBannerAdListener.onAdClicked(this);
        }
    }

    protected void onAdLoaded(){
        if(mInternalBannerAdListener != null){
            mInternalBannerAdListener.onAdLoaded(this);
        }
    }

    protected void onAdLoadFailed(int errorCode){
        if(mInternalBannerAdListener != null){
            mInternalBannerAdListener.onAdLoadFailed(this, errorCode);
        }
    }
}
