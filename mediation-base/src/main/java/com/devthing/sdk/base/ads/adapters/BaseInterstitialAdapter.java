package com.devthing.sdk.base.ads.adapters;

import android.content.Context;

import com.devthing.sdk.base.ads.listeners.InternalInterstitialAdListener;

import java.util.Map;

import androidx.annotation.Keep;

@Keep
public abstract class BaseInterstitialAdapter extends BaseAdAdapter {

    protected InternalInterstitialAdListener mInternalInterstitialAdListener;

    public BaseInterstitialAdapter(Context context) {
        super(context);
    }

    public void loadAd(String placementId, Map<String, Object> options) {
        mPlacementId = placementId;
        internalLoadAd(placementId, options);
    }

    public void destroy() {
        mInternalInterstitialAdListener = null;
        internalDestroy();
    }

    public void setInternalInterstitialAdListener(InternalInterstitialAdListener listener) {
        mInternalInterstitialAdListener = listener;
    }

    protected abstract void internalLoadAd(String placementId, Map<String, Object> options);

    protected abstract void internalDestroy();

    public abstract void show();

    protected void onAdLoaded() {
        if (mInternalInterstitialAdListener != null) {
            mInternalInterstitialAdListener.onAdLoaded(this);
        }
    }

    protected void onAdLoadFailed(int errorCode) {
        if (mInternalInterstitialAdListener != null) {
            mInternalInterstitialAdListener.onAdLoadFailed(this, errorCode);
        }
    }

    protected void onAdDisplayed() {
        if (mInternalInterstitialAdListener != null) {
            mInternalInterstitialAdListener.onAdDisplayed(this);
        }
    }

    protected void onAdClosed() {
        if (mInternalInterstitialAdListener != null) {
            mInternalInterstitialAdListener.onAdClosed(this);
        }
    }

    protected void onAdClicked(){
        if(mInternalInterstitialAdListener != null){
            mInternalInterstitialAdListener.onAdClicked(this);
        }
    }
}
