package com.devthing.sdk.base.ads.adapters;

import android.content.Context;
import android.view.View;

import com.devthing.sdk.base.AdErrorCode;
import com.devthing.sdk.base.ads.listeners.InternalNativeAdListener;
import com.devthing.sdk.base.ads.nativeads.NativeViewBinder;

import java.util.Map;

public abstract class BaseNativeAdAdapter extends BaseAdAdapter {

    private int mAdNetworkId;
    private InternalNativeAdListener mInternalNativeAdListener;

    public BaseNativeAdAdapter(Context context) {
        super(context);
    }

    public int getAdNetworkId() {
        return mAdNetworkId;
    }

    public void loadAd(final String placementId, final Map<String, Object> options) {
//        mAdNetworkId = setting.getAdNetworkId();
//        internalLoadAd(setting, options);
    }

    public void setInternalNativeAdListener(InternalNativeAdListener listener) {
        mInternalNativeAdListener = listener;
    }

    protected abstract void internalLoadAd(String placementId, Map<String, Object> options);

    public abstract boolean isAdLoaded();

    public abstract View render(NativeViewBinder binder);

    protected void onAdLoaded() {
        if (mInternalNativeAdListener != null) {
            mInternalNativeAdListener.onAdLoaded(this);
        }
    }

    protected void onAdLoadFailed(@AdErrorCode.ImoErrorCodeDef int errorCode) {
        if (mInternalNativeAdListener != null) {
            mInternalNativeAdListener.onAdLoadFailed(this, errorCode);
        }
    }

    protected void onAdClicked() {
        if (mInternalNativeAdListener != null) {
            mInternalNativeAdListener.onAdClicked(this);
        }
    }

    public void destroy(){
        mInternalNativeAdListener = null;
        internalDestroy();
    }

    protected abstract void internalDestroy();

}
