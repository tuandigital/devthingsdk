package com.devthing.sdk.base.ads.adapters;

import android.content.Context;

import com.devthing.sdk.base.ads.listeners.InternalRewardedAdListener;

import java.util.Map;

import androidx.annotation.Keep;

@Keep
public abstract class BaseRewardedAdapter extends BaseAdAdapter {
    protected InternalRewardedAdListener mInternalRewardedAdListener;

    public BaseRewardedAdapter(Context context) {
        super(context);
    }

    public void loadAd(String placementId, Map<String, Object> options) {
        mPlacementId = placementId;
        internalLoadAd(placementId, options);
    }

    public void setInternalRewardedAdListener(InternalRewardedAdListener listener) {
        mInternalRewardedAdListener = listener;
    }

    protected abstract void internalLoadAd(String placementId, Map<String, Object> options);

    public void destroy() {
        mInternalRewardedAdListener = null;
        internalDestroy();
    }

    protected abstract void internalDestroy();

    public abstract void show();

    protected void onAdLoadFailed(int errorCode) {
        if (mInternalRewardedAdListener != null) {
            mInternalRewardedAdListener.onAdLoadFailed(this, errorCode);
        }
    }

    protected void onAdLoaded() {
        if (mInternalRewardedAdListener != null) {
            mInternalRewardedAdListener.onAdLoaded(this);
        }
    }

    protected void onAdDisplayed() {
        if (mInternalRewardedAdListener != null) {
            mInternalRewardedAdListener.onAdDisplayed(this);
        }
    }

    protected void onAdDisplayFailed(){
        if(mInternalRewardedAdListener != null){
            mInternalRewardedAdListener.onAdDisplayFailed(this);
        }
    }

    protected void onAdClosed() {
        if (mInternalRewardedAdListener != null) {
            mInternalRewardedAdListener.onAdClosed(this);
        }
    }

    protected void onAdRewardedSuccess() {
        if (mInternalRewardedAdListener != null) {
            mInternalRewardedAdListener.onRewardSuccess(this);
        }
    }

    protected void onAdClicked(){
        if(mInternalRewardedAdListener != null){
            mInternalRewardedAdListener.onAdClicked(this);
        }
    }

}
