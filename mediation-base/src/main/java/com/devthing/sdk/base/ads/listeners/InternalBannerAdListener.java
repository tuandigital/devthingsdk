package com.devthing.sdk.base.ads.listeners;

import com.devthing.sdk.base.AdErrorCode;
import com.devthing.sdk.base.ads.adapters.BaseBannerAdapter;

import androidx.annotation.Keep;

@Keep
public interface InternalBannerAdListener {
    void onAdLoaded(BaseBannerAdapter adapter);
    void onAdLoadFailed(BaseBannerAdapter adapter, @AdErrorCode.ImoErrorCodeDef int errorCode);
    void onAdClicked(BaseBannerAdapter adapter);
}
