package com.devthing.sdk.base.ads.listeners;

import com.devthing.sdk.base.AdErrorCode;
import com.devthing.sdk.base.ads.adapters.BaseInterstitialAdapter;

import androidx.annotation.Keep;

@Keep
public interface InternalInterstitialAdListener {
    void onAdLoaded(BaseInterstitialAdapter adapter);
    void onAdDisplayed(BaseInterstitialAdapter adapter);
    void onAdClosed(BaseInterstitialAdapter adapter);
    void onAdLoadFailed(BaseInterstitialAdapter adapter, @AdErrorCode.ImoErrorCodeDef int errorCode);
    void onAdClicked(BaseInterstitialAdapter adapter);
}
