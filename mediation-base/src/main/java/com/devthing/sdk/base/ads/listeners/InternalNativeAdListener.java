package com.devthing.sdk.base.ads.listeners;

import com.devthing.sdk.base.AdErrorCode;
import com.devthing.sdk.base.ads.adapters.BaseNativeAdAdapter;

import androidx.annotation.Keep;

@Keep
public interface InternalNativeAdListener {

    void onAdLoaded(BaseNativeAdAdapter adapter);

    void onAdLoadFailed(BaseNativeAdAdapter adapter, @AdErrorCode.ImoErrorCodeDef int errorCode);

    void onAdClicked(BaseNativeAdAdapter adapter);
}
