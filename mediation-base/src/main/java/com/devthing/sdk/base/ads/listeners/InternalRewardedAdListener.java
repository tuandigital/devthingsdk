package com.devthing.sdk.base.ads.listeners;

import com.devthing.sdk.base.AdErrorCode;
import com.devthing.sdk.base.ads.adapters.BaseRewardedAdapter;

import androidx.annotation.Keep;

@Keep
public interface InternalRewardedAdListener{

    void onRewardSuccess(BaseRewardedAdapter adapter);
    void onAdLoaded(BaseRewardedAdapter adapter);
    void onAdDisplayed(BaseRewardedAdapter adapter);
    void onAdClosed(BaseRewardedAdapter adapter);
    void onAdLoadFailed(BaseRewardedAdapter adapter, @AdErrorCode.ImoErrorCodeDef int errorCode);
    void onAdClicked(BaseRewardedAdapter adapter);
    void onAdDisplayFailed(BaseRewardedAdapter adapter);
}
