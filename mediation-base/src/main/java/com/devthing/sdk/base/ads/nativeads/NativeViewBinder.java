package com.devthing.sdk.base.ads.nativeads;

import androidx.annotation.Keep;

@Keep
public class NativeViewBinder {
    public final int layoutId;
    public final int titleId;
    public final int textId;
    public final int callToActionId;
    public final int mainImageId;
    public final int iconImageId;
    public final int adChoiceImageId;

    private NativeViewBinder(Builder builder) {
        this.layoutId = builder.layoutId;
        this.titleId = builder.titleId;
        this.textId = builder.textId;
        this.callToActionId = builder.callToActionId;
        this.mainImageId = builder.mainImageId;
        this.iconImageId = builder.iconImageId;
        this.adChoiceImageId = builder.adChoiceImageId;
    }

    public static final class Builder {
        private final int layoutId;
        private int titleId = -1;
        private int textId = -1;
        private int callToActionId = -1;
        private int mainImageId = -1;
        private int iconImageId = -1;
        private int adChoiceImageId = -1;

        public Builder(int layoutId) {
            this.layoutId = layoutId;
        }

        public Builder titleId(int titleId) {
            this.titleId = titleId;
            return this;
        }

        public Builder textId(int textId) {
            this.textId = textId;
            return this;
        }

        public Builder callToActionId(int callToActionId){
            this.callToActionId = callToActionId;
            return this;
        }

        public Builder mainImageId(int mainImageId){
            this.mainImageId = mainImageId;
            return this;
        }

        public Builder iconImageId(int iconImageId){
            this.iconImageId = iconImageId;
            return this;
        }

        public Builder adChoiceImageId(int adChoiceImageId){
            this.adChoiceImageId = adChoiceImageId;
            return this;
        }

        public NativeViewBinder build(){
            return new NativeViewBinder(this);
        }
    }
}
