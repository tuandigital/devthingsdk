package com.devthing.sdk.utils;


import androidx.annotation.Keep;

@Keep
public class CommonConstants {
	public static final String OPT_ADMOB_APP_ID = "admob_app_id";
	public static final String OPT_UNITY_GAME_ID = "unity_game_id";
}
