package com.devthing.sdk.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import androidx.annotation.Keep;

public class MiscUtil {

    private static final String PACKAGE_PATH = "com.devthing.sdk.ads.adapters";

    @Keep
    public static boolean isConnected(Context context){
        final ConnectivityManager connectivityManager =
                (ConnectivityManager) context.getSystemService( Context.CONNECTIVITY_SERVICE );
        final NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        return networkInfo != null && networkInfo.isConnected();
    }

    public static String getAdapterClassPath(String adapterClass){
        return PACKAGE_PATH + "." + adapterClass;
    }

    public static boolean isAdapterClassExist(String adapterClass){
        String fullClassName = getAdapterClassPath(adapterClass);
        try {
            Class clazz = Class.forName(fullClassName);
            return true;
        } catch (ClassNotFoundException e) {
            return false;
        }
    }
}
