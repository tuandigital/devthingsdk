package com.devthing.sdk.ads.adapters;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;
import android.view.View;

import com.devthing.sdk.base.AdErrorCode;
import com.facebook.ads.Ad;
import com.facebook.ads.AdError;
import com.facebook.ads.AdListener;
import com.facebook.ads.AdSize;
import com.facebook.ads.AdView;
import com.devthing.sdk.base.ads.adapters.BaseBannerAdapter;
import com.devthing.sdk.ads.utils.FanUtil;
import com.facebook.ads.AudienceNetworkAds;

import java.util.Map;

import androidx.annotation.Keep;

@Keep
public class FanBannerAdapter extends BaseBannerAdapter {

    private AdView mAdView;

    public FanBannerAdapter(Context context) {
        super(context);
    }

    @Override
    public View loadBannerInternal(String placementId, Map<String, Object> options) {
        if (mAdView != null) {
            destroyAdView();
            mAdView = null;
        }
        if(!AudienceNetworkAds.isInitialized(getContext())){
            AudienceNetworkAds.initialize(getContext());
        }
        if (!TextUtils.isEmpty(placementId)) {
            String adSizeStr = options.containsKey(OPT_ADSIZE) ? (String) options.get(OPT_ADSIZE) : ADSIZE_BANNER;
            AdSize adSize = getAdSize(adSizeStr);
            mAdView = new AdView(getContext(), placementId, adSize);
            mAdView.setAdListener(new AdListener() {
                @Override
                public void onError(Ad ad, AdError adError) {
                    FanBannerAdapter.this.onAdLoadFailed(FanUtil.getInternalAdError(adError));
                }

                @Override
                public void onAdLoaded(Ad ad) {
                    FanBannerAdapter.this.onAdLoaded();
                }

                @Override
                public void onAdClicked(Ad ad) {
                    FanBannerAdapter.this.onAdClicked();
                }

                @Override
                public void onLoggingImpression(Ad ad) {

                }
            });
        }
        if (mAdView != null) {
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                @Override
                public void run() {
                    mAdView.loadAd();
                }
            });
        } else {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    onAdLoadFailed(AdErrorCode.INTERNAL_ERROR);
                }
            }, 500);
        }
        return mAdView;
    }

    @Override
    public void internalDestroy() {
        destroyAdView();
    }

    private void destroyAdView() {
        if (mAdView != null) {
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                @Override
                public void run() {
                    mAdView.destroy();
                    mAdView = null;
                }
            });
        }
    }

    private AdSize getAdSize(String adSizeStr) {
        if (!TextUtils.isEmpty(adSizeStr)) {
            switch (adSizeStr) {
                case ADSIZE_BANNER:
                    return AdSize.BANNER_HEIGHT_50;
                case ADSIZE_BANNER_90:
                    return AdSize.BANNER_HEIGHT_90;
                case ADSIZE_RECTANGLE_250:
                    return AdSize.RECTANGLE_HEIGHT_250;
            }
        }
        return AdSize.BANNER_HEIGHT_50;
    }

}
