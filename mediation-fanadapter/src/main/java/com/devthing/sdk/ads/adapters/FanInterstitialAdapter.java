package com.devthing.sdk.ads.adapters;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;

import com.devthing.sdk.ads.utils.FanUtil;
import com.devthing.sdk.base.AdErrorCode;
import com.devthing.sdk.base.ads.adapters.BaseInterstitialAdapter;
import com.facebook.ads.Ad;
import com.facebook.ads.AdError;
import com.facebook.ads.AudienceNetworkAds;
import com.facebook.ads.InterstitialAd;
import com.facebook.ads.InterstitialAdListener;

import java.util.Map;

import androidx.annotation.Keep;

@Keep
public class FanInterstitialAdapter extends BaseInterstitialAdapter implements InterstitialAdListener {

	private InterstitialAd mInterstitialAd;

	public FanInterstitialAdapter(Context context) {
		super(context);
	}

	@Override
	protected void internalLoadAd(String placementId, Map<String, Object> options) {
		if (mInterstitialAd != null) {
			mInterstitialAd.destroy();
			mInterstitialAd = null;
		}
		if(!AudienceNetworkAds.isInitialized(getContext())){
			AudienceNetworkAds.initialize(getContext());
		}
		if (!TextUtils.isEmpty(placementId)) {
			mInterstitialAd = new InterstitialAd(getContext(), placementId);
			mInterstitialAd.setAdListener(this);
		}
		if (mInterstitialAd != null) {
			new Handler(Looper.getMainLooper()).post(new Runnable() {
				@Override
				public void run() {
					mInterstitialAd.loadAd();
				}
			});
		} else {
			onAdLoadFailed(AdErrorCode.INTERNAL_ERROR);
		}
	}

	@Override
	protected void internalDestroy() {
		new Handler(Looper.getMainLooper()).post(new Runnable() {
			@Override
			public void run() {
				if (mInterstitialAd != null) {
					mInterstitialAd.destroy();
					mInterstitialAd = null;
				}
			}
		});
	}

	@Override
	public void show() {
		new Handler(Looper.getMainLooper()).post(new Runnable() {
			@Override
			public void run() {
				mInterstitialAd.show();
			}
		});
	}

	@Override
	public void onInterstitialDisplayed(Ad ad) {
		onAdDisplayed();
	}

	@Override
	public void onInterstitialDismissed(Ad ad) {
		onAdClosed();
	}

	@Override
	public void onError(Ad ad, AdError adError) {
		onAdLoadFailed(FanUtil.getInternalAdError(adError));
	}

	@Override
	public void onAdLoaded(Ad ad) {
		onAdLoaded();
	}

	@Override
	public void onAdClicked(Ad ad) {
		onAdClicked();
	}

	@Override
	public void onLoggingImpression(Ad ad) {

	}

}
