package com.devthing.sdk.ads.adapters;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;

import com.devthing.sdk.ads.utils.FanUtil;
import com.devthing.sdk.base.AdErrorCode;
import com.devthing.sdk.base.ads.adapters.BaseRewardedAdapter;
import com.facebook.ads.Ad;
import com.facebook.ads.AdError;
import com.facebook.ads.AudienceNetworkAds;
import com.facebook.ads.RewardedVideoAd;
import com.facebook.ads.RewardedVideoAdListener;

import java.util.Map;

import androidx.annotation.Keep;

@Keep
public class FanRewardedAdapter extends BaseRewardedAdapter implements RewardedVideoAdListener {

	private RewardedVideoAd mRewardedVideoAd;

	public FanRewardedAdapter(Context context) {
		super(context);
	}

	@Override
	protected void internalLoadAd(String placementId, Map<String, Object> options) {
		if (mRewardedVideoAd != null) {
			mRewardedVideoAd.destroy();
			mRewardedVideoAd = null;
		}
		if(!AudienceNetworkAds.isInitialized(getContext())){
			AudienceNetworkAds.initialize(getContext());
		}
		if (!TextUtils.isEmpty(placementId)) {
			mRewardedVideoAd = new RewardedVideoAd(getContext(), placementId);
			mRewardedVideoAd.setAdListener(this);
		}
		if (mRewardedVideoAd != null) {
			mRewardedVideoAd.loadAd();
		} else {
			onAdLoadFailed(AdErrorCode.INTERNAL_ERROR);
		}
	}

	@Override
	protected void internalDestroy() {
		if(mRewardedVideoAd != null){
			mRewardedVideoAd.destroy();
			mRewardedVideoAd = null;
		}
	}

	@Override
	public void show() {
		new Handler(Looper.getMainLooper()).post(new Runnable() {
			@Override
			public void run() {
				mRewardedVideoAd.show();
			}
		});
	}

	@Override
	public void onRewardedVideoCompleted() {
		onAdRewardedSuccess();
	}

	@Override
	public void onError(Ad ad, AdError adError) {
		onAdLoadFailed(FanUtil.getInternalAdError(adError));
	}

	@Override
	public void onAdLoaded(Ad ad) {
		onAdLoaded();
	}

	@Override
	public void onAdClicked(Ad ad) {
		onAdClicked();
	}

	@Override
	public void onLoggingImpression(Ad ad) {
		onAdDisplayed();
	}

	@Override
	public void onRewardedVideoClosed() {
		onAdClosed();
	}

}
