package com.devthing.sdk.ads.utils;

import com.facebook.ads.AdError;
import com.devthing.sdk.base.AdErrorCode;

import androidx.annotation.Keep;

@Keep
public class FanUtil {
    public static int getInternalAdError(AdError adError) {
        switch (adError.getErrorCode()) {
            case AdError.NETWORK_ERROR_CODE:
                return AdErrorCode.NETWORK_ERROR;
            case AdError.NO_FILL_ERROR_CODE:
                return AdErrorCode.NO_FILL;
            default:
                return AdErrorCode.INTERNAL_ERROR;
        }
    }

}
