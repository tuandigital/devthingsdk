package com.devthing.sdk.ads.adapters;

import android.content.Context;
import android.text.TextUtils;

import com.devthing.sdk.ads.utils.UnityInterstitialShowCallback;
import com.devthing.sdk.ads.utils.UnitySingleton;
import com.devthing.sdk.base.AdErrorCode;
import com.devthing.sdk.base.ads.adapters.BaseInterstitialAdapter;
import com.devthing.sdk.utils.CommonConstants;

import java.util.Map;

import androidx.annotation.Keep;

@Keep
public class UnityInterstitialAdapter extends BaseInterstitialAdapter {

	public UnityInterstitialAdapter(Context context) {
		super(context);
	}


	@Override
	protected void internalLoadAd(String placementId, Map<String, Object> options) {
		String unityGameId = (String) options.get(CommonConstants.OPT_UNITY_GAME_ID);
		UnitySingleton.getInstance().initialize(getContext(), unityGameId);
		if (!TextUtils.isEmpty(placementId)) {
			if (UnitySingleton.getInstance().isAdsLoaded(placementId)) {
				onAdLoaded();
			} else {
				onAdLoadFailed(AdErrorCode.NO_FILL);
			}
		} else {
			onAdLoadFailed(AdErrorCode.INTERNAL_ERROR);
		}
	}

	@Override
	protected void internalDestroy() {

	}

	@Override
	public void show() {
		UnitySingleton.getInstance().showInterstitial(getContext(), getPlacementId(), new UnityInterstitialShowCallback(getPlacementId()) {
			@Override
			public void onUnityInterstitialDisplayed() {
				onAdDisplayed();
			}

			@Override
			public void onUnityInterstitialDisplayFailed() {

			}

			@Override
			public void onUnityInterstitialClosed() {
				onAdClosed();
			}
		});
	}
}
