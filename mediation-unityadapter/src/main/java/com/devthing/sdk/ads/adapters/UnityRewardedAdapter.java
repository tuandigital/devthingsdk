package com.devthing.sdk.ads.adapters;

import android.content.Context;
import android.text.TextUtils;

import com.devthing.sdk.ads.utils.UnityRewardedShowCallback;
import com.devthing.sdk.ads.utils.UnitySingleton;
import com.devthing.sdk.base.AdErrorCode;
import com.devthing.sdk.base.ads.adapters.BaseRewardedAdapter;
import com.devthing.sdk.utils.CommonConstants;

import java.util.Map;

import androidx.annotation.Keep;

@Keep
public class UnityRewardedAdapter extends BaseRewardedAdapter {

	public UnityRewardedAdapter(Context context) {
		super(context);
	}

	@Override
	protected void internalLoadAd(String placementId, Map<String, Object> options) {
		String unityGameId = (String) options.get(CommonConstants.OPT_UNITY_GAME_ID);
		UnitySingleton.getInstance().initialize(getContext(), unityGameId);
		if(!TextUtils.isEmpty(placementId)){
			if(UnitySingleton.getInstance().isAdsLoaded(placementId)){
				onAdLoaded();
			} else {
				onAdLoadFailed(AdErrorCode.NO_FILL);
			}
		} else {
			onAdLoadFailed(AdErrorCode.INTERNAL_ERROR);
		}
	}

	@Override
	protected void internalDestroy() {

	}

	@Override
	public void show() {
		UnitySingleton.getInstance().showRewardedAd(getContext(), getPlacementId(), new UnityRewardedShowCallback(getPlacementId()) {
			@Override
			public void onUnityRewardedDisplayed() {
				onAdDisplayed();
			}

			@Override
			public void onUnityRewardedDisplayFailed() {
				onAdDisplayFailed();
			}

			@Override
			public void onUnityRewardedClosed() {
				onAdClosed();
			}

			@Override
			public void onUnityRewardedSuccess() {
				onAdRewardedSuccess();
			}
		});
	}
}
