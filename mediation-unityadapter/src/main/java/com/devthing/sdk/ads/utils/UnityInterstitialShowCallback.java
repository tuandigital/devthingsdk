package com.devthing.sdk.ads.utils;

import androidx.annotation.Keep;

@Keep
public abstract class UnityInterstitialShowCallback {

	private String mPlacementId;

	public UnityInterstitialShowCallback(String placementId){
		mPlacementId = placementId;
	}

	public String getPlacementId(){
		return mPlacementId;
	}

	public abstract void onUnityInterstitialDisplayed();
	public abstract void onUnityInterstitialDisplayFailed();
	public abstract void onUnityInterstitialClosed();
}
