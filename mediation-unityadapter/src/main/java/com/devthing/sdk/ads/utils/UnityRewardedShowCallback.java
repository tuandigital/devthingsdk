package com.devthing.sdk.ads.utils;

import androidx.annotation.Keep;

@Keep
public abstract class UnityRewardedShowCallback {

	private final String mPlacementId;

	public UnityRewardedShowCallback(String placementId){
		mPlacementId = placementId;
	}

	public String getPlacementId() {
		return mPlacementId;
	}

	public abstract void onUnityRewardedDisplayed();
	public abstract void onUnityRewardedDisplayFailed();
	public abstract void onUnityRewardedClosed();
	public abstract void onUnityRewardedSuccess();
}
