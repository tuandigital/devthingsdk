package com.devthing.sdk.ads.utils;

import android.app.Activity;
import android.content.Context;

import com.unity3d.ads.IUnityAdsListener;
import com.unity3d.ads.UnityAds;

import java.util.HashSet;

import androidx.annotation.Keep;

@Keep
public class UnitySingleton implements IUnityAdsListener {
	private static volatile UnitySingleton sInstance;
	private HashSet<String> mAvailablePlacements;
	private UnityInterstitialShowCallback mInterstitialShowCallback;
	private UnityRewardedShowCallback mRewardedShowCallback;

	public static UnitySingleton getInstance() {
		if (sInstance == null) {
			synchronized (UnitySingleton.class) {
				if (sInstance == null) {
					sInstance = new UnitySingleton();
				}
			}
		}
		return sInstance;
	}

	private UnitySingleton() {
		mAvailablePlacements = new HashSet<>();
	}

	public void initialize(Context context, String gameId) {
		if (!UnityAds.isInitialized()) {
			UnityAds.initialize((Activity) context, gameId, this, false);
		}
	}

	@Override
	public void onUnityAdsReady(String placementId) {
		mAvailablePlacements.add(placementId);
		if (mInterstitialShowCallback != null && placementId.equals(mInterstitialShowCallback.getPlacementId())) {
		}
	}

	@Override
	public void onUnityAdsStart(String placementId) {
		mAvailablePlacements.remove(placementId);
		if (mInterstitialShowCallback != null && placementId.equals(mInterstitialShowCallback.getPlacementId())) {
			mInterstitialShowCallback.onUnityInterstitialDisplayed();
		}
		if(mRewardedShowCallback != null && placementId.equals(mRewardedShowCallback.getPlacementId())){
			mRewardedShowCallback.onUnityRewardedDisplayed();
		}
	}

	@Override
	public void onUnityAdsFinish(String placementId, UnityAds.FinishState result) {
		if (mInterstitialShowCallback != null) {
			if (placementId.equals(mInterstitialShowCallback.getPlacementId())) {
				if (result == UnityAds.FinishState.COMPLETED) {
					mInterstitialShowCallback.onUnityInterstitialClosed();
				}
			}
		} else if(mRewardedShowCallback != null){
			if(placementId.equals(mRewardedShowCallback.getPlacementId())){
				if(result == UnityAds.FinishState.COMPLETED){
					mRewardedShowCallback.onUnityRewardedSuccess();
				}
				mRewardedShowCallback.onUnityRewardedClosed();
			}
		}
	}

	@Override
	public void onUnityAdsError(UnityAds.UnityAdsError error, String message) {
		if (mInterstitialShowCallback != null) {
			mInterstitialShowCallback.onUnityInterstitialDisplayFailed();
		} else if (mRewardedShowCallback != null) {
			mRewardedShowCallback.onUnityRewardedDisplayFailed();
		}
	}

	public boolean isAdsLoaded(String placementId) {
		if (!mAvailablePlacements.contains(placementId) && UnityAds.isReady(placementId)) {
			mAvailablePlacements.add(placementId);
		}
		return mAvailablePlacements.contains(placementId);
	}

	public void showInterstitial(Context context, String placementId, UnityInterstitialShowCallback callback) {
		if (UnityAds.isReady(placementId)) {
			mRewardedShowCallback = null;
			mInterstitialShowCallback = callback;
			UnityAds.show((Activity) context, placementId);
		} else {
			callback.onUnityInterstitialDisplayFailed();
		}
	}

	public void showRewardedAd(Context context, String placementId, UnityRewardedShowCallback callback) {
		if (UnityAds.isReady(placementId)) {
			mInterstitialShowCallback = null;
			mRewardedShowCallback = callback;
			UnityAds.show((Activity) context, placementId);
		} else {
			callback.onUnityRewardedDisplayFailed();
		}
	}


}
