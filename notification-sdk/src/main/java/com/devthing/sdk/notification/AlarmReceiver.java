package com.devthing.sdk.notification;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class AlarmReceiver extends BroadcastReceiver {
    public static final String CONTENT_INTENT = "content_intent";

    @Override
    public void onReceive(Context context, Intent intent) {
        if(intent != null){
            Intent contentIntent = intent.getParcelableExtra(CONTENT_INTENT);
            if(contentIntent != null){
                NotificationService.enqueueWork(context, NotificationService.class, NotificationService.JOB_ID, contentIntent);
            }
        }
    }
}
