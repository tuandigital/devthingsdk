package com.devthing.sdk.notification;


import android.text.TextUtils;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.util.Calendar;
import java.util.Map;

/**
 * Created by tuandigital on 1/31/18.
 */

public class BackgroundFirebaseMessageService extends FirebaseMessagingService {

    private static final int DELIVERY_NOW = 1;
    private static final int DELIVERY_INTELLIGENT = 2;
    private static final int DELIVERY_DAY_TIME = 3;
    private static final int TYPE_ALERT = 1;
    private static final int TYPE_ADS = 2;

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
        Map<String, String> data = remoteMessage.getData();
        if(data != null && data.size() > 0) {
            String title = data.get("title");
            String message = data.get("message");
            String iconUrl = data.get("imageUrl");
            String url = data.get("url");
            String typeStr = data.get("type");
            int type = TextUtils.isEmpty(typeStr) ? TYPE_ALERT : Integer.parseInt(typeStr);
            String deliveryStr = data.get("delivery");
            int delivery = TextUtils.isEmpty(deliveryStr) ? DELIVERY_NOW : Integer.parseInt(deliveryStr);
            long postTime = -1;
            if (delivery != DELIVERY_NOW) {
                long deliveryTime = 68400000;//default 07:00PM
                if (delivery == DELIVERY_INTELLIGENT) {
                    deliveryTime = NotificationSdk.getOpenTime(this);
                } else if (delivery == DELIVERY_DAY_TIME) {
                    String postTimeStr = data.get("postTime");
                    if(!TextUtils.isEmpty(postTimeStr)) {
                        deliveryTime = Integer.parseInt(postTimeStr);
                    }
                }
                Calendar cal = Calendar.getInstance();
                cal.set(Calendar.HOUR_OF_DAY, 0);
                cal.set(Calendar.MINUTE, 0);
                cal.set(Calendar.SECOND, 0);
                cal.add(Calendar.MILLISECOND, (int) deliveryTime);
                if (cal.after(Calendar.getInstance())) {
                    postTime = cal.getTimeInMillis();
                } else {
                    cal.add(Calendar.HOUR, 24);
                    postTime = cal.getTimeInMillis();
                }
            }
            NotificationUtils.scheduleNotification(this, type, title, message, iconUrl, url, postTime);
        }
    }
}
