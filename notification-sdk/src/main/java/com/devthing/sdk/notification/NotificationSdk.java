package com.devthing.sdk.notification;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;
import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessaging;

import java.util.Calendar;

/**
 * Created by tuandigital on 1/31/18.
 */

public class NotificationSdk {

    private static final String OPENED_TIME = "opened_time";

    public static void updateOpenTime(Context context){
        FirebaseMessaging.getInstance().subscribeToTopic("main");
        SharedPreferences pref = context.getSharedPreferences("litesdk", Context.MODE_PRIVATE);
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        long milisInDay = Calendar.getInstance().getTimeInMillis() - cal.getTimeInMillis();
        pref.edit().putLong(OPENED_TIME, milisInDay).apply();
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        if(!TextUtils.isEmpty(refreshedToken)) {
            Log.i("ImoLog", "Refreshed token: " + refreshedToken);
        }
    }

    public static long getOpenTime(Context context){
        SharedPreferences pref = context.getSharedPreferences("litesdk", Context.MODE_PRIVATE);
        return pref.getLong(OPENED_TIME, 68400000);
    }
}
