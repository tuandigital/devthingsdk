package com.devthing.sdk.notification;

import android.app.AlarmManager;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v4.app.JobIntentService;
import android.support.v4.app.NotificationCompat;
import android.text.TextUtils;
import android.webkit.URLUtil;
import android.widget.RemoteViews;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by tuandigital on 1/30/18.
 */

public class NotificationService extends JobIntentService {

    public static final String EXTRAS_ICON_URL = "icon_url";
    public static final String EXTRAS_TITLE = "title";
    public static final String EXTRAS_MESSAGE = "message";
    public static final String EXTRAS_URI = "uri";
    public static final String EXTRAS_TYPE = "type";
    public static final String EXTRAS_TRY_COUNT = "try_count";
    private static final String DEFAULT_GROUP_KEY = "main";
    private static final String CHANNEL_NAME = "General";
    public static final int TYPE_ALERT = 1;
    public static final int TYPE_ADS = 2;

    private static final String CHANNEL_ID = "notification_service";
    private static final int OPEN_URI = 1111;
    private static final long NOW = -1;
    public static final int JOB_ID = 1;

    @Override
    protected void onHandleWork(@NonNull Intent intent) {
        String iconUrl = intent.getStringExtra(EXTRAS_ICON_URL);
        String title = intent.getStringExtra(EXTRAS_TITLE);
        String message = intent.getStringExtra(EXTRAS_MESSAGE);
        String uriStr = intent.getStringExtra(EXTRAS_URI);
        int type = intent.getIntExtra(EXTRAS_TYPE, TYPE_ALERT);
        int tryCount = intent.getIntExtra(EXTRAS_TRY_COUNT, 0);
        NotificationChannel channel = null;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            channel = new NotificationChannel(CHANNEL_ID, CHANNEL_NAME, NotificationManager.IMPORTANCE_HIGH);
            NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
            if (notificationManager != null) {
                notificationManager.createNotificationChannel(channel);
            }
        }
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this, CHANNEL_ID);

        int smallIconId = getAppIconResId();
        Bitmap largeIcon = null;
        if (!TextUtils.isEmpty(iconUrl) && URLUtil.isValidUrl(iconUrl)) {
            largeIcon = getBitmapFromURL(iconUrl);
            if (largeIcon == null && tryCount < 10) {
                intent.putExtra(EXTRAS_TRY_COUNT, tryCount + 1);
                PendingIntent repeatIntent = PendingIntent.getService(this, 1, intent, PendingIntent.FLAG_UPDATE_CURRENT);
                AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
                if (alarmManager != null) {
                    alarmManager.set(AlarmManager.RTC_WAKEUP, System.currentTimeMillis() + 1800000, repeatIntent);
                    return;
                }
            }
        } else {
            largeIcon = BitmapFactory.decodeResource(getResources(), smallIconId);
        }
        int notificationId = TextUtils.isEmpty(message) ? type : message.hashCode();
        if (type == TYPE_ADS) {
            Resources res = getResources();
            String packageName = getPackageName();
            RemoteViews remoteViews = new RemoteViews(getPackageName(), res.getIdentifier("notification_ads", "layout", packageName));
            remoteViews.setTextViewText(res.getIdentifier("title", "id", packageName), title);
            remoteViews.setTextViewText(res.getIdentifier("message", "id", packageName), message);
            remoteViews.setBitmap(res.getIdentifier("icon", "id", packageName), "setImageBitmap", largeIcon);
            int notificationIconResId = getResources().getIdentifier("ic_notification", "drawable", getPackageName());
            builder.setSmallIcon(notificationIconResId)
                    .setCustomContentView(remoteViews);
        } else {
            builder.setSmallIcon(smallIconId)
                    .setContentTitle(title)
                    .setContentText(message)
                    .setLargeIcon(largeIcon)
                    .setGroup(DEFAULT_GROUP_KEY);
        }
        Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        builder.setAutoCancel(true)
                .setPriority(NotificationCompat.PRIORITY_HIGH)
                .setSound(alarmSound);

        Intent launchIntent = null;
        if (!TextUtils.isEmpty(uriStr)) {
            Uri uri = Uri.parse(uriStr);
            if (uri.getScheme().equals("market")) {
                String packageName = uri.getQueryParameter("id");
                if (TextUtils.isEmpty(packageName) || isApplicationExist(packageName)) {
                    return;
                }
                if (type == TYPE_ADS) {
                    notificationId = packageName.hashCode();
                }
            }
            launchIntent = new Intent(Intent.ACTION_VIEW, uri);
        } else {
            PackageManager pm = getPackageManager();
            launchIntent = pm.getLaunchIntentForPackage(getPackageName());
        }
        PendingIntent pendingOpenUri = PendingIntent.getActivity(this, notificationId, launchIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        builder.setContentIntent(pendingOpenUri);

        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        if (notificationManager != null) {
            notificationManager.notify(notificationId, builder.build());
        }
    }

    public static Bitmap getBitmapFromURL(String src) {
        try {
            URL url = new URL(src);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            return BitmapFactory.decodeStream(input);
        } catch (IOException e) {
            // Log exception
            return null;
        }
    }

    private int getAppIconResId() {
        String packageName = getPackageName();
        PackageManager pm = getPackageManager();
        try {
            ApplicationInfo applicationInfo = pm.getApplicationInfo(packageName, PackageManager.GET_META_DATA);
            return applicationInfo.icon;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return -1;
    }

    private boolean isApplicationExist(String packageName) {
        PackageManager pm = getPackageManager();
        try {
            PackageInfo info = pm.getPackageInfo(packageName, PackageManager.GET_META_DATA);
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
        return true;
    }

}
