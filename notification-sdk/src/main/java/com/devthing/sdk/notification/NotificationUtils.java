package com.devthing.sdk.notification;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;

/**
 * Created by tuandigital on 1/31/18.
 */

public class NotificationUtils {

    public static void postNotification(Context context, int type, String title, String message, String iconUrl, String uri) {
        scheduleNotification(context, type, title, message, iconUrl, uri, -1);
    }

    public static void scheduleNotification(Context context, int type, String title, String message, String iconUrl, String uri, long time) {
        Intent intent = new Intent();
        intent.putExtra(NotificationService.EXTRAS_TYPE, type);
        intent.putExtra(NotificationService.EXTRAS_TITLE, title);
        intent.putExtra(NotificationService.EXTRAS_MESSAGE, message);
        intent.putExtra(NotificationService.EXTRAS_URI, uri);
        intent.putExtra(NotificationService.EXTRAS_ICON_URL, iconUrl);
        if (time != -1) {
            Intent intentWrapper = new Intent(context, AlarmReceiver.class);
            intentWrapper.putExtra(AlarmReceiver.CONTENT_INTENT, intent);
            PendingIntent pendingIntent = PendingIntent.getBroadcast(context, type, intentWrapper, PendingIntent.FLAG_UPDATE_CURRENT);
            AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
            if (alarmManager != null) {
                alarmManager.set(AlarmManager.RTC_WAKEUP, time, pendingIntent);
            }
        } else {
            NotificationService.enqueueWork(context, NotificationService.class, NotificationService.JOB_ID, intent);
        }
    }
}
